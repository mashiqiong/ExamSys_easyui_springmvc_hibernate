package com.examsys.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.examsys.po.ExamDetail;
import com.examsys.po.ExamMain;
import com.examsys.service.ExamDetailService;
import com.examsys.service.ExamMainService;
import com.examsys.service.QuestionService;
/*
@Controller
@RequestMapping("/examDetail")
public class ExamDetailController {

	private Logger log=Logger.getLogger(this.getClass());
	
	@Resource
	private ExamDetailService examDetailService;
	
	@Resource
	private ExamMainService examMainService;
	
	@Resource
	private QuestionService questionService;
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView mv=new ModelAndView();
		try {
			List<ExamDetail> examDetailList=examDetailService.getList();
		mv.addObject("examDetailList",examDetailList);
		mv.setViewName("examDetailList/ExamDetailListAdd");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/addSave")
	public ModelAndView addSave(ExamDetail examDetail,
			@RequestParam(name="main_id",required=true) Integer main_id,
			@RequestParam(name="question_id",required=true) Integer question_id){
		log.info(examDetail);
		ModelAndView mv=new ModelAndView();
		mv.addObject("main_id",main_id);
		mv.addObject("question_id",question_id);
		mv.addObject("answer",examDetail.getAnswer());
		mv.addObject("score",examDetail.getScore());
		mv.addObject("status",examDetail.getStatus());
		mv.addObject("remark",examDetail.getRemark());
		
		try {
			boolean flag = examDetailService.add(examDetail);
			if(!flag){
				mv.setViewName("examDetail/ExamDetailAdd");
				return mv;
			}
			mv.setViewName("examDetail/ExamDetailManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/update")
	public ModelAndView update(@RequestParam(name="id",required=true) Integer id){
		log.info(id);
		ModelAndView mv=new ModelAndView();
		try {
			ExamDetail examDetail=examDetailService.get(id);
			mv.addObject("examDetail",examDetail);
			if(examDetail==null){
				mv.setViewName("examDetail/ExamDetailManager");
				return mv;
			}
			mv.setViewName("examDetail/ExamDetailUpdate");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/updateSave")
	public ModelAndView updateSave(ExamDetail examDetail,
			@RequestParam(name="main_id",required=true) Integer main_id,
			@RequestParam(name="question_id",required=true) Integer question_id){
		log.info(examDetail);
		ModelAndView mv=new ModelAndView();
		mv.addObject("main_id",main_id);
		mv.addObject("question_id",question_id);
		mv.addObject("answer",examDetail.getAnswer());
		mv.addObject("score",examDetail.getScore());
		mv.addObject("status",examDetail.getStatus());
		mv.addObject("remark",examDetail.getRemark());
		
		try {
			ExamDetail oldexamDetail=examDetailService.get(examDetail.getId());
		
			
			oldexamDetail.setAnswer(examDetail.getAnswer());
			oldexamDetail.setScore(examDetail.getScore());
			oldexamDetail.setStatus(examDetail.getStatus());
			oldexamDetail.setRemark(examDetail.getRemark());
			
			boolean flag = examDetailService.update(oldexamDetail);
			if(!flag){
				mv.setViewName("examDetail/ExamDetailUpdate");
				return mv;
			}
			mv.setViewName("examDetail/ExamDetailManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/delete")
	public ModelAndView delete(@RequestParam(name="id",required=true) Integer id){
		log.info(id);
		
		ModelAndView mv=new ModelAndView();
		try {
			boolean flag = examDetailService.delete(id);
			if(flag){
				mv.addObject("msg","删除成功");
			}
			mv.setViewName("examDetail/ExamDetailManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/select")
	public ModelAndView select(){
		ModelAndView mv=new ModelAndView();
		try {
			List<ExamDetail> examDetailList=examDetailService.getList();
		mv.addObject("examDetailList",examDetailList);
		mv.setViewName("examDetail/ExamDetailManager");
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
		
	}
	
}

*/



