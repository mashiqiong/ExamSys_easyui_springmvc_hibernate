package com.examsys.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.examsys.po.ExamMain;
import com.examsys.service.ExamMainService;
import com.examsys.service.PaperService;
import com.examsys.service.UsersService;
/*
@Controller
@RequestMapping("/examMain")
public class ExamMainController {

	private Logger log=Logger.getLogger(this.getClass());
	
	@Resource
	private ExamMainService examMainService;
	
	@Resource
	private UsersService usersService;
	
	@Resource
	private PaperService paperService;
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView mv=new ModelAndView();
		try {
			List<ExamMain> examMainList=examMainService.getList();
		mv.addObject("examMainList",examMainList);
		mv.setViewName("examMainList/ExamMainListAdd");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/addSave")
	public ModelAndView addSave(ExamMain examMain,
			@RequestParam(name="user_id",required=true) Integer user_id,
			@RequestParam(name="paper_id",required=true) Integer paper_id){
		log.info(examMain);
		ModelAndView mv=new ModelAndView();
		mv.addObject("user_id",user_id);
		mv.addObject("paper_id",paper_id);
		mv.addObject("start_time",examMain.getStart_time());
		mv.addObject("end_time",examMain.getEnd_time());
		mv.addObject("score",examMain.getScore());
		mv.addObject("ip",examMain.getIp());
		mv.addObject("status",examMain.getStatus());
		mv.addObject("remark",examMain.getRemark());
		try {
			boolean flag = examMainService.add(examMain);
			if(!flag){
				mv.setViewName("examMain/ExamMainAdd");
				return mv;
			}
			mv.setViewName("examMain/ExamMainManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/update")
	public ModelAndView update(@RequestParam(name="id",required=true) Integer id){
		log.info(id);
		ModelAndView mv=new ModelAndView();
		try {
			ExamMain examMain=examMainService.get(id);
			mv.addObject("examMain",examMain);
			if(examMain==null){
				mv.setViewName("examMain/ExamMainManager");
				return mv;
			}
			mv.setViewName("examMain/ExamMainUpdate");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/updateSave")
	public ModelAndView updateSave(ExamMain examMain,
			@RequestParam(name="user_id",required=true) Integer user_id,
			@RequestParam(name="paper_id",required=true) Integer paper_id){
		log.info(examMain);
		ModelAndView mv=new ModelAndView();
		mv.addObject("user_id",user_id);
		mv.addObject("paper_id",paper_id);
		mv.addObject("start_time",examMain.getStart_time());
		mv.addObject("end_time",examMain.getEnd_time());
		mv.addObject("score",examMain.getScore());
		mv.addObject("ip",examMain.getIp());
		mv.addObject("status",examMain.getStatus());
		mv.addObject("remark",examMain.getRemark());
		
		try {
			ExamMain oldexamMain=examMainService.get(examMain.getId());
		
			
			oldexamMain.setStart_time(examMain.getStart_time());
			oldexamMain.setEnd_time(examMain.getEnd_time());
			oldexamMain.setIp(examMain.getIp());
			oldexamMain.setScore(examMain.getScore());
			oldexamMain.setStatus(examMain.getStatus());
			oldexamMain.setRemark(examMain.getRemark());
			
			boolean flag = examMainService.update(oldexamMain);
			if(!flag){
				mv.setViewName("examMain/ExamMainUpdate");
				return mv;
			}
			mv.setViewName("examMain/ExamMainManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/delete")
	public ModelAndView delete(@RequestParam(name="id",required=true) Integer id){
		log.info(id);
		
		ModelAndView mv=new ModelAndView();
		try {
			boolean flag = examMainService.delete(id);
			if(flag){
				mv.addObject("msg","删除成功");
			}
			mv.setViewName("examMain/ExamMainManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/select")
	public ModelAndView select(){
		ModelAndView mv=new ModelAndView();
		try {
			List<ExamMain> examMainList=examMainService.getList();
		mv.addObject("examMainList",examMainList);
		mv.setViewName("examMain/ExamMainManager");
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
		
	}
	
}
*/




