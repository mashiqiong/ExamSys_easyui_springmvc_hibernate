package com.examsys.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.examsys.po.Onlines;
import com.examsys.service.OnlinesService;
import com.examsys.service.PaperService;
import com.examsys.service.UsersService;
/*
@Controller
@RequestMapping("/onlines")
public class OnlinesController {

	private Logger log=Logger.getLogger(this.getClass());
	
	@Resource
	private UsersService usersService;
	
	@Resource
	private PaperService paperService;
	
	@Resource
	private OnlinesService onlinesService;
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView mv=new ModelAndView();
		try {
			List<Onlines> onlinesList=onlinesService.getList();
		mv.addObject("onlinesList",onlinesList);
		mv.setViewName("onlines/OnlinesAdd");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/addSave")
	public ModelAndView addSave(Onlines onlines,
			@RequestParam(name="user_id",required=true) Integer user_id,
			@RequestParam(name="paper_id",required=true) Integer paper_id){
		log.info(onlines);
		ModelAndView mv=new ModelAndView();
		mv.addObject("user_id",user_id);
		mv.addObject("paper_id",paper_id);
		mv.addObject("last_time",onlines.getLast_time());
		mv.addObject("ip",onlines.getIp());
		
		try {
			boolean flag = onlinesService.add(onlines);
			if(!flag){
				mv.setViewName("onlines/onlinesAdd");
				return mv;
			}
			mv.setViewName("onlines/OnlinesManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/update")
	public ModelAndView update(@RequestParam(name="id",required=true) Integer id){
		log.info(id);
		ModelAndView mv=new ModelAndView();
		try {
			Onlines onlines=onlinesService.get(id);
			mv.addObject("onlines",onlines);
			if(onlines==null){
				mv.setViewName("onlines/OnlinesManager");
				return mv;
			}
			mv.setViewName("onlines/OnlinesUpdate");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/updateSave")
	public ModelAndView updateSave(Onlines onlines,
			@RequestParam(name="user_id",required=true) Integer user_id,
			@RequestParam(name="paper_id",required=true) Integer paper_id){
		log.info(onlines);
		ModelAndView mv=new ModelAndView();
		mv.addObject("user_id",user_id);
		mv.addObject("paper_id",paper_id);
		mv.addObject("last_time",onlines.getLast_time());
		mv.addObject("ip",onlines.getIp());
		
		try {
			Onlines oldonlines=onlinesService.get(onlines.getId());
		
			oldonlines.setLast_time(onlines.getLast_time());
			oldonlines.setIp(onlines.getIp());
			
			boolean flag = onlinesService.update(oldonlines);
			if(!flag){
				mv.setViewName("onlines/OnlinesUpdate");
				return mv;
			}
			mv.setViewName("onlines/OnlinesManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/delete")
	public ModelAndView delete(@RequestParam(name="id",required=true) Integer id){
		log.info(id);
		
		ModelAndView mv=new ModelAndView();
		try {
			boolean flag = onlinesService.delete(id);
			if(flag){
				mv.addObject("msg","删除成功");
			}
			mv.setViewName("onlines/OnlinesManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/select")
	public ModelAndView select(){
		ModelAndView mv=new ModelAndView();
		try {
			List<Onlines> onlinesList=onlinesService.getList();
		mv.addObject("onlinesList",onlinesList);
		mv.setViewName("onlines/OnlinesManager");
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
		
	}
	
}*/





