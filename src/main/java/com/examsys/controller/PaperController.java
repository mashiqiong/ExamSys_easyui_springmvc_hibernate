package com.examsys.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.examsys.po.Paper;
import com.examsys.service.AdminService;
import com.examsys.service.PaperService;

@Controller
@RequestMapping("/paper")
public class PaperController {

	private Logger log=Logger.getLogger(this.getClass());
	
	@Resource
	private AdminService adminService;
	
	@Resource
	private PaperService paperService;
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView mv=new ModelAndView();
		try {
			List<Paper> paperList=paperService.getList();
		mv.addObject("paperList",paperList);
		mv.setViewName("paper/PaperAdd");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/addSave")
	public ModelAndView addSave(Paper paper,
			@RequestParam(name="admin_id",required=true) Integer admin_id){
		log.info(paper);
		ModelAndView mv=new ModelAndView();
		mv.addObject("admin_id",admin_id);
		mv.addObject("paper_name",paper.getPaper_name());
		mv.addObject("start_time",paper.getStart_time());
		mv.addObject("end_time",paper.getEnd_time());
		mv.addObject("paper_minute",paper.getPaper_minute());
		mv.addObject("total_score",paper.getTotal_score());
		mv.addObject("post_date",paper.getPost_date());
		mv.addObject("show_score",paper.getShow_score());
		mv.addObject("qorder",paper.getQorder());
		mv.addObject("status",paper.getStatus());
		mv.addObject("remark",paper.getRemark());
		
		try {
			boolean flag = paperService.add(paper);
			if(!flag){
				mv.setViewName("paper/paperAdd");
				return mv;
			}
			mv.setViewName("paper/PaperManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/update")
	public ModelAndView update(@RequestParam(name="id",required=true) Integer id){
		log.info(id);
		ModelAndView mv=new ModelAndView();
		try {
			Paper paper=paperService.get(id);
			mv.addObject("paper",paper);
			if(paper==null){
				mv.setViewName("paper/PaperManager");
				return mv;
			}
			mv.setViewName("paper/PaperUpdate");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/updateSave")
	public ModelAndView updateSave(Paper paper,
			@RequestParam(name="admin_id",required=true) Integer admin_id){
		log.info(paper);
		ModelAndView mv=new ModelAndView();
		mv.addObject("admin_id",admin_id);
		mv.addObject("paper_name",paper.getPaper_name());
		mv.addObject("start_time",paper.getStart_time());
		mv.addObject("end_time",paper.getEnd_time());
		mv.addObject("paper_minute",paper.getPaper_minute());
		mv.addObject("total_score",paper.getTotal_score());
		mv.addObject("post_date",paper.getPost_date());
		mv.addObject("show_score",paper.getShow_score());
		mv.addObject("qorder",paper.getQorder());
		mv.addObject("status",paper.getStatus());
		mv.addObject("remark",paper.getRemark());
		
		try {
			Paper oldpaper=paperService.get(paper.getId());
		
			oldpaper.setPaper_name(paper.getPaper_name());
			oldpaper.setStart_time(paper.getStart_time());
			oldpaper.setEnd_time(paper.getEnd_time());
			oldpaper.setPaper_minute(paper.getPaper_minute());
			oldpaper.setTotal_score(paper.getTotal_score());
			oldpaper.setPost_date(paper.getPost_date());
			oldpaper.setShow_score(paper.getShow_score());
			oldpaper.setQorder(paper.getQorder());
			oldpaper.setStatus(paper.getStatus());
			
			
			boolean flag = paperService.update(oldpaper);
			if(!flag){
				mv.setViewName("paper/PaperUpdate");
				return mv;
			}
			mv.setViewName("paper/PaperManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/delete")
	public ModelAndView delete(@RequestParam(name="id",required=true) Integer id){
		log.info(id);
		
		ModelAndView mv=new ModelAndView();
		try {
			boolean flag = paperService.delete(id);
			if(flag){
				mv.addObject("msg","删除成功");
			}
			mv.setViewName("paper/PaperManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/select")
	public ModelAndView select(){
		ModelAndView mv=new ModelAndView();
		try {
			List<Paper> paperList=paperService.getList();
		mv.addObject("paperList",paperList);
		mv.setViewName("paper/PaperManager");
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
		
	}
	
}





