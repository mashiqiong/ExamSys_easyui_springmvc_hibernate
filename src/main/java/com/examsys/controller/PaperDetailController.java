package com.examsys.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.examsys.po.PaperDetail;
import com.examsys.service.PaperDetailService;
import com.examsys.service.PaperService;

@Controller
@RequestMapping("/paperDetail")
public class PaperDetailController {

	private Logger log=Logger.getLogger(this.getClass());
	
	@Resource
	private PaperDetailService paperDetailService;
	
	@Resource
	private PaperService paperService;
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView mv=new ModelAndView();
		try {
			List<PaperDetail> paperDetailList=paperDetailService.getList();
		mv.addObject("paperDetailList",paperDetailList);
		mv.setViewName("paperDetailList/PaperDetailListAdd");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/addSave")
	public ModelAndView addSave(PaperDetail paperDetail,
			@RequestParam(name="paper_id",required=true) Integer paper_id,
			@RequestParam(name="paper_section_id",required=true) Integer paper_section_id,
			@RequestParam(name="question_id",required=true) Integer question_id){
		log.info(paperDetail);
		ModelAndView mv=new ModelAndView();
		mv.addObject("paper_id",paper_id);
		mv.addObject("paper_section_id",paper_section_id);
		mv.addObject("question_id",question_id);
		mv.addObject("score",paperDetail.getScore());
		mv.addObject("porder",paperDetail.getPorder());
		
		try {
			boolean flag = paperDetailService.add(paperDetail);
			if(!flag){
				mv.setViewName("paperDetail/PaperDetailAdd");
				return mv;
			}
			mv.setViewName("paperDetail/PaperDetailManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/update")
	public ModelAndView update(@RequestParam(name="id",required=true) Integer id){
		log.info(id);
		ModelAndView mv=new ModelAndView();
		try {
			PaperDetail paperDetail=paperDetailService.get(id);
			mv.addObject("paperDetail",paperDetail);
			if(paperDetail==null){
				mv.setViewName("paperDetail/PaperDetailManager");
				return mv;
			}
			mv.setViewName("paperDetail/PaperDetailUpdate");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/updateSave")
	public ModelAndView updateSave(PaperDetail paperDetail,
			@RequestParam(name="paper_id",required=true) Integer paper_id,
			@RequestParam(name="paper_section_id",required=true) Integer paper_section_id,
			@RequestParam(name="question_id",required=true) Integer question_id){
		log.info(paperDetail);
		ModelAndView mv=new ModelAndView();
		mv.addObject("paper_id",paper_id);
		mv.addObject("paper_section_id",paper_section_id);
		mv.addObject("question_id",question_id);
		mv.addObject("score",paperDetail.getScore());
		mv.addObject("porder",paperDetail.getPorder());
		
		try {
			PaperDetail oldpaperDetail=paperDetailService.get(paperDetail.getId());
		
			oldpaperDetail.setScore(paperDetail.getScore());
			oldpaperDetail.setPorder(paperDetail.getPorder());
			
			
			boolean flag = paperDetailService.update(oldpaperDetail);
			if(!flag){
				mv.setViewName("paperDetail/PaperDetailUpdate");
				return mv;
			}
			mv.setViewName("paperDetail/PaperDetailManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/delete")
	public ModelAndView delete(@RequestParam(name="id",required=true) Integer id){
		log.info(id);
		
		ModelAndView mv=new ModelAndView();
		try {
			boolean flag = paperDetailService.delete(id);
			if(flag){
				mv.addObject("msg","删除成功");
			}
			mv.setViewName("paperDetail/PaperDetailManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/select")
	public ModelAndView select(){
		ModelAndView mv=new ModelAndView();
		try {
			List<PaperDetail> paperDetailList=paperDetailService.getList();
		mv.addObject("paperDetailList",paperDetailList);
		mv.setViewName("paperDetail/PaperDetailManager");
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
		
	}
	
}





