package com.examsys.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.examsys.po.PaperSection;
import com.examsys.service.PaperSectionService;
import com.examsys.service.PaperService;

@Controller
@RequestMapping("/paperSection")
public class PaperSectionController {

	private Logger log=Logger.getLogger(this.getClass());
	
	@Resource
	private PaperSectionService paperSectionService;
	
	@Resource
	private PaperService paperService;
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView mv=new ModelAndView();
		try {
			List<PaperSection> paperSectionList=paperSectionService.getList();
		mv.addObject("paperSectionList",paperSectionList);
		mv.setViewName("paperSectionList/PaperSectionListAdd");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/addSave")
	public ModelAndView addSave(PaperSection paperSection,
			@RequestParam(name="paper_id",required=true) Integer paper_id){
		log.info(paperSection);
		ModelAndView mv=new ModelAndView();
		mv.addObject("paper_id",paper_id);
		mv.addObject("section_name",paperSection.getSection_name());
		mv.addObject("per_score",paperSection.getPer_score());
		mv.addObject("remark",paperSection.getRemark());
		
		try {
			boolean flag = paperSectionService.add(paperSection);
			if(!flag){
				mv.setViewName("paperSection/PaperSectionAdd");
				return mv;
			}
			mv.setViewName("paperSection/PaperSectionManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/update")
	public ModelAndView update(@RequestParam(name="id",required=true) Integer id){
		log.info(id);
		ModelAndView mv=new ModelAndView();
		try {
			PaperSection paperSection=paperSectionService.get(id);
			mv.addObject("paperSection",paperSection);
			if(paperSection==null){
				mv.setViewName("paperSection/PaperSectionManager");
				return mv;
			}
			mv.setViewName("paperSection/PaperSectionUpdate");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/updateSave")
	public ModelAndView updateSave(PaperSection paperSection,
			@RequestParam(name="paper_id",required=true) Integer paper_id){
		log.info(paperSection);
		ModelAndView mv=new ModelAndView();
		mv.addObject("paper_id",paper_id);
		mv.addObject("section_name",paperSection.getSection_name());
		mv.addObject("per_score",paperSection.getPer_score());
		mv.addObject("remark",paperSection.getRemark());
		
		try {
			PaperSection oldpaperSection=paperSectionService.get(paperSection.getId());
		
			oldpaperSection.setSection_name(paperSection.getSection_name());;
			oldpaperSection.setPer_score(paperSection.getPer_score());
			oldpaperSection.setRemark(paperSection.getRemark());
			
			boolean flag = paperSectionService.update(oldpaperSection);
			if(!flag){
				mv.setViewName("paperSection/PaperSectionUpdate");
				return mv;
			}
			mv.setViewName("paperSection/PaperSectionManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/delete")
	public ModelAndView delete(@RequestParam(name="id",required=true) Integer id){
		log.info(id);
		
		ModelAndView mv=new ModelAndView();
		try {
			boolean flag = paperSectionService.delete(id);
			if(flag){
				mv.addObject("msg","删除成功");
			}
			mv.setViewName("paperSection/PaperSectionManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/select")
	public ModelAndView select(){
		ModelAndView mv=new ModelAndView();
		try {
			List<PaperSection> paperSectionList=paperSectionService.getList();
		mv.addObject("paperSectionList",paperSectionList);
		mv.setViewName("paperSection/PaperSectionManager");
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
		
	}
	
}





