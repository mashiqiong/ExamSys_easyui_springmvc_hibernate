package com.examsys.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.examsys.po.PaperUserGroup;
import com.examsys.service.PaperService;
import com.examsys.service.PaperUserGroupService;
import com.examsys.service.UserGroupsService;

@Controller
@RequestMapping("/paperUserGroup")
public class PaperUserGroupController {

	private Logger log=Logger.getLogger(this.getClass());
	
	@Resource
	private PaperUserGroupService paperUserGroupService;
	
	@Resource
	private PaperService paperService;
	
	@Resource
	private UserGroupsService userGroupsService;
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView mv=new ModelAndView();
		try {
			List<PaperUserGroup> paperUserGroupList=paperUserGroupService.getList();
		mv.addObject("paperUserGroupList",paperUserGroupList);
		mv.setViewName("paperUserGroup/PaperUserGroupAdd");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/addSave")
	public ModelAndView addSave(PaperUserGroup paperUserGroup,
			@RequestParam(name="paper_id",required=true) Integer paper_id,
			@RequestParam(name="user_group_id",required=true) Integer user_group_id){
		log.info(paperUserGroup);
		ModelAndView mv=new ModelAndView();
		mv.addObject("paper_id",paper_id);
		mv.addObject("user_group_id",user_group_id);
		
		try {
			boolean flag = paperUserGroupService.add(paperUserGroup);
			if(!flag){
				mv.setViewName("paperUserGroup/paperUserGroupAdd");
				return mv;
			}
			mv.setViewName("paperUserGroup/paperUserGroupManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/update")
	public ModelAndView update(@RequestParam(name="id",required=true) Integer id){
		log.info(id);
		ModelAndView mv=new ModelAndView();
		try {
			PaperUserGroup paperUserGroup=paperUserGroupService.get(id);
			mv.addObject("paperUserGroup",paperUserGroup);
			if(paperUserGroup==null){
				mv.setViewName("paperUserGroup/PaperUserGroupManager");
				return mv;
			}
			mv.setViewName("paperUserGroup/PaperUserGroupUpdate");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/updateSave")
	public ModelAndView updateSave(PaperUserGroup paperUserGroup,
			@RequestParam(name="paper_id",required=true) Integer paper_id,
			@RequestParam(name="user_group_id",required=true) Integer user_group_id){
		log.info(paperUserGroup);
		ModelAndView mv=new ModelAndView();
		mv.addObject("paper_id",paper_id);
		mv.addObject("user_group_id",user_group_id);
		
		try {
			PaperUserGroup oldpaperUserGroup=paperUserGroupService.get(paperUserGroup.getId());
		
			
			
			boolean flag = paperUserGroupService.update(oldpaperUserGroup);
			if(!flag){
				mv.setViewName("paperUserGroup/PaperUserGroupUpdate");
				return mv;
			}
			mv.setViewName("paperUserGroup/PaperUserGroupManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/delete")
	public ModelAndView delete(@RequestParam(name="id",required=true) Integer id){
		log.info(id);
		
		ModelAndView mv=new ModelAndView();
		try {
			boolean flag = paperUserGroupService.delete(id);
			if(flag){
				mv.addObject("msg","删除成功");
			}
			mv.setViewName("paperUserGroup/PaperUserGroupManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/select")
	public ModelAndView select(){
		ModelAndView mv=new ModelAndView();
		try {
			List<PaperUserGroup> paperUserGroupList=paperUserGroupService.getList();
		mv.addObject("paperUserGroupList",paperUserGroupList);
		mv.setViewName("paperUserGroup/PaperUserGroupManager");
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
		
	}
	
}





