package com.examsys.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.examsys.po.Admin;
import com.examsys.po.Question;
import com.examsys.po.QuestionDb;
import com.examsys.service.QuestionDbService;
import com.examsys.service.QuestionService;
import com.examsys.util.Page;
/**
 * 题目控制层
 *
 */
@Controller
@RequestMapping("/question")
public class QuestionController {
	
	//log4j日志对象
	private Logger log=Logger.getLogger(this.getClass());
	
	@Resource //用@Resource注解，告诉spring这里需要把业务逻辑层对象注入进来(IoC,DI)
	private QuestionService questionService;
	
	@Resource //用@Resource注解，告诉spring这里需要把业务逻辑层对象注入进来(IoC,DI)
	private QuestionDbService questionDbService;
	
	/**
	 * 获取题库下拉框数据
	 * @return
	 */
	@RequestMapping("/questionDbDatas")
	public @ResponseBody List<QuestionDb> questionDbDatas(){
		log.info("获取题库下拉框数据");
		List<QuestionDb> questionDbList=null;
		try {
			questionDbList=questionDbService.getList();
		} catch (Exception e) {
			log.error("获取题库下拉框数据失败",e);
		}
		return questionDbList;
	}
	
	/**
	 * 保存添加
	 * @param question
	 * @return
	 */
	@RequestMapping("/addSave")
	public @ResponseBody Map addSave(HttpServletRequest req,Question question,
			@RequestParam(name="db_id",required=true) Integer db_id){
		
		log.info("接收到页面添加的数据："+question);//把question放到日志
		Map jsonDatas=new HashMap();//存放json数据的集合
		jsonDatas.put("status", 0);//默认状态为0，表示操作失败
		
		try {
			Admin admin = (Admin)req.getSession().getAttribute("ADMIN");
			
			QuestionDb questionDb = questionDbService.get(db_id);
			question.setAdmin(admin);
			question.setQuestionDb(questionDb);
			question.setCreate_date(new Date(System.currentTimeMillis()));
			
			boolean flag = questionService.add(question);
			if(flag){
				jsonDatas.put("status", 1);//设置状态为1，表示操作成功
			}
		} catch (Exception e) {
			log.error("添加失败", e);
		}
		return jsonDatas;//返回存放json数据的集合,最终给springmvc转换为json数据输出给浏览器
	}
	
	/**
	 * 初始化修改
	 * @param id
	 * @return
	 */
	@RequestMapping("/update")
	public @ResponseBody Question update(@RequestParam(name="id",required=true) Integer id){
		log.info("开始初始化编号为"+id+"的题目数据供前端修改");//把id放到日志
		Question question=null;
		try {
			question=questionService.get(id);
		} catch (Exception e) {
			log.error("初始化修改失败", e);
		}
		return question;
	}
	
	/**
	 * 保存修改
	 * @param question
	 * @param db_id
	 * @return
	 */
	@RequestMapping("/updateSave")
	public @ResponseBody Map updateSave(Question question,
			@RequestParam(name="db_id",required=true) Integer db_id){
		
		log.info("接收到页面修改的数据："+question+",题库编号："+db_id);//把question放到日志
		Map jsonDatas=new HashMap();//存放json数据的集合
		jsonDatas.put("status", 0);//默认状态为0，表示操作失败
		
		try {
			Question oldquestion=questionService.get(question.getId());
		
			oldquestion.setQtype(question.getQtype());
			oldquestion.setQlevel(question.getQlevel());
			oldquestion.setQfrom(question.getQfrom());
			oldquestion.setContent(question.getContent());
			oldquestion.setSkey(question.getSkey());
			oldquestion.setKey_desc(question.getKey_desc());
			oldquestion.setCreate_date(question.getCreate_date());
			oldquestion.setStatus(question.getStatus());
			oldquestion.setRemark(question.getRemark());
			oldquestion.setNotInIds(question.getNotInIds());
			
			QuestionDb questionDb = questionDbService.get(db_id);
			question.setQuestionDb(questionDb);
			
			boolean flag = questionService.update(oldquestion);
			if(flag){
				jsonDatas.put("status", 1);//设置状态为1，表示操作成功
			}
		} catch (Exception e) {
			log.error("修改失败", e);
		}
		return jsonDatas;//返回存放json数据的集合,最终给springmvc转换为json数据输出给浏览器
	}
	
	/**
	 * 删除题目
	 *@param id 待删除的题目的编号
	 */
	@RequestMapping("/delete")
	public @ResponseBody Map delete(@RequestParam(name="id",required=true) Integer id){
		log.info("开始删除编号为"+id+"的系统参数");//把id放到日志
		Map jsonDatas=new HashMap();//存放json数据的集合
		jsonDatas.put("status", 0);//默认状态为0，表示操作失败
		
		try {
			boolean flag = questionService.delete(id);
			if(flag){
				jsonDatas.put("status", 1);//设置状态为1，表示操作成功
			}
		} catch (Exception e) {
			log.error("删除失败", e);
		}
		return jsonDatas;//返回存放json数据的集合,最终给springmvc转换为json数据输出给浏览器
	}
	
	/**
	 * 题目管理
	 */
	@RequestMapping("/select")
	public ModelAndView select(){
		log.info("进入题目管理页面");
		ModelAndView mv=new ModelAndView();//创建视图模型对象
		mv.setViewName("question/questionManager");//设置显示页面
		
		return mv;//返回视图模型对象
		
	}
	
	/**
	 * 题目管理表格数据
	 * @param question 查询条件
	 * @param page 页码
	 * @param rows 每一页行数
	 * @return
	 */
	@RequestMapping("/selectDatas")
	public @ResponseBody Map selectDatas(Question question,
			//page当前是第几页
			@RequestParam(value="page",required=false)int page,
			//rows每页几条数据
			@RequestParam(value="rows",required=false)int rows){
		
		log.info("开始获取题目管理表格数据");
		Map jsonDatas=new HashMap();//待返回json集合数据;
		try {
			//分页处理
			Page<Question> pageObj = questionService.getList(question,page,rows);
			//取记录总条数
			jsonDatas.put("total", pageObj.getTotal());//总数
			jsonDatas.put("rows", pageObj.getResultlist());//前端需要的行数据
		} catch (Exception e) {
			log.error("获取题目管理表格数据失败",e);
		}
		return jsonDatas;//返回存放数据的集合,最终给springmvc转换为json数据输出给浏览器
	}
	
}





