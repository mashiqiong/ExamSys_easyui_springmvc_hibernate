package com.examsys.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.examsys.po.Admin;
import com.examsys.po.QuestionDb;
import com.examsys.service.AdminService;
import com.examsys.service.QuestionDbService;
import com.examsys.util.Page;

/**
 * 题库管理控制层
 *
 */
@Controller
@RequestMapping("/questionDb")
public class QuestionDbController {

	//log4j日志对象
	private Logger log=Logger.getLogger(this.getClass());
	
	@Resource //用@Resource注解，告诉spring这里需要把业务逻辑层对象注入进来(IoC,DI)
	private QuestionDbService questionDbService;
	
	@Resource //用@Resource注解，告诉spring这里需要把业务逻辑层对象注入进来(IoC,DI)
	private AdminService adminService;
	
	/**
	 * 保存添加
	 * @param questionDb
	 * @return
	 */
	@RequestMapping("/addSave")
	public @ResponseBody Map addSave(HttpServletRequest req,QuestionDb questionDb){
		log.info("接收到页面修改的数据："+questionDb);//把questionDb放到日志
		Map jsonDatas=new HashMap();//存放json数据的集合
		jsonDatas.put("status", 0);//默认状态为0，表示操作失败
		
		Admin admin = (Admin)req.getAttribute("ADMIN");
		if(admin==null){//管理员未登录时不允许添加题库
			log.error("添加失败,管理员未登录");
			return jsonDatas;
		}
		
		questionDb.setAdmin(admin);
		questionDb.setCreate_date(new Date(System.currentTimeMillis()));
		
		try {
			boolean flag = questionDbService.add(questionDb);
			if(flag){
				jsonDatas.put("status", 1);//设置状态为1，表示操作成功
			}
		} catch (Exception e) {
			log.error("添加失败", e);
		}
		return jsonDatas;//返回存放json数据的集合,最终给springmvc转换为json数据输出给浏览器
	}
	
	/**
	 * 初始化修改
	 * @param id
	 * @return
	 */
	@RequestMapping("/update")
	public QuestionDb update(@RequestParam(name="id",required=true) Integer id){
		log.info("开始初始化编号为"+id+"的题库数据供前端修改");//把id放到日志
		QuestionDb questionDb=null;
		try {
			//通过id去获取题库
			questionDb=questionDbService.get(id);
		} catch (Exception e) {
			log.error("初始化修改失败", e);
		}
		return questionDb;//返回功能对象,最终给springmvc转换为json数据输出给浏览器
	}
	
	/**
	 * 保存修改
	 * @param questionDb
	 * @return
	 */
	@RequestMapping("/updateSave")
	public @ResponseBody Map updateSave(QuestionDb questionDb){
		log.info("接收到页面修改的数据："+questionDb);//把questionDb放到日志
		Map jsonDatas=new HashMap();//存放json数据的集合
		jsonDatas.put("status", 0);//默认状态为0，表示操作失败
		
		try {
			//通过id去获取题库
			QuestionDb oldquestionDb=questionDbService.get(questionDb.getId());
		
			oldquestionDb.setName(questionDb.getName());
			oldquestionDb.setStatus(questionDb.getStatus());
			oldquestionDb.setRemark(questionDb.getRemark());
			
			//调用修改的方法，修改原先的信息
			boolean flag = questionDbService.update(oldquestionDb);
			if(flag){
				jsonDatas.put("status", 1);//设置状态为1，表示操作成功
			}
		} catch (Exception e) {
			log.error("修改失败", e);
		}
		
		return jsonDatas;//返回存放json数据的集合,最终给springmvc转换为json数据输出给浏览器
	}
	
	/**
	 * 删除题库
	 *@param id 待删除的题库的编号
	 */
	@RequestMapping("/delete")
	public @ResponseBody Map delete(@RequestParam(name="id",required=true) Integer id){
		log.info("开始删除编号为"+id+"的系统参数");//把id放到日志
		Map jsonDatas=new HashMap();//存放json数据的集合
		jsonDatas.put("status", 0);//默认状态为0，表示操作失败
		try {
			//通过id去删除数据
			boolean flag = questionDbService.delete(id);
			if(flag){
				jsonDatas.put("status", 1);//设置状态为1，表示操作成功
			}
		} catch (Exception e) {
			log.error("删除失败", e);
		}
		return jsonDatas;//返回存放json数据的集合,最终给springmvc转换为json数据输出给浏览器
	}
	
	/**
	 * 题库管理
	 * @return
	 */
	@RequestMapping("/select")
	public ModelAndView select(){
		log.info("进入题库管理页面");
		ModelAndView mv=new ModelAndView();//创建视图模型对象
		mv.setViewName("questionDb/QuestionDbManager");//设置显示页面
		return mv;//返回视图模型对象
		
	}
	
	/**
	 * 题库管理表格数据
	 * @param questionDb 查询条件
	 * @param page 页码
	 * @param rows 每一页行数
	 * @return
	 */
	@RequestMapping("/selectDatas")
	public @ResponseBody Map selectDatas(QuestionDb questionDb,
			//page当前是第几页
			@RequestParam(value="page",required=false)int page,
			//rows每页几条数据
			@RequestParam(value="rows",required=false)int rows){
		
		log.info("开始获取题库管理表格数据");
		Map jsonDatas=new HashMap();//待返回json集合数据;
		try {
			//分页处理
			Page<QuestionDb> pageObj = questionDbService.getList(questionDb,page,rows);
			//取记录总条数
			jsonDatas.put("total", pageObj.getTotal());//总数
			jsonDatas.put("rows", pageObj.getResultlist());//前端需要的行数据
		} catch (Exception e) {
			log.error("获取题库管理表格数据失败",e);
		}
		return jsonDatas;//返回存放数据的集合,最终给springmvc转换为json数据输出给浏览器
	}
	
}


