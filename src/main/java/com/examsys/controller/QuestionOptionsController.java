package com.examsys.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.examsys.po.QuestionOptions;
import com.examsys.service.QuestionOptionsService;
import com.examsys.service.QuestionService;
/*
@Controller
@RequestMapping("/questionOptions")
public class QuestionOptionsController {

	private Logger log=Logger.getLogger(this.getClass());
	
	@Resource
	private QuestionOptionsService questionOptionsService;
	
	@Resource
	private QuestionService questionService;
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView mv=new ModelAndView();
		try {
			List<QuestionOptions> questionOptions=questionOptionsService.getList();
		mv.addObject("questionOptions",questionOptions);
		mv.setViewName("questionOptions/QuestionOptionsAdd");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/addSave")
	public ModelAndView addSave(QuestionOptions questionOptions,
			@RequestParam(name="question_id",required=true) Integer question_id){
		log.info(questionOptions);
		ModelAndView mv=new ModelAndView();
		mv.addObject("question_id",question_id);
		mv.addObject("salisa",questionOptions.getSalisa());
		mv.addObject("soption",questionOptions.getSoption());
		mv.addObject("sextend",questionOptions.getSextend());
		
		try {
			boolean flag = questionOptionsService.add(questionOptions);
			if(!flag){
				mv.setViewName("questionOptions/questionOptionsAdd");
				return mv;
			}
			mv.setViewName("questionOptions/questionOptionsManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/update")
	public ModelAndView update(@RequestParam(name="id",required=true) Integer id){
		log.info(id);
		ModelAndView mv=new ModelAndView();
		try {
			QuestionOptions questionOptions=questionOptionsService.get(id);
			mv.addObject("questionOptions",questionOptions);
			if(questionOptions==null){
				mv.setViewName("questionOptions/QuestionOptionsManager");
				return mv;
			}
			mv.setViewName("questionOptions/QuestionOptionsUpdate");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/updateSave")
	public ModelAndView updateSave(QuestionOptions questionOptions,
			@RequestParam(name="question_id",required=true) Integer question_id){
		log.info(questionOptions);
		ModelAndView mv=new ModelAndView();
		mv.addObject("question_id",question_id);
		mv.addObject("salisa",questionOptions.getSalisa());
		mv.addObject("soption",questionOptions.getSoption());
		mv.addObject("sextend",questionOptions.getSextend());
		
		try {
			QuestionOptions oldquestionOptions=questionOptionsService.get(questionOptions.getId());
		
			oldquestionOptions.setSalisa(questionOptions.getSalisa());
			oldquestionOptions.setSoption(questionOptions.getSoption());
			oldquestionOptions.setSextend(questionOptions.getSextend());
			
			boolean flag = questionOptionsService.update(oldquestionOptions);
			if(!flag){
				mv.setViewName("questionOptions/QuestionOptionsUpdate");
				return mv;
			}
			mv.setViewName("questionOptions/QuestionOptionsManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/delete")
	public ModelAndView delete(@RequestParam(name="id",required=true) Integer id){
		log.info(id);
		
		ModelAndView mv=new ModelAndView();
		try {
			boolean flag = questionOptionsService.delete(id);
			if(flag){
				mv.addObject("msg","删除成功");
			}
			mv.setViewName("questionOptions/QuestionOptionsManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping("/select")
	public ModelAndView select(){
		ModelAndView mv=new ModelAndView();
		try {
			List<QuestionOptions> questionOptions=questionOptionsService.getList();
		mv.addObject("questionOptions",questionOptions);
		mv.setViewName("questionOptions/QuestionOptionsManager");
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
		
	}
	
}
*/




