package com.examsys.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.examsys.po.SysTips;
import com.examsys.service.SysTipsService;
import com.examsys.util.Page;

/**
 * 系统提示信息管理控制层
 *
 */
@Controller
@RequestMapping("/sysTips")
public class SysTipsController {

	//log4j日志对象
	private Logger log=Logger.getLogger(this.getClass());
	
	@Resource //用@Resource注解，告诉spring这里需要把业务逻辑层对象注入进来(IoC,DI)
	private SysTipsService sysTipsService;
	
	/**
	 * 保存添加
	 * @param sysTips
	 * @return
	 */
	@RequestMapping("/addSave")
	public @ResponseBody Map addSave(SysTips sysTips){
		log.info("接收到页面添加的数据："+sysTips);//把sysTips放到日志
		Map jsonDatas=new HashMap();//存放json数据的集合
		jsonDatas.put("status", 0);//默认状态为0，表示操作失败
		
		try {
			boolean flag = sysTipsService.add(sysTips);
			if(flag){
				jsonDatas.put("status", 1);//设置状态为1，表示操作成功
			}
		} catch (Exception e) {
			log.error("添加失败", e);
		}
		return jsonDatas;//返回存放json数据的集合,最终给springmvc转换为json数据输出给浏览器
	}
	
	/**
	 * 初始化修改
	 * @param id 待修改的系统提示信息的编号
	 * @return
	 */
	@RequestMapping("/update")
	public @ResponseBody SysTips update(@RequestParam(name="id",required=true) Integer id){
		log.info("开始初始化编号为"+id+"的系统提示信息数据供前端修改");//把id放到日志
		SysTips sysTips=null;
		try {
			//通过id去获取系统参数信息
			sysTips=sysTipsService.get(id);
		} catch (Exception e) {
			log.error("初始化修改失败", e);
		}
		return sysTips;//返回功能对象,最终给springmvc转换为json数据输出给浏览器
	}
	
	/**
	 * 保存修改
	 * @param sysTips
	 * @return
	 */
	@RequestMapping("/updateSave")
	public @ResponseBody Map updateSave(SysTips sysTips){
		log.info("接收到页面修改的数据："+sysTips);//把sysTips放到日志
		Map jsonDatas=new HashMap();//存放json数据的集合
		jsonDatas.put("status", 0);//默认状态为0，表示操作失败
		
		try {
			
			SysTips oldsysTips=sysTipsService.get(sysTips.getId());
		
			oldsysTips.setScode(sysTips.getScode());
			oldsysTips.setSdesc(sysTips.getSdesc());
			
			//调用修改的方法，修改原先的信息
			boolean flag = sysTipsService.update(oldsysTips);
			if(flag){
				jsonDatas.put("status", 1);//设置状态为1，表示操作成功
			}
		} catch (Exception e) {
			log.error("修改失败", e);
		}
		
		return jsonDatas;//返回存放json数据的集合,最终给springmvc转换为json数据输出给浏览器
	}
	
	/**
	 * 删除系统提示信息
	 *@param id 待删除的系统提示信息的编号
	 */
	@RequestMapping("/delete")
	public @ResponseBody Map delete(@RequestParam(name="id",required=true) Integer id){
		log.info("开始删除编号为"+id+"的系统参数");//把id放到日志
		Map jsonDatas=new HashMap();//存放json数据的集合
		jsonDatas.put("status", 0);//默认状态为0，表示操作失败
		try {
			//通过id去删除数据
			boolean flag = sysTipsService.delete(id);
			if(flag){
				jsonDatas.put("status", 1);//设置状态为1，表示操作成功
			}
		} catch (Exception e) {
			log.error("删除失败", e);
		}
		return jsonDatas;//返回存放json数据的集合,最终给springmvc转换为json数据输出给浏览器
	}
	
	/**
	 * 系统提示信息管理
	 */
	@RequestMapping("/select")
	public ModelAndView select(){
		log.info("进入系统提示信息管理页面");
		ModelAndView mv=new ModelAndView();//创建视图模型对象
		mv.setViewName("sysTips/SysTipsManager");//设置显示页面
		return mv;//返回视图模型对象
	}
	
	/**
	 * 系统提示信息管理表格数据
	 * @param sysTips 查询条件
	 * @param page 页码
	 * @param rows 每一页行数
	 * @return
	 */
	@RequestMapping("/selectDatas")
	public @ResponseBody Map selectDatas(SysTips sysTips,
			//page当前是第几页
			@RequestParam(value="page",required=false)int page,
			//rows每页几条数据
			@RequestParam(value="rows",required=false)int rows){
		
		log.info("开始获取系统提示信息管理表格数据");
		Map jsonDatas=new HashMap();//待返回json集合数据;
		try {
			//分页处理
			Page<SysTips> pageObj = sysTipsService.getList(sysTips,page,rows);
			//取记录总条数
			jsonDatas.put("total", pageObj.getTotal());//总数
			jsonDatas.put("rows", pageObj.getResultlist());//前端需要的行数据
		} catch (Exception e) {
			log.error("获取系统提示信息管理表格数据失败",e);
		}
		return jsonDatas;//返回存放数据的集合,最终给springmvc转换为json数据输出给浏览器
	}
	
}

