package com.examsys.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.examsys.po.UserGroups;
import com.examsys.service.UserGroupsService;
import com.examsys.util.Page;
/**
 * 用户组控制层
 *
 */
@Controller
@RequestMapping("/userGroups")
public class UserGroupController {

	//log4j日志对象
	public Logger log=Logger.getLogger(this.getClass());
	
	@Resource //用@Resource注解，告诉spring这里需要把业务逻辑层对象注入进来(IoC,DI)
	private UserGroupsService userGroupsService;
	
	/**
	 * 保存添加
	 * @param userGroups
	 * @return
	 */
	@RequestMapping("/addSave")
	public @ResponseBody Map addSave(UserGroups userGroups){
		log.info("接收到页面添加的数据："+userGroups);//把userGroups放到日志
		Map jsonDatas=new HashMap();//存放json数据的集合
		jsonDatas.put("status", 0);//默认状态为0，表示操作失败
		
		try {
			boolean flag = userGroupsService.add(userGroups);
			if(flag){
				jsonDatas.put("status", 1);//设置状态为1，表示操作成功
			}
		} catch (Exception e) {
			log.error("添加失败", e);
		}
		return jsonDatas;//返回存放json数据的集合,最终给springmvc转换为json数据输出给浏览器
	}
	
	/**
	 * 初始化修改
	 * @param id
	 * @return
	 */
	@RequestMapping("/update")
	public @ResponseBody UserGroups update(@RequestParam(name="id",required=true) Integer id){
		log.info("开始初始化编号为"+id+"的用户组数据供前端修改");//把id放到日志
		UserGroups userGroups=null;
		try {
			//通过id去获取用户组
			userGroups=userGroupsService.get(id);
		} catch (Exception e) {
			log.error("初始化修改失败", e);
		}
		return userGroups;//返回功能对象,最终给springmvc转换为json数据输出给浏览器
	}
	
	/**
	 * 保存修改
	 * @param userGroups
	 * @return
	 */
	@RequestMapping("/updateSave")
	public @ResponseBody Map updateSave(UserGroups userGroups){
		log.info("接收到页面修改的数据："+userGroups);//把userGroups放到日志
		Map jsonDatas=new HashMap();//存放json数据的集合
		jsonDatas.put("status", 0);//默认状态为0，表示操作失败
		
		try {
			//通过id去获取用户组
			UserGroups groups=userGroupsService.get(userGroups.getId());
			groups.setGroup_name(userGroups.getGroup_name());
			groups.setRemark(userGroups.getRemark());
			
			//调用修改的方法，修改原先的信息
			boolean flag = userGroupsService.update(userGroups);
			if(flag){
				jsonDatas.put("status", 1);//设置状态为1，表示操作成功
			}
		} catch (Exception e) {
			log.error("修改失败", e);
		}
		
		return jsonDatas;//返回存放json数据的集合,最终给springmvc转换为json数据输出给浏览器
	}
	
	/**
	 * 删除用户组
	 * @param id 待删除的用户组的编号
	 * @return
	 */
	@RequestMapping("/delete")
	public @ResponseBody Map delete(@RequestParam(name="id",required=true) Integer id){
		log.info("开始删除编号为"+id+"的系统参数");//把id放到日志
		Map jsonDatas=new HashMap();//存放json数据的集合
		jsonDatas.put("status", 0);//默认状态为0，表示操作失败
		try {
			//通过id去删除数据
			boolean flag = userGroupsService.delete(id);
			if(flag){
				jsonDatas.put("status", 1);//设置状态为1，表示操作成功
			}
		} catch (Exception e) {
			log.error("删除失败", e);
		}
		return jsonDatas;//返回存放json数据的集合,最终给springmvc转换为json数据输出给浏览器
	}
	
	/**
	 * 用户组管理
	 * @return
	 */
	@RequestMapping("/select")
	public ModelAndView select(){
		log.info("进入用户组管理页面");
		ModelAndView mv=new ModelAndView();//创建视图模型对象
		mv.setViewName("userGroups/UserGroupsManager");//设置显示页面
		return mv;//返回视图模型对象
	}
	
	/**
	 * 用户组管理表格数据
	 * @param userGroups 查询条件
	 * @param page 页码
	 * @param rows 每一页行数
	 * @return
	 */
	@RequestMapping("/selectDatas")
	public @ResponseBody Map selectDatas(UserGroups userGroups,
			//page当前是第几页
			@RequestParam(value="page",required=false)int page,
			//rows每页几条数据
			@RequestParam(value="rows",required=false)int rows){
		
		log.info("开始获取用户组管理表格数据");
		Map jsonDatas=new HashMap();//待返回json集合数据;
		try {
			//分页处理
			Page<UserGroups> pageObj = userGroupsService.getList(userGroups,page,rows);
			//取记录总条数
			jsonDatas.put("total", pageObj.getTotal());//总数
			jsonDatas.put("rows", pageObj.getResultlist());//前端需要的行数据
		} catch (Exception e) {
			log.error("获取用户组管理表格数据失败",e);
		}
		return jsonDatas;//返回存放数据的集合,最终给springmvc转换为json数据输出给浏览器
	}
}



