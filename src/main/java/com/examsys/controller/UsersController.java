package com.examsys.controller;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.examsys.po.UserGroups;
import com.examsys.po.Users;
import com.examsys.service.UserGroupsService;
import com.examsys.service.UsersService;
import com.examsys.util.Page;

/**
 * 会员或考试者管理控制层
 *
 */
@Controller
@RequestMapping("/users")
public class UsersController {
	//log4j日志对象
	private Logger log=Logger.getLogger(this.getClass());
	
	@Resource //用@Resource注解，告诉spring这里需要把业务逻辑层对象注入进来(IoC,DI)
	private UsersService usersService;
	@Resource //用@Resource注解，告诉spring这里需要把业务逻辑层对象注入进来(IoC,DI)
	private UserGroupsService userGroupsService;
	
	/**
	 * 初始化用户组数据
	 * @return
	 */
	@RequestMapping("/userGroupsDatas")
	public @ResponseBody List<UserGroups> userGroupsDatas(){
		log.info("获取用户组下拉框数据");
		List<UserGroups> usersGroupsList=null;
		try {
			usersGroupsList=userGroupsService.getList();
		} catch (Exception e) {
			log.error("获取用户组下拉框数据失败",e);
		}
		return usersGroupsList;//返回存放数据的集合,最终给springmvc转换为json数据输出给浏览器
	}
	
	/**
	 * 保存添加
	 * @param users
	 * @param group_id
	 * @return
	 */
	@RequestMapping("/addSave")
	public @ResponseBody Map addSave(Users users,
			@RequestParam(name="group_id",required=true) Integer group_id){
		log.info("接收到页面添加的数据："+users);//把users放到日志
		Map jsonDatas=new HashMap();//存放json数据的集合
		jsonDatas.put("status", 0);//默认状态为0，表示操作失败
		
		try {
			UserGroups userGroups=userGroupsService.get(group_id);
			users.setUserGroups(userGroups);
			users.setCreate_date(new Date(System.currentTimeMillis()));//创建时间
			users.setLogin_date(new Date(System.currentTimeMillis()));
			users.setLogin_times(0);//登录次数
			
			boolean flag = usersService.add(users);
			if(flag){
				jsonDatas.put("status", 1);//设置状态为1，表示操作成功
			}
		} catch (Exception e) {
			log.error("添加失败", e);
		}
		return jsonDatas;//返回存放json数据的集合,最终给springmvc转换为json数据输出给浏览器
		
	}
	
	/**
	 * 初始化修改
	 * @param id 待修改的用户的编号
	 * @return
	 */
	@RequestMapping("/update")
	public Users update(@RequestParam(name="id",required=true) Integer id){
		log.info("开始初始化编号为"+id+"的用户数据供前端修改");//把id放到日志
		Users users=null;
		try {
			users=usersService.get(id);
		} catch (Exception e) {
			log.error("初始化修改失败", e);
		}
		return users;
	}
	
	/**
	 * 保存修改
	 * @param users 用户信息对象
	 * @param group_id 用户组
	 * @return
	 */
	@RequestMapping("/updateSave")
	public @ResponseBody Map updateSave(Users users,
			@RequestParam(name="group_id",required=true) Integer group_id){
		log.info("接收到页面修改的数据："+users);//把users放到日志
		Map jsonDatas=new HashMap();//存放json数据的集合
		jsonDatas.put("status", 0);//默认状态为0，表示操作失败
		
		try {
			Users oldusers=usersService.get(users.getId());
			oldusers.setUser_name(users.getUser_name());
			oldusers.setUser_pass(users.getUser_pass());
			oldusers.setUser_no(users.getUser_no());
			oldusers.setReal_name(users.getReal_name());
			oldusers.setEmail(users.getEmail());
			oldusers.setPhone(users.getPhone());
			oldusers.setStatus(users.getStatus());
			oldusers.setRemark(users.getRemark());
			
			UserGroups userGroups = userGroupsService.get(group_id);
			oldusers.setUserGroups(userGroups);
			
			boolean flag = usersService.update(oldusers);
			if(flag){
				jsonDatas.put("status", 1);//设置状态为1，表示操作成功
			}
		} catch (Exception e) {
			log.error("修改失败", e);
		}
		return jsonDatas;//返回存放json数据的集合,最终给springmvc转换为json数据输出给浏览器
		
	}
	
	/**
	 * 删除会员或考试者
	 * @param id 待删除的考试者的编号
	 * @return
	 */
	@RequestMapping("/delete")
	public @ResponseBody Map delete(@RequestParam(name="id",required=true) Integer id){
		log.info("开始删除编号为"+id+"的系统参数");//把id放到日志
		Map jsonDatas=new HashMap();//存放json数据的集合
		jsonDatas.put("status", 0);//默认状态为0，表示操作失败
		try {
			//通过id去删除数据
			boolean flag = usersService.delete(id);
			if(flag){
				jsonDatas.put("status", 1);//设置状态为1，表示操作成功
			}
			
		} catch (Exception e) {
			log.error("删除失败", e);
		}
		return jsonDatas;
	}
	
	/**
	 * 会员或者考试者管理
	 */
	@RequestMapping("/select")
	public ModelAndView select(){
		log.info("进入会员或者考试者管理页面");
		ModelAndView mv=new ModelAndView();//创建视图模型对象
		mv.setViewName("users/usersManager");//设置显示页面
		return mv;//返回视图模型对象
	}
	
	/**
	 * 会员或者考试者管理表格数据
	 * @param users 查询条件
	 * @param page 页码
	 * @param rows 每一页行数
	 * @return
	 */
	@RequestMapping("/selectDatas")
	public @ResponseBody Map selectDatas(Users users,
			//page当前是第几页
			@RequestParam(value="page",required=false)int page,
			//rows每页几条数据
			@RequestParam(value="rows",required=false)int rows){
		
		log.info("开始获取会员或者考试者管理表格数据");
		Map jsonDatas=new HashMap();//待返回json集合数据;
		try {
			//分页处理
			Page<Users> pageObj = usersService.getList(users,page,rows);
			//取记录总条数
			jsonDatas.put("total", pageObj.getTotal());//总数
			jsonDatas.put("rows", pageObj.getResultlist());//前端需要的行数据
		} catch (Exception e) {
			log.error("获取会员或者考试者管理表格数据失败",e);
		}
		return jsonDatas;//返回存放数据的集合,最终给springmvc转换为json数据输出给浏览器
	}
}


