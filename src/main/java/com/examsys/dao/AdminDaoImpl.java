package com.examsys.dao;

import java.util.List;

import com.examsys.po.Admin;
import com.examsys.util.Page;

/**
 * 管理员数据访问层实现类
 * @author edu-1
 *
 */
public class AdminDaoImpl extends AbstractBaseDao<Admin, Integer> implements AdminDao {

	/**
	 * 删除管理员
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public void delete(Integer id) throws Exception {
		Admin admin = this.get(Admin.class, id);
		this.delete(admin);
	}

	/**
	 * 获取管理员
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public Admin get(Integer id) throws Exception {
		return this.get(Admin.class, id);
	}

	/**
	 * 获得管理员表所有记录
	 */
	@Override
	public List<Admin> getList() throws Exception {
		//构建查询语句
		String sql="FROM Admin";
		List<Admin> list = this.query(sql, new Object[]{});
		return list;
	}

	/**
	 * 带条件获取管理员记录
	 */
	@Override
	public List<Admin> getList(Admin obj) throws Exception {
		//构建查询语句
		String sql="FROM Admin a WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getUser_name()!=null&&!"".equals(obj.getUser_name())){
				sql+=" AND a.user_name LIKE '%"+obj.getUser_name()+"%'";
			}
			
			if(obj.getAdminRoles()!=null&&obj.getAdminRoles().getId()!=null&&obj.getAdminRoles().getId()!=0){
				sql+=" AND a.adminRoles.id="+obj.getAdminRoles().getId();
			}
		}
		
		List<Admin> list = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		return list;
	}
	
	
	/**
	 *通过用户名去获取管理员 
	 * @param user_name
	 * @return
	 */
	public Admin getAdminByUserName(String userName) throws Exception{
		//构建查询语句
		String sql="FROM Admin a WHERE a.user_name=?";
		Admin admin = this.uniqueQuery(sql, new Object[]{userName});//调用父类的查询方法拿数据
		return admin;
	}

	@Override
	public Page<Admin> getList(Admin obj, Integer page, Integer limit) throws Exception {
		//构建查询语句
		String sql="FROM Admin a WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getUser_name()!=null&&!"".equals(obj.getUser_name())){
				sql+=" AND a.user_name LIKE '%"+obj.getUser_name()+"%'";
			}
			
			if(obj.getAdminRoles()!=null&&obj.getAdminRoles().getId()!=null&&obj.getAdminRoles().getId()!=0){
				sql+=" AND a.adminRoles.id="+obj.getAdminRoles().getId();
			}
		}
		
		Page<Admin> pageObj = this.searchForPager(sql, page, limit);//调用父类的查询方法拿数据
		return pageObj;
	}

}
