package com.examsys.dao;

import java.util.List;

import com.examsys.po.AdminRoles;
import com.examsys.util.Page;
/**
 * 管理员角色数据访问层实现类
 * @author edu-1
 *
 */
public class AdminRolesDaoImpl extends AbstractBaseDao<AdminRoles,Integer> implements AdminRolesDao{

	/**
	 * 删除管理员角色
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public void delete(Integer id) throws Exception {

		AdminRoles adminRoles=this.get(AdminRoles.class,id);
		this.delete(adminRoles);
	}

	/**
	 * 获取管理员角色
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public AdminRoles get(Integer id) throws Exception {
		return this.get(AdminRoles.class,id);
	}

	/**
	 * 获得管理员角色表所有记录
	 */
	@Override
	public List<AdminRoles> getList() throws Exception {
		//构建查询语句
		String sql="FROM AdminRoles";//这是hql语句，from的是对象名而不是sql中的表名
		List<AdminRoles> list = this.query(sql, new Object[]{});//调用AbstractBaseDao抽象类中的query查找方法
		return list;
	}

	/**
	 * 带条件获取管理员角色记录
	 */
	@Override
	public List<AdminRoles> getList(AdminRoles obj) throws Exception {
		//构建查询语句
		String sql="FROM AdminRoles a WHERE 1=1";
		
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();//注意id不能大写，因为是属性而不是数据库表的对象名，属性名是实体类中有的，且是小写
			}
			
			if(obj.getRole_name()!=null&&!"".equals(obj.getRole_name())){
				sql+=" AND a.role_name LIKE '%"+obj.getRole_name()+"%'";
			}
		}
		
		List<AdminRoles> list = this.query(sql, new Object[]{});	
		return list;
	}

	@Override
	public Page<AdminRoles> getList(AdminRoles obj, Integer page, Integer limit) throws Exception {
		//构建查询语句
		String sql="FROM AdminRoles a WHERE 1=1";
		
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();//注意id不能大写，因为是属性而不是数据库表的对象名，属性名是实体类中有的，且是小写
			}
			
			if(obj.getRole_name()!=null&&!"".equals(obj.getRole_name())){
				sql+=" AND a.role_name LIKE '%"+obj.getRole_name()+"%'";
			}
		}
		
		Page<AdminRoles> pageObj = this.searchForPager(sql, page, limit);	
		return pageObj;
	}

}
