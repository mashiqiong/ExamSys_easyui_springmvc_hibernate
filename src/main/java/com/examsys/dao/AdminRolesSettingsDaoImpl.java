package com.examsys.dao;

import java.util.List;

import com.examsys.po.AdminRolesSettings;
import com.examsys.util.Page;
/**
 * 系统功能数据访问层实现类
 * @author edu-1
 *
 */
public class AdminRolesSettingsDaoImpl extends AbstractBaseDao<AdminRolesSettings, Integer> implements AdminRolesSettingsDao {


	/**
	 * 删除系统功能
	 * @param id 编号
	 */
	@Override
	public void delete(Integer id) throws Exception {
		AdminRolesSettings adminRolesSettings=this.get(AdminRolesSettings.class,id);
		this.delete(adminRolesSettings);
	}

	/**
	 * 获取系统功能
	 * @param id 编号
	 */
	@Override
	public AdminRolesSettings get(Integer id) throws Exception {
		return this.get(AdminRolesSettings.class,id);//包裝有這個類的原始數據對象
	}

	/**
	 * 获取所有系统功能所有记录
	 */
	@Override
	public List<AdminRolesSettings> getList() throws Exception {
		//构建查询语句
		String sql="from AdminRolesSettings";
		List<AdminRolesSettings> list = this.query(sql, new Object[]{});
		return list;
	}

	/**
	 * 带条件获取所有系统功能记录
	 */
	@Override
	public List<AdminRolesSettings> getList(AdminRolesSettings obj) throws Exception {
		//构建查询语句
		String sql="from AdminRolesSettings a where 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getName()!=null&&!"".equals(obj.getName())){
				sql+=" AND a.name like '%"+obj.getName()+"%'";
			}
			
			if(obj.getCode()!=null&&!"".equals(obj.getCode())){
				sql+=" AND a.code like '%"+obj.getCode()+"%'";
			}
			
			if(obj.getAdminRolesSettings()!=null&&obj.getAdminRolesSettings().getId()!=null){
				sql+=" AND a.adminRolesSettings.id="+obj.getAdminRolesSettings().getId();
			}
		}

		List<AdminRolesSettings> list = this.query(sql, new Object[]{});
		return list;
	}

	@Override
	public Page<AdminRolesSettings> getList(AdminRolesSettings obj, Integer page, Integer limit) throws Exception {
		//构建查询语句
		String sql="from AdminRolesSettings a where 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getName()!=null&&!"".equals(obj.getName())){
				sql+=" AND a.name like '%"+obj.getName()+"%'";
			}
			
			if(obj.getCode()!=null&&!"".equals(obj.getCode())){
				sql+=" AND a.code like '%"+obj.getCode()+"%'";
			}
			
			if(obj.getAdminRolesSettings()!=null&&obj.getAdminRolesSettings().getId()!=null){
				sql+=" AND a.adminRolesSettings.id="+obj.getAdminRolesSettings().getId();
			}
		}
		Page<AdminRolesSettings> pageObj = this.searchForPager(sql, page, limit);
		return pageObj;
	}

}
