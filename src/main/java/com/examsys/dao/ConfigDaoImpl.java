package com.examsys.dao;

import java.util.List;

import com.examsys.po.Config;
import com.examsys.util.Page;
/**
 * 系统参数数据访问层实现类
 * @author edu-1
 *
 */
public class ConfigDaoImpl extends AbstractBaseDao<Config, Integer> implements ConfigDao {

	/**
	 * 删除系统参数
	 */
	@Override
	public void delete(Integer id) throws Exception {
		Config config=this.get(Config.class,id);
		this.delete(config);
	}

	/**
	 * 获取系统参数
	 */
	@Override
	public Config get(Integer id) throws Exception {
		return this.get(Config.class,id);
	}

	/**
	 * 获取所有系统参数记录
	 */
	@Override
	public List<Config> getList() throws Exception {
		//构建查询语句
		String sql="FROM Config";
		List<Config> list = this.query(sql, new Object[]{});
		return list;
	}

	/**
	 * 带条件获取系统参数记录
	 */
	@Override
	public List<Config> getList(Config obj) throws Exception {
		//构建查询语句
		String sql="FROM Config a WHERE 1=1";
		
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getName()!=null&&!"".equals(obj.getName())){
				sql+=" AND a.name LIKE '%"+obj.getName()+"%'";
			}
		}
		
		List<Config> list = this.query(sql, new Object[]{});
		return list;
	}

	@Override
	public Page<Config> getList(Config obj, Integer page, Integer limit) throws Exception {
		//构建查询语句
		String sql="FROM Config a WHERE 1=1";
		
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getName()!=null&&!"".equals(obj.getName())){
				sql+=" AND a.name LIKE '%"+obj.getName()+"%'";
			}
		}
		
		Page<Config> pageObj = this.searchForPager(sql, page, limit);
		return pageObj;
	}

}
