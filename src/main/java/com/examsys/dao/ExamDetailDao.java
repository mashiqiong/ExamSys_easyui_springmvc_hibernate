package com.examsys.dao;

import com.examsys.po.ExamDetail;
/**
 * 考试答题卡明细数据访问层接口
 * @author edu-1
 *
 */
public interface ExamDetailDao extends IBaseDao<ExamDetail, Integer> {

}
