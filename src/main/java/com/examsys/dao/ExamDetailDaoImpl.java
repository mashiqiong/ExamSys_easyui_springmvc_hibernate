package com.examsys.dao;

import java.util.List;

import com.examsys.po.ExamDetail;
import com.examsys.util.Page;

/**
 * 考试答题卡明细数据访问层实现类
 * @author edu-1
 *
 */
public class ExamDetailDaoImpl extends AbstractBaseDao<ExamDetail, Integer> implements ExamDetailDao {

	/**
	 * 删除考试答题卡明细
	 * @param id 编号
	 */
	@Override
	public void delete(Integer id) throws Exception {

		ExamDetail detail=this.get(ExamDetail.class,id);
		this.delete(detail);
	
	}

	/**
	 * 获取考试答题卡明细
	 * @param id 编号
	 */
	@Override
	public ExamDetail get(Integer id) throws Exception {
		//构建查询语句
		return this.get(ExamDetail.class,id);
		
	}

	/**
	 * 获取所有考试答题卡明细
	 */
	@Override
	public List<ExamDetail> getList() throws Exception {
		//构建查询语句
		String sql="from ExamDetail";
		List<ExamDetail> list = this.query(sql, new Object[]{});
		return list;
	}

	/**
	 * 带条件获取考试答题卡明细
	 */
	@Override
	public List<ExamDetail> getList(ExamDetail obj) throws Exception {
		//构建查询语句
		String sql="from ExamDetail a WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getQuestion()!=null&&obj.getQuestion().getId()!=null
					&&obj.getQuestion().getId()!=0){
				sql+=" AND a.question.id="+obj.getQuestion().getId();
			
			}
			
			if(obj.getExamMain()!=null&&obj.getExamMain().getId()!=null
					&&obj.getExamMain().getId()!=0){
				sql+=" AND a.examMain.id="+obj.getExamMain().getId();
			}
		}
		List<ExamDetail> list = this.query(sql, new Object[]{});
		return list;
	}

	@Override
	public Page<ExamDetail> getList(ExamDetail obj, Integer page, Integer limit) throws Exception {
		//构建查询语句
		String sql="from ExamDetail a WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getQuestion()!=null&&obj.getQuestion().getId()!=null
					&&obj.getQuestion().getId()!=0){
				sql+=" AND a.question.id="+obj.getQuestion().getId();
			
			}
			
			if(obj.getExamMain()!=null&&obj.getExamMain().getId()!=null
					&&obj.getExamMain().getId()!=0){
				sql+=" AND a.examMain.id="+obj.getExamMain().getId();
			}
		}
		Page<ExamDetail> pageObj = this.searchForPager(sql, page, limit);
		return pageObj;
	}

}
