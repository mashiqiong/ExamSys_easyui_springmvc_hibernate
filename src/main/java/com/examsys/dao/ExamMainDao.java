package com.examsys.dao;

import com.examsys.po.ExamMain;
/**
 * 考生答题卡数据访问层接口
 * @author edu-1
 *
 */
public interface ExamMainDao extends IBaseDao<ExamMain, Integer> {

}
