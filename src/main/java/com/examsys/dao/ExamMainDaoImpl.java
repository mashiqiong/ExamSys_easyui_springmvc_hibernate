package com.examsys.dao;

import java.util.List;

import com.examsys.po.ExamMain;
import com.examsys.util.Page;

/**
 * 考生答题卡数据访问层实现类
 * @author edu-1
 *
 */
public class ExamMainDaoImpl extends AbstractBaseDao<ExamMain, Integer> implements ExamMainDao {

	/**
	 * 删除考生答题卡
	 */
	@Override
	public void delete(Integer id) throws Exception {
		
		ExamMain examMain=this.get(ExamMain.class,id);
		this.delete(examMain);
	}

	/**
	 * 获取考生答题卡
	 * @param id 编号
	 */
	@Override
	public ExamMain get(Integer id) throws Exception {
		return this.get(ExamMain.class,id);
	}

	/**
	 * 获取所有考生答题卡记录
	 */
	@Override
	public List<ExamMain> getList() throws Exception {
		//构建查询语句
		String sql="from ExamMain";
		List<ExamMain> list = this.query(sql, new Object[]{});
		return list;
	}

	/**
	 * 带条件获取考生答题卡
	 */
	@Override
	public List<ExamMain> getList(ExamMain obj) throws Exception {
		//构建查询语句
		String sql="from ExamMain a WHERE 1=1";
		
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getUsers()!=null&&obj.getUsers().getId()!=null
					&&obj.getUsers().getId()!=0){
				sql+=" AND a.users.id="+obj.getUsers().getId();
			}
			
			if(obj.getUsers()!=null&&obj.getUsers().getUser_name()!=null
					&&!"".equals(obj.getUsers().getUser_name())){
				sql+=" AND a.users.user_name='"+obj.getUsers().getUser_name()+"'";
			}
			
			if(obj.getUsers()!=null&&obj.getUsers().getUser_no()!=null
					&&!"".equals(obj.getUsers().getUser_no())){
				sql+=" AND a.users.user_no='"+obj.getUsers().getUser_no()+"'";
			}
			
			if(obj.getUsers()!=null&&obj.getUsers().getReal_name()!=null
					&&!"".equals(obj.getUsers().getReal_name())){
				sql+=" AND a.users.real_name='"+obj.getUsers().getReal_name()+"'";
			}
			
			if(obj.getPaper()!=null&&obj.getPaper().getId()!=null
					&&obj.getPaper().getId()!=0){
				sql+=" AND a.paper.id="+obj.getPaper().getId();
			}
			
			if(obj.getPaper()!=null&&obj.getPaper().getPaper_name()!=null
					&&!"".equals(obj.getPaper().getPaper_name())){
				sql+=" AND a.paper.paper_name='"+obj.getPaper().getPaper_name()+"'";
			}
			
		}
		
		List<ExamMain> list = this.query(sql, new Object[]{});
		return list;
	}

	@Override
	public Page<ExamMain> getList(ExamMain obj, Integer page, Integer limit) throws Exception {
		//构建查询语句
		String sql="from ExamMain a WHERE 1=1";
		
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getUsers()!=null&&obj.getUsers().getId()!=null
					&&obj.getUsers().getId()!=0){
				sql+=" AND a.users.id="+obj.getUsers().getId();
			}
			
			if(obj.getUsers()!=null&&obj.getUsers().getUser_name()!=null
					&&!"".equals(obj.getUsers().getUser_name())){
				sql+=" AND a.users.user_name='"+obj.getUsers().getUser_name()+"'";
			}
			
			if(obj.getUsers()!=null&&obj.getUsers().getUser_no()!=null
					&&!"".equals(obj.getUsers().getUser_no())){
				sql+=" AND a.users.user_no='"+obj.getUsers().getUser_no()+"'";
			}
			
			if(obj.getUsers()!=null&&obj.getUsers().getReal_name()!=null
					&&!"".equals(obj.getUsers().getReal_name())){
				sql+=" AND a.users.real_name='"+obj.getUsers().getReal_name()+"'";
			}
			
			if(obj.getPaper()!=null&&obj.getPaper().getId()!=null
					&&obj.getPaper().getId()!=0){
				sql+=" AND a.paper.id="+obj.getPaper().getId();
			}
			
			if(obj.getPaper()!=null&&obj.getPaper().getPaper_name()!=null
					&&!"".equals(obj.getPaper().getPaper_name())){
				sql+=" AND a.paper.paper_name='"+obj.getPaper().getPaper_name()+"'";
			}
			
		}
		
		Page<ExamMain> pageObj = this.searchForPager(sql, page, limit);
		return pageObj;
	}

}
