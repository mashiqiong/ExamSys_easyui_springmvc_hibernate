package com.examsys.dao;

import com.examsys.po.Onlines;
/**
 * 考试者在线考试的时间记录数据访问层接口
 * @author edu-1
 *
 */
public interface OnlinesDao extends IBaseDao<Onlines, Integer> {

}
