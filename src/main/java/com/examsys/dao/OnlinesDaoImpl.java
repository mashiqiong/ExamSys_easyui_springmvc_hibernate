package com.examsys.dao;

import java.util.Date;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.examsys.po.Onlines;
import com.examsys.po.Paper;
import com.examsys.po.Users;
import com.examsys.util.Page;

/**
 * 考试者在线考试的时间记录数据访问层接口
 * @author edu-1
 *
 */
public class OnlinesDaoImpl extends AbstractBaseDao<Onlines, Integer> implements OnlinesDao {

	
	/**
	 * 删除考试者在线考试的时间记录
	 */
	@Override
	public void delete(Integer id) throws Exception {
		Onlines onlines=this.get(Onlines.class,id);
		this.delete(onlines);
	}

	/**
	 * 获取考试者在线考试的时间记录
	 */
	@Override
	public Onlines get(Integer id) throws Exception {
		return this.get(Onlines.class,id);
	}

	/**
	 * 获取所有考试者在线考试的时间记录
	 */
	@Override
	public List<Onlines> getList() throws Exception {
		//构建查询语句
		String sql="from Onlines";
		List<Onlines> list = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		return list;
	}

	/**
	 * 带条件获取考试者在线考试的时间记录
	 */
	@Override
	public List<Onlines> getList(Onlines obj) throws Exception {
		//构建查询语句
		String sql="from Onlines a WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getUsers()!=null&&obj.getUsers().getId()!=null
					&&obj.getUsers().getId()!=0){
				sql+=" AND a.users.id="+obj.getUsers().getId();
			}
			
			if(obj.getUsers()!=null&&obj.getUsers().getUser_name()!=null
					&&!"".equals(obj.getUsers().getUser_name())){
				sql+=" AND a.users.user_name='"+obj.getUsers().getUser_name()+"'";
			}
			
			if(obj.getUsers()!=null&&obj.getUsers().getUser_no()!=null
					&&!"".equals(obj.getUsers().getUser_no())){
				sql+=" AND a.users.user_no='"+obj.getUsers().getUser_no()+"'";
			}
			
			if(obj.getUsers()!=null&&obj.getUsers().getReal_name()!=null
					&&!"".equals(obj.getUsers().getReal_name())){
				sql+=" AND a.users.real_name='"+obj.getUsers().getReal_name()+"'";
			}
			
			if(obj.getPaper()!=null&&obj.getPaper().getId()!=null
					&&obj.getPaper().getId()!=0){
				sql+=" AND a.paper.id="+obj.getPaper().getId();
			}
			
			if(obj.getPaper()!=null&&obj.getPaper().getPaper_name()!=null
					&&!"".equals(obj.getPaper().getPaper_name())){
				sql+=" AND a.paper.paper_name='"+obj.getPaper().getPaper_name()+"'";
			}
			
		}

		List<Onlines> list = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		return list;
	}

	@Override
	public Page<Onlines> getList(Onlines obj, Integer page, Integer limit) throws Exception {
		//构建查询语句
		String sql="from Onlines a WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getUsers()!=null&&obj.getUsers().getId()!=null
					&&obj.getUsers().getId()!=0){
				sql+=" AND a.users.id="+obj.getUsers().getId();
			}
			
			if(obj.getUsers()!=null&&obj.getUsers().getUser_name()!=null
					&&!"".equals(obj.getUsers().getUser_name())){
				sql+=" AND a.users.user_name='"+obj.getUsers().getUser_name()+"'";
			}
			
			if(obj.getUsers()!=null&&obj.getUsers().getUser_no()!=null
					&&!"".equals(obj.getUsers().getUser_no())){
				sql+=" AND a.users.user_no='"+obj.getUsers().getUser_no()+"'";
			}
			
			if(obj.getUsers()!=null&&obj.getUsers().getReal_name()!=null
					&&!"".equals(obj.getUsers().getReal_name())){
				sql+=" AND a.users.real_name='"+obj.getUsers().getReal_name()+"'";
			}
			
			if(obj.getPaper()!=null&&obj.getPaper().getId()!=null
					&&obj.getPaper().getId()!=0){
				sql+=" AND a.paper.id="+obj.getPaper().getId();
			}
			
			if(obj.getPaper()!=null&&obj.getPaper().getPaper_name()!=null
					&&!"".equals(obj.getPaper().getPaper_name())){
				sql+=" AND a.paper.paper_name='"+obj.getPaper().getPaper_name()+"'";
			}
			
		}

		Page<Onlines> pageObj = this.searchForPager(sql, page, limit);//调用父类的查询方法拿数据
		return pageObj;
	}

}
