package com.examsys.dao;

import com.examsys.po.Paper;
/**
 * 试卷数据访问层接口
 * @author edu-1
 *
 */
public interface PaperDao extends IBaseDao<Paper, Integer> {
	public Integer getSeq() throws Exception;//获取试题系列
}
