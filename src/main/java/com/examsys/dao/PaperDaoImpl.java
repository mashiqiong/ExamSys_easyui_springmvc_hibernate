package com.examsys.dao;

import java.util.List;

import com.examsys.po.Paper;
import com.examsys.util.Page;

/**
 * 试卷数据访问层实现类
 * @author edu-1
 *
 */
public class PaperDaoImpl extends AbstractBaseDao<Paper, Integer> implements PaperDao {

	
	/**
	 * 删除试卷
	 * @param id 编号
	 */
	@Override
	public void delete(Integer id) throws Exception {
		Paper paper=this.get(Paper.class,id);
		this.delete(paper);
	}

	/**
	 * 获取试卷
	 * @param id 编号
	 */
	@Override
	public Paper get(Integer id) throws Exception {
		return this.get(Paper.class,id);
	}

	/**
	 * 获取所有试卷
	 */
	@Override
	public List<Paper> getList() throws Exception {
		//构建查询语句
		String sql="from Paper";
		List<Paper> list = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		return list;
	}

	/**
	 * 带条件获取试卷
	 */
	@Override
	public List<Paper> getList(Paper obj) throws Exception {
		//构建查询语句
		String sql="from Paper a WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getAdmin()!=null&&obj.getAdmin().getId()!=null
					&&obj.getAdmin().getId()!=0){
				sql+=" AND a.admin.id="+obj.getAdmin().getId();
			}
			
			if(obj.getPaper_name()!=null&&!"".equals(obj.getPaper_name())){
				sql+=" AND a.paper_name LIKE '%"+obj.getPaper_name()+"%'";
			}
			
		}
		List<Paper> list = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		return list;
	}

	/**
	 * 获得系列值的方法
	 */
	@Override
	public Integer getSeq() throws Exception {
		String sql="select PAPER_ID_SEQ.nextval SEQ from dual";
		return this.getSeq(sql);
	}

	@Override
	public Page<Paper> getList(Paper obj, Integer page, Integer limit) throws Exception {
		//构建查询语句
		String sql="from Paper a WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getAdmin()!=null&&obj.getAdmin().getId()!=null
					&&obj.getAdmin().getId()!=0){
				sql+=" AND a.admin.id="+obj.getAdmin().getId();
			}
			
			if(obj.getPaper_name()!=null&&!"".equals(obj.getPaper_name())){
				sql+=" AND a.paper_name LIKE '%"+obj.getPaper_name()+"%'";
			}
			
		}
		Page<Paper> pageObj = this.searchForPager(sql, page, limit);//调用父类的查询方法拿数据
		return pageObj;
	}
}
