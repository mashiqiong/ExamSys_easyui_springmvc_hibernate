package com.examsys.dao;

import java.util.List;

import com.examsys.po.PaperDetail;
import com.examsys.util.Page;
/**
 * 试卷明细的数据访问层实现类
 * @author edu-1
 *
 */
public class PaperDetailDaoImpl extends AbstractBaseDao<PaperDetail, Integer> implements PaperDetailDao {

	
	/**
	 * 添加多条信息
	 * @param paperDetails
	 * @throws Exception
	 */
	public void add(List<PaperDetail> paperDetails) throws Exception{
		for(PaperDetail paperDetail:paperDetails){
			add(paperDetail);
		}
	}
	
	/**
	 * 通过编号删除试卷明细
	 */
	@Override
	public void delete(Integer id) throws Exception {
		PaperDetail detail=this.get(PaperDetail.class,id);
		this.delete(detail);
	}

	/**
	 * 通过试卷编号删除试卷明细
	 */
	@Override
	public void deleteByPaperId(Integer paper_id) throws Exception {
		String sql="delete from PAPER_DETAIL a WHERE a.QUESTION_ID="+paper_id;
		this.delete(sql);
	}
	
	/**
	 * 通过编号获取试卷明细
	 */
	@Override
	public PaperDetail get(Integer id) throws Exception {
		return this.get(PaperDetail.class,id);
	}

	/**
	 * 获取所有试卷明细记录
	 */
	@Override
	public List<PaperDetail> getList() throws Exception {
		String sql="from PaperDetail";
		List<PaperDetail> list = this.query(sql, new Object[]{});
		return list;
	}

	/**
	 * 通过条件获取试卷明细
	 */
	@Override
	public List<PaperDetail> getList(PaperDetail obj) throws Exception {
		String sql="from PaperDetail a WHERE 1=1";
		
		if(obj.getPaper()!=null&&obj.getPaper().getId()!=null){
			sql+=" AND a.paper.id="+obj.getPaper().getId();
		}
		
		if(obj.getPaperSection()!=null&&obj.getPaperSection().getId()!=null){
			sql+=" AND a.paperSection.id="+obj.getPaperSection().getId();
		}
		
		if(obj.getQuestion()!=null&&obj.getQuestion().getId()!=null){
			sql+=" AND a.question.id="+obj.getQuestion().getId();
		}
		
		if(obj.getPaper()!=null&&obj.getPaper().getPaper_name()!=null){
			sql+=" AND a.paper.paper_name LIKE '%"+obj.getPaper().getPaper_name()+"%'";
		}
		
		List<PaperDetail> list = this.query(sql, new Object[]{});
		
		return list;
	}

	@Override
	public Page<PaperDetail> getList(PaperDetail obj, Integer page, Integer limit) throws Exception {
		String sql="from PaperDetail a WHERE 1=1";
		
		if(obj.getPaper()!=null&&obj.getPaper().getId()!=null){
			sql+=" AND a.paper.id="+obj.getPaper().getId();
		}
		
		if(obj.getPaperSection()!=null&&obj.getPaperSection().getId()!=null){
			sql+=" AND a.paperSection.id="+obj.getPaperSection().getId();
		}
		
		if(obj.getQuestion()!=null&&obj.getQuestion().getId()!=null){
			sql+=" AND a.question.id="+obj.getQuestion().getId();
		}
		
		if(obj.getPaper()!=null&&obj.getPaper().getPaper_name()!=null){
			sql+=" AND a.paper.paper_name LIKE '%"+obj.getPaper().getPaper_name()+"%'";
		}
		
		Page<PaperDetail> pageObj = this.searchForPager(sql, page, limit);
		
		return pageObj;
	}

}
