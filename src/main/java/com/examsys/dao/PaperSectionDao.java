package com.examsys.dao;

import com.examsys.po.PaperSection;
/**
 * 试卷中的章节数据访问层接口
 * @author edu-1
 *
 */
public interface PaperSectionDao extends IBaseDao<PaperSection, Integer> {
	/**
	 * 通过试卷编号删除章节
	 */
	public void deleteByPaperId(Integer id) throws Exception;
}
