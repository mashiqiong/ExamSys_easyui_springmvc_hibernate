package com.examsys.dao;

import java.util.List;

import com.examsys.po.PaperSection;
import com.examsys.util.Page;

/**
 * 试卷中的章节数据访问层实现类
 * @author edu-1
 *
 */
public class PaperSectionDaoImpl extends AbstractBaseDao<PaperSection, Integer> implements PaperSectionDao {

	/**
	 * 删除试卷中的章节
	 * @param id 编号
	 */
	@Override
	public void delete(Integer id) throws Exception {
		PaperSection section=this.get(PaperSection.class,id);
		this.delete(section);
	}

	/**
	 * 通过试卷编号删除章节
	 */
	public void deleteByPaperId(Integer id) throws Exception{
		String sql="delete from PAPER_SECTION a WHERE a.PAPER_ID="+id;
		this.delete(sql);
	}
	
	
	/**
	 * 试卷中的章节
	 * @param id 编号
	 */
	@Override
	public PaperSection get(Integer id) throws Exception {
		
		return this.get(PaperSection.class,id);
	}

	/**
	 * 获取所有试卷中的章节
	 */
	@Override
	public List<PaperSection> getList() throws Exception {
		//构建查询语句
		String sql="from PaperSection";
		List<PaperSection> list = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		return list;
	}

	/**
	 * 带条件获取试卷中的章节
	 */
	@Override
	public List<PaperSection> getList(PaperSection obj) throws Exception {
		//构建查询语句
		String sql="from PaperSection a WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getSection_name()!=null&&!"".equals(obj.getSection_name())){
				sql+=" AND a.section_name='"+obj.getSection_name()+"'";
			}
			
			if(obj.getPaper()!=null&&obj.getPaper().getId()!=null
					&&obj.getPaper().getId()!=0){
				sql+=" AND a.paper.id="+obj.getPaper().getId();
			}
			
			if(obj.getPaper()!=null&&obj.getPaper().getPaper_name()!=null
					&&!"".equals(obj.getPaper().getPaper_name())){
				sql+=" AND a.paper.paper_name='"+obj.getPaper().getPaper_name()+"'";
			}
			
		}

		List<PaperSection> list = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		return list;
	}

	@Override
	public Page<PaperSection> getList(PaperSection obj, Integer page, Integer limit) throws Exception {
		//构建查询语句
		String sql="from PaperSection a WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getSection_name()!=null&&!"".equals(obj.getSection_name())){
				sql+=" AND a.section_name='"+obj.getSection_name()+"'";
			}
			
			if(obj.getPaper()!=null&&obj.getPaper().getId()!=null
					&&obj.getPaper().getId()!=0){
				sql+=" AND a.paper.id="+obj.getPaper().getId();
			}
			
			if(obj.getPaper()!=null&&obj.getPaper().getPaper_name()!=null
					&&!"".equals(obj.getPaper().getPaper_name())){
				sql+=" AND a.paper.paper_name='"+obj.getPaper().getPaper_name()+"'";
			}
			
		}

		Page<PaperSection> pageObj = this.searchForPager(sql, page, limit);//调用父类的查询方法拿数据
		return pageObj;
	}

}
