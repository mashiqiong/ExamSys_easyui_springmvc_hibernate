package com.examsys.dao;

import java.util.List;

import com.examsys.po.PaperUserGroup;
import com.examsys.util.Page;
/**
 * 参与考试的用户组数据访问层实现类
 * @author edu-1
 *
 */
public class PaperUserGroupDaoImpl extends AbstractBaseDao<PaperUserGroup, Integer> implements PaperUserGroupDao {

	/**
	 * 添加多条参与考试的用户组信息
	 * @param paperUserGroups
	 * @throws Exception
	 */
	public void add(List<PaperUserGroup> paperUserGroups) throws Exception{
		for(PaperUserGroup paperUserGroup:paperUserGroups){
			this.add(paperUserGroup);
		}
	}
	
	/**
	 * 删除参与考试的用户组
	 */
	@Override
	public void delete(Integer id) throws Exception {
		PaperUserGroup group=this.get(PaperUserGroup.class,id);
		this.delete(group);
	}

	/**
	 * 获取参与考试的用户组
	 */
	@Override
	public PaperUserGroup get(Integer id) throws Exception {
		return this.get(PaperUserGroup.class,id);
		
	}

	/**
	 * 获取所有参与考试的用户组
	 */
	@Override
	public List<PaperUserGroup> getList() throws Exception {
		//构建查询语句
		String sql="from PaperUserGroup";
		List<PaperUserGroup> list = this.query(sql, new Object[]{});
		return list;
	}

	/**
	 * 带条件获取参与考试的用户组
	 */
	@Override
	public List<PaperUserGroup> getList(PaperUserGroup obj) throws Exception {
		//构建查询语句
		String sql="from PaperUserGroup WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getUserGroups()!=null&&obj.getUserGroups().getId()!=null
					&&obj.getUserGroups().getId()!=0){
				sql+=" AND a.userGroups.id="+obj.getUserGroups().getId();
			}
			
			if(obj.getUserGroups()!=null&&obj.getUserGroups().getGroup_name()!=null
					&&!"".equals(obj.getUserGroups().getGroup_name())){
				sql+=" AND a.userGroups.group_name='"+obj.getUserGroups().getGroup_name()+"'";
			}
			
			if(obj.getPaper()!=null&&obj.getPaper().getId()!=null
					&&obj.getPaper().getId()!=0){
				sql+=" AND a.paper.id="+obj.getPaper().getId();
			}
			
			if(obj.getPaper()!=null&&obj.getPaper().getPaper_name()!=null
					&&!"".equals(obj.getPaper().getPaper_name())){
				sql+=" AND a.paper.paper_name='"+obj.getPaper().getPaper_name()+"'";
			}
		}
		
		List<PaperUserGroup> list = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		return list;
	}

	@Override
	public Page<PaperUserGroup> getList(PaperUserGroup obj, Integer page, Integer limit) throws Exception {
		//构建查询语句
		String sql="from PaperUserGroup WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getUserGroups()!=null&&obj.getUserGroups().getId()!=null
					&&obj.getUserGroups().getId()!=0){
				sql+=" AND a.userGroups.id="+obj.getUserGroups().getId();
			}
			
			if(obj.getUserGroups()!=null&&obj.getUserGroups().getGroup_name()!=null
					&&!"".equals(obj.getUserGroups().getGroup_name())){
				sql+=" AND a.userGroups.group_name='"+obj.getUserGroups().getGroup_name()+"'";
			}
			
			if(obj.getPaper()!=null&&obj.getPaper().getId()!=null
					&&obj.getPaper().getId()!=0){
				sql+=" AND a.paper.id="+obj.getPaper().getId();
			}
			
			if(obj.getPaper()!=null&&obj.getPaper().getPaper_name()!=null
					&&!"".equals(obj.getPaper().getPaper_name())){
				sql+=" AND a.paper.paper_name='"+obj.getPaper().getPaper_name()+"'";
			}
		}
		
		Page<PaperUserGroup> pageObj = this.searchForPager(sql, page, limit);//调用父类的查询方法拿数据
		return pageObj;
	}

}
