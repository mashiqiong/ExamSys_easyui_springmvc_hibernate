package com.examsys.dao;

import java.util.List;

import com.examsys.po.Question;
import com.examsys.util.Page;

/**
 * 试题数据访问层实现类
 * @author edu-1
 *
 */
public class QuestionDaoImpl extends AbstractBaseDao<Question, Integer> implements QuestionDao {

	/**
	 * 删除试题
	 * @param id 编号
	 */
	@Override
	public void delete(Integer id) throws Exception {
		Question question=this.get(Question.class,id);
		this.delete(question);
	}

	/**
	 * 获取试题
	 * @param id 编号
	 */
	@Override
	public Question get(Integer id) throws Exception {
		
		return this.get(Question.class,id);
	}

	/**
	 * 获取所有试题
	 * 
	 */
	@Override
	public List<Question> getList() throws Exception {
		//构建查询语句
		String sql="from Question";
		List<Question> list = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		return list;
	}

	/**
	 * 带条件获取试题
	 * 
	 */
	@Override
	public List<Question> getList(Question obj) throws Exception {
		//构建查询语句
		String sql="from Question a WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getQuestionDb()!=null&&obj.getQuestionDb().getId()!=null
					&&obj.getQuestionDb().getId()!=0){
				sql+=" AND a.questionDb.id="+obj.getQuestionDb().getId();
			}
			
			if(obj.getQtype()!=null&&obj.getQtype()!=0){
				sql+=" AND a.qtype="+obj.getQtype();
			}
			
			if(obj.getQlevel()!=null&&obj.getQlevel()!=0){
				sql+=" AND a.qlevel="+obj.getQlevel();
			}
			
			if(obj.getContent()!=null&&!"".equals(obj.getContent())){
				sql+=" AND a.content LIKE '%"+obj.getContent()+"%'";
			}
			
		}
		List<Question> list = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		return list;
	}

	/**
	 * 通过条件获得信息列表，不包括给定编号的试题
	 * @param notInIds
	 * @return
	 */
	public List<Question> getList(String notInIds,Question question) throws Exception{
		//构建查询语句
		String sql="from Question a WHERE 1=1";
		if(notInIds!=null&&!"".equals(notInIds)){//条件构造
			sql+=" AND a.id NOT IN("+notInIds+")";
		}
		
		if(question.getQuestionDb()!=null&&question.getQuestionDb().getId()!=null
				&&question.getQuestionDb().getId()!=0){
			sql+=" AND a.questionDb.id="+question.getQuestionDb().getId();
		}
		
		if(question.getQtype()!=null&&question.getQtype()!=0){
			sql+=" AND a.qtype="+question.getQtype();
		}
		
		if(question.getQlevel()!=null&&question.getQlevel()!=0){
			sql+=" AND a.qlevel="+question.getQlevel();
		}
		
		List<Question> list = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		
		return list;
	}
	
	/**
	 * 获得序列值的方法
	 */
	@Override
	public Integer getSeq() throws Exception {
		String sql="select QUESTION_ID_SEQ.nextval id from dual";
		return this.getSeq(sql);
	}

	@Override
	public Page<Question> getList(Question question, Integer page, Integer limit) throws Exception {
		String notInIds = question.getNotInIds();
		//构建查询语句
		String sql="from Question a WHERE 1=1";
		if(notInIds!=null&&!"".equals(notInIds)){//条件构造
			sql+=" AND a.id NOT IN("+notInIds+")";
		}
		
		if(question.getQuestionDb()!=null&&question.getQuestionDb().getId()!=null
				&&question.getQuestionDb().getId()!=0){
			sql+=" AND a.questionDb.id="+question.getQuestionDb().getId();
		}
		
		if(question.getQtype()!=null&&question.getQtype()!=0){
			sql+=" AND a.qtype="+question.getQtype();
		}
		
		if(question.getQlevel()!=null&&question.getQlevel()!=0){
			sql+=" AND a.qlevel="+question.getQlevel();
		}
		
		Page<Question> pageObj = this.searchForPager(sql, page, limit);//调用父类的查询方法拿数据
		
		return pageObj;
	}

}
