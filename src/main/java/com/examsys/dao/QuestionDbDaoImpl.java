package com.examsys.dao;

import java.util.List;

import com.examsys.po.QuestionDb;
import com.examsys.util.Page;

/**
 * 题库数据访问层实现类
 * @author edu-1
 *
 */
public class QuestionDbDaoImpl extends AbstractBaseDao<QuestionDb, Integer> implements QuestionDbDao {

	/**
	 * 删除题库
	 */
	@Override
	public void delete(Integer id) throws Exception {
		QuestionDb db=this.get(QuestionDb.class,id);
		this.delete(db);
	}

	/**
	 * 获取题库
	 * @param id 编号
	 */
	@Override
	public QuestionDb get(Integer id) throws Exception {
		return this.get(QuestionDb.class,id);
	}

	/**
	 * 获取所有题库
	 */
	@Override
	public List<QuestionDb> getList() throws Exception {
		//构建查询语句
		String sql="from QuestionDb";
		List<QuestionDb> list = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		return list;
	}

	/**
	 * 带条件获取题库
	 */
	@Override
	public List<QuestionDb> getList(QuestionDb obj) throws Exception {
		//构建查询语句
		String sql="from QuestionDb a WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getName()!=null&&!"".equals(obj.getName())){
				sql+=" AND a.name LIKE '%"+obj.getName()+"%'";
			}
			
		}

		List<QuestionDb> list = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		return list;
	}

	@Override
	public Page<QuestionDb> getList(QuestionDb obj, Integer page, Integer limit) throws Exception {
		//构建查询语句
		String sql="from QuestionDb a WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getName()!=null&&!"".equals(obj.getName())){
				sql+=" AND a.name LIKE '%"+obj.getName()+"%'";
			}
			
		}

		Page<QuestionDb> pageObj = this.searchForPager(sql, page, limit);//调用父类的查询方法拿数据
		return pageObj;
	}

}
