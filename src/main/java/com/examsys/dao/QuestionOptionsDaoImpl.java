package com.examsys.dao;

import java.util.List;

import com.examsys.po.QuestionOptions;
import com.examsys.util.Page;

/**
 * 试题选项数据访问层实现类
 * @author edu-1
 *
 */
public class QuestionOptionsDaoImpl extends AbstractBaseDao<QuestionOptions, Integer> implements QuestionOptionsDao {

	/**
	 * 删除试题选项
	 */
	@Override
	public void delete(Integer id) throws Exception {
		QuestionOptions questionOptions=this.get(QuestionOptions.class,id);
		this.delete(questionOptions);
	}

	/**
	 * 通过试题编号删除试题选项
	 */
	@Override
	public void deleteByQuestionId(Integer question_id) throws Exception {
		String sql="delete from QUESTION_OPTIONS a WHERE a.QUESTION_ID="+question_id;
		this.delete(sql);
		
	}
	
	/**
	 * 获取试题选项
	 * @param id 编号
	 */
	@Override
	public QuestionOptions get(Integer id) throws Exception {
		
		return this.get(QuestionOptions.class,id);
}

	/**
	 * 获取所有试题选项
	 */
	@Override
	public List<QuestionOptions> getList() throws Exception {
		//构建查询语句
		String sql="from QuestionOptions";
		List<QuestionOptions> list = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		return list;
		
	}

	/**
	 * 带条件获取试题选项
	 */
	@Override
	public List<QuestionOptions> getList(QuestionOptions obj) throws Exception {
		//构建查询语句
		String sql="from QuestionOptions a WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getQuestion()!=null&&obj.getQuestion().getId()!=null
					&&obj.getQuestion().getId()!=0){
				sql+=" AND a.question.id="+obj.getQuestion().getId();
			}
			
		}
		List<QuestionOptions> list = this.query(sql, new Object[]{});
		return list;
	}

	@Override
	public Page<QuestionOptions> getList(QuestionOptions obj, Integer page, Integer limit) throws Exception {
		//构建查询语句
		String sql="from QuestionOptions a WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getQuestion()!=null&&obj.getQuestion().getId()!=null
					&&obj.getQuestion().getId()!=0){
				sql+=" AND a.question.id="+obj.getQuestion().getId();
			}
			
		}
		Page<QuestionOptions> pageObj = this.searchForPager(sql, page, limit);
		return pageObj;
	}

}
