package com.examsys.dao;

import com.examsys.po.SysTips;
/**
 * 系统提示信息数据访问层接口
 * @author edu-1
 *
 */
public interface SysTipsDao extends IBaseDao<SysTips, Integer> {

}
