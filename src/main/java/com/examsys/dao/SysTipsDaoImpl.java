package com.examsys.dao;

import java.util.List;

import com.examsys.po.SysTips;
import com.examsys.util.Page;
/**
 * 系统提示信息数据访问层实现类
 * @author edu-1
 *
 */
public class SysTipsDaoImpl extends AbstractBaseDao<SysTips, Integer> implements SysTipsDao {

	/**
	 * 删除系统提示信息
	 */
	@Override
	public void delete(Integer id) throws Exception {
		SysTips sysTips=this.get(SysTips.class,id);
		this.delete(sysTips);
	}

	/**
	 * 获取系统提示信息
	 */
	@Override
	public SysTips get(Integer id) throws Exception {
		return this.get(SysTips.class,id);
	}

	/**
	 * 获取所有系统提示信息记录
	 */
	@Override
	public List<SysTips> getList() throws Exception {
		//构建查询语句
		String sql="FROM SysTips";
		List<SysTips> list = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		return list;
	}

	/**
	 * 带条件获取系统提示信息记录
	 */
	@Override
	public List<SysTips> getList(SysTips obj) throws Exception {
		//构建查询语句
		String sql="FROM SysTips a WHERE 1=1";
		
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getScode()!=null&&!"".equals(obj.getScode())){
				sql+=" AND a.scode LIKE '%"+obj.getScode()+"%'";
			}
			
			if(obj.getSdesc()!=null&&!"".equals(obj.getSdesc())){
				sql+=" AND a.sdesc LIKE '%"+obj.getSdesc()+"%'";
			}
		}
		
		List<SysTips> list = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		
		return list;
	}

	@Override
	public Page<SysTips> getList(SysTips obj, Integer page, Integer limit) throws Exception {
		//构建查询语句
		String sql="FROM SysTips a WHERE 1=1";
		
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getScode()!=null&&!"".equals(obj.getScode())){
				sql+=" AND a.scode LIKE '%"+obj.getScode()+"%'";
			}
			
			if(obj.getSdesc()!=null&&!"".equals(obj.getSdesc())){
				sql+=" AND a.sdesc LIKE '%"+obj.getSdesc()+"%'";
			}
		}
		
		Page<SysTips> pageObj = this.searchForPager(sql, page, limit);//调用父类的查询方法拿数据
		
		return pageObj;
	}

}
