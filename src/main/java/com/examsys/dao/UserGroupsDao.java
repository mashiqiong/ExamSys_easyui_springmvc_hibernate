package com.examsys.dao;

import java.util.List;

import com.examsys.po.UserGroups;
/**
 * 会员组数据访问层接口
 * @author edu-1
 *
 */
public interface UserGroupsDao extends IBaseDao<UserGroups, Integer> {
	/**
	 * 通过条件获得信息列表，不包括给定编号的用户组
	 * @param notInIds
	 * @return
	 */
	public List<UserGroups> getList(String notInIds)throws Exception;
}
