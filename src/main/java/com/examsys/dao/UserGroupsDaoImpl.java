package com.examsys.dao;

import java.util.List;

import com.examsys.po.UserGroups;
import com.examsys.util.Page;
/**
 * 会员组数据访问层实现类
 * @author edu-1
 *
 */
public class UserGroupsDaoImpl extends AbstractBaseDao<UserGroups, Integer> implements UserGroupsDao {

	/**
	 * 删除会员组
	 */
	@Override
	public void delete(Integer id) throws Exception {
		UserGroups groups=this.get(UserGroups.class,id);
		this.delete(groups);
	}

	/**
	 * 获取会员组
	 */
	@Override
	public UserGroups get(Integer id) throws Exception {
		
		return this.get(UserGroups.class,id);
	}

	@Override
	public List<UserGroups> getList() throws Exception {
		//构建查询语句
		String sql="FROM UserGroups";
		List<UserGroups> list = this.query(sql, new Object[]{});
		return list;
	}

	@Override
	public List<UserGroups> getList(UserGroups obj) throws Exception {
		//构建查询语句
		String sql="FROM UserGroups a WHERE 1=1";
		
		if(obj!=null&&obj.getId()!=null){
			sql+=" AND a.id="+obj.getId();
		}
		
		if(obj!=null&&obj.getGroup_name()!=null){
			sql+=" AND a.group_name LIKE '%"+obj.getGroup_name()+"%'";
		}
		List<UserGroups> list = this.query(sql, new Object[]{});
		
		return list;
	}
	
	/**
	 * 通过条件获得信息列表，不包括给定编号的用户组
	 * @param notInIds
	 * @return
	 */
	public List<UserGroups> getList(String notInIds)throws Exception{
		//构建查询语句
		String sql="FROM UserGroups a WHERE 1=1";
		 List<UserGroups> list = this.query(sql, new Object[]{notInIds});
		return list;
	}

	@Override
	public Page<UserGroups> getList(UserGroups obj, Integer page, Integer limit) throws Exception {
		//构建查询语句
		String sql="FROM UserGroups a WHERE 1=1";
		
		if(obj!=null&&obj.getId()!=null){
			sql+=" AND a.id="+obj.getId();
		}
		
		if(obj!=null&&obj.getGroup_name()!=null){
			sql+=" AND a.group_name LIKE '%"+obj.getGroup_name()+"%'";
		}
		Page<UserGroups> pageObj = this.searchForPager(sql, page, limit);
		
		return pageObj;
	}
	

}
