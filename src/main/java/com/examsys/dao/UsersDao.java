package com.examsys.dao;

import com.examsys.po.Users;
/**
 * 会员或者考试者数据访问层接口
 * @author edu-1
 *
 */
public interface UsersDao extends IBaseDao<Users, Integer> {
	/**
	 * 通过用户名去获取考试者
	 * @param userName
	 * @return
	 */
	public Users getUsersByUserName(String userName)throws Exception;
}
