package com.examsys.dao;

import java.util.List;

import com.examsys.po.Users;
import com.examsys.util.Page;

/**
 * 会员或者考试者数据访问层实现类
 * @author edu-1
 *
 */
public class UsersDaoImpl extends AbstractBaseDao<Users, Integer> implements UsersDao {

	/**
	 * 删除会员或者考试者
	 */
	@Override
	public void delete(Integer id) throws Exception {
		Users users=this.get(Users.class,id);
		this.delete(users);
	}

	/**
	 * 获取会员或者考试者
	 * @param id 编号
	 */
	@Override
	public Users get(Integer id) throws Exception {
		
		return this.get(Users.class,id);
	}

	/**
	 * 获取所有会员或者考试者
	 */
	@Override
	public List<Users> getList() throws Exception {
		//构建查询语句
		String sql="from Users";
		List<Users> list = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		return list;
	}

	/**
	 * 带条件获取会员或者考试者
	 */
	@Override
	public List<Users> getList(Users obj) throws Exception {
		//构建查询语句
		String sql="from Users a WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getUser_name()!=null&&!"".equals(obj.getUser_name())){
				sql+=" AND a.user_name LIKE '%"+obj.getUser_name()+"%'";
			}
			
			if(obj.getUser_no()!=null&&!"".equals(obj.getUser_no())){
				sql+=" AND a.user_no='"+obj.getUser_no()+"'";
			}
			
			if(obj.getReal_name()!=null&&!"".equals(obj.getReal_name())){
				sql+=" AND a.real_name='"+obj.getReal_name()+"'";
			}
			
			if(obj.getUserGroups()!=null&&obj.getUserGroups().getId()!=null&&obj.getUserGroups().getId()!=0){
				sql+=" AND a.userGroups.id="+obj.getUserGroups().getId();
			}
		}

		List<Users> list = this.query(sql, new Object[]{});//调用父类的查询方法拿数据
		return list;
	}
	
	
	/**
	 * 通过用户名去获取考试者
	 * @param userName
	 * @return
	 */
	public Users getUsersByUserName(String userName)throws Exception{
		//构建查询语句
		String sql="from Users a WHERE a.USER_NAME=?";
		Users users=this.uniqueQuery(sql, new Object[]{userName});
		return users;	
	}

	@Override
	public Page<Users> getList(Users obj, Integer page, Integer limit) throws Exception {
		//构建查询语句
		String sql="from Users a WHERE 1=1";
		if(obj!=null){//条件构造
			
			if(obj.getId()!=null&&obj.getId()!=0){
				sql+=" AND a.id="+obj.getId();
			}
			
			if(obj.getUser_name()!=null&&!"".equals(obj.getUser_name())){
				sql+=" AND a.user_name LIKE '%"+obj.getUser_name()+"%'";
			}
			
			if(obj.getUser_no()!=null&&!"".equals(obj.getUser_no())){
				sql+=" AND a.user_no='"+obj.getUser_no()+"'";
			}
			
			if(obj.getReal_name()!=null&&!"".equals(obj.getReal_name())){
				sql+=" AND a.real_name='"+obj.getReal_name()+"'";
			}
			
			if(obj.getUserGroups()!=null&&obj.getUserGroups().getId()!=null&&obj.getUserGroups().getId()!=0){
				sql+=" AND a.userGroups.id="+obj.getUserGroups().getId();
			}
		}

		Page<Users> pageObj = this.searchForPager(sql, page, limit);//调用父类的查询方法拿数据
		return pageObj;
	}

}
