package com.examsys.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 系统功能实体类
 * @author edu-1
 *
 */
@Entity
@Table(name = "ADMIN_ROLES_SETTINGS")
@NamedQueries({
@NamedQuery(name = "AdminRolesSettings.findAll", query = "SELECT a FROM AdminRolesSettings a")})
public class AdminRolesSettings {
	
    private static final long serialVersionUID = 1L;
	    @Id
	    @SequenceGenerator(name="bkdex_seq_generator",sequenceName="ADMIN_ROLES_SETTINGS_ID_SEQ",allocationSize = 1)
		@GeneratedValue(generator="bkdex_seq_generator",strategy=GenerationType.SEQUENCE)
	    @Basic(optional = false)
	    @Column(name = "ID")
	private Integer id;//编号
	    @Basic(optional = false)
	    @Column(name = "NAME") 
	private String name;//菜单或功能名称
	    @Column(name = "CODE")
	private String code;//菜单或者功能代码
	    @Column(name = "URL")
	private String url;//菜单链接
	    @JoinColumn(name = "PARENT_ID", referencedColumnName = "ID")
	    @ManyToOne(optional = true,fetch=FetchType.EAGER)
	private AdminRolesSettings adminRolesSettings;//父级菜单
	    @Column(name = "PORDER")
	private Integer porder;//排序号
	
	public AdminRolesSettings() {
		super();
	}

	public AdminRolesSettings(Integer id, String name, String code, AdminRolesSettings adminRolesSettings,
			Integer porder) {
		super();
		this.id = id;
		this.name = name;
		this.code = code;
		this.adminRolesSettings = adminRolesSettings;
		this.porder = porder;
	}

	/**
	 * 获取编号
	 * @return
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 设置编号
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public AdminRolesSettings getAdminRolesSettings() {
		return adminRolesSettings;
	}

	public void setAdminRolesSettings(AdminRolesSettings adminRolesSettings) {
		this.adminRolesSettings = adminRolesSettings;
	}

	public Integer getPorder() {
		return porder;
	}

	public void setPorder(Integer porder) {
		this.porder = porder;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdminRolesSettings other = (AdminRolesSettings) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	
}
