package com.examsys.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.examsys.po.Question;

/**
 * 考生答题明细表实体类或者答题卡明细
 * @author edu-1
 *
 */
@Entity
@Table(name = "EXAM_DETAIL")
@NamedQueries({
@NamedQuery(name = "ExamDetail.findAll", query = "SELECT e FROM ExamDetail e")})
public class ExamDetail {
	
	private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="bkdex_seq_generator",sequenceName="EXAM_DETAIL_ID_SEQ",allocationSize = 1)
	@GeneratedValue(generator="bkdex_seq_generator",strategy=GenerationType.SEQUENCE)
    @Basic(optional = false)
    @Column(name = "ID")
	private Integer id;//编号
    @JoinColumn(name = "MAIN_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
	private ExamMain examMain;//答题卡主表
    @JoinColumn(name = "QUESTION_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
	private Question question;//所答题目
	@Column(name = "ANSWER")
	private String answer;//考生答题内容
	 // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SCORE")
	private double score;//本题所得分数
    @Basic(optional = false)
    @Column(name = "STATUS")
	private String status;//状态，用于记录此题是否作答了,0未作答，1已作答
    @Column(name = "REMARK")
    private String remark;//备注
    
	
	public ExamDetail() {
		super();
	}
	
	public ExamDetail(Integer id, ExamMain examMain, Question question, String answer, double score, String status,
			String remark) {
		super();
		this.id = id;
		this.examMain = examMain;
		this.question = question;
		this.answer = answer;
		this.score = score;
		this.status = status;
		this.remark = remark;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public ExamMain getExamMain() {
		return examMain;
	}
	public void setExamMain(ExamMain examMain) {
		this.examMain = examMain;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	public String toString() {
		return "ExamDetail [id=" + id + ", examMain=" + examMain + ", question=" + question + ", answer=" + answer
				+ ", score=" + score + ", status=" + status + ", remark=" + remark + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamDetail other = (ExamDetail) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
