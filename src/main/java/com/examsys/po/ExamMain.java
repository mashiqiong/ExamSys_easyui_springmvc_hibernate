package com.examsys.po;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.examsys.po.ExamDetail;
import com.examsys.po.Paper;
import com.examsys.po.Users;

/**
 * 考生答题主表实体类或也就是答题卡
 * @author edu-1
 *
 */
@Entity
@Table(name = "EXAM_MAIN")
@NamedQueries({
@NamedQuery(name = "ExamMain.findAll", query = "SELECT e FROM ExamMain e")})
public class ExamMain {
	
	private static final long serialVersionUID = 1L;
	    @Id
	    @Basic(optional = false)
	    @Column(name = "ID")
	private Integer id;//编号
	    @JoinColumn(name = "USER_ID", referencedColumnName = "ID")
	    @ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Users users;//答题卡所属的用户或者考试者
	    @JoinColumn(name = "PAPER_ID", referencedColumnName = "ID")
	    @ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Paper paper;//所答的试卷
	    @Column(name = "START_TIME")
	    @Temporal(TemporalType.TIMESTAMP)
	private Date start_time;//开始考试时间
	    @Column(name = "END_TIME")
	    @Temporal(TemporalType.TIMESTAMP)
	private Date end_time;//结束考试时间
	   // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
	    @Column(name = "SCORE")
	private double score;//所得分数
	    @Column(name = "IP")
	private String ip;//登录进来的IP
	    @Column(name = "STATUS")
	private String status;//考试状态,1考试中，2已经交卷，3已经评分
	    @Column(name = "REMARK")
	private String remark;//备注
	    @OneToMany(mappedBy = "examMain", fetch = FetchType.EAGER)
	private List<ExamDetail> examDetails=new ArrayList<ExamDetail>();//答题明细,一对多
   
	public ExamMain() {
		super();
	}
	
	public ExamMain(Integer id, Users users, Paper paper, Date start_time, Date end_time, double score, String ip,
			String status, String remark) {
		super();
		this.id = id;
		this.users = users;
		this.paper = paper;
		this.start_time = start_time;
		this.end_time = end_time;
		this.score = score;
		this.ip = ip;
		this.status = status;
		this.remark = remark;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Users getUsers() {
		return users;
	}
	public void setUsers(Users users) {
		this.users = users;
	}
	public Paper getPaper() {
		return paper;
	}
	public void setPaper(Paper paper) {
		this.paper = paper;
	}
	public Date getStart_time() {
		return start_time;
	}
	public void setStart_time(Date start_time) {
		this.start_time = start_time;
	}
	public Date getEnd_time() {
		return end_time;
	}
	public void setEnd_time(Date end_time) {
		this.end_time = end_time;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public List<ExamDetail> getExamDetails() {
		return examDetails;
	}

	public void setExamDetails(List<ExamDetail> examDetails) {
		this.examDetails = examDetails;
	}

	@Override
	public String toString() {
		return "ExamMain [id=" + id + ", users=" + users + ", paper=" + paper + ", start_time=" + start_time
				+ ", end_time=" + end_time + ", score=" + score + ", ip=" + ip + ", status=" + status + ", remark="
				+ remark + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamMain other = (ExamMain) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
