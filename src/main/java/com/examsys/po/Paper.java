package com.examsys.po;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.examsys.po.Admin;
import com.examsys.po.ExamMain;
import com.examsys.po.Onlines;
import com.examsys.po.PaperDetail;
import com.examsys.po.PaperSection;
import com.examsys.po.PaperUserGroup;

/**
 * 试卷实体类
 * @author edu-1
 *
 */
@Entity
@Table(name = "PAPER")
@NamedQueries({
@NamedQuery(name = "Paper.findAll", query = "SELECT p FROM Paper p")})
public class Paper {
	
	private static final long serialVersionUID = 1L;
	    @Id
	    @SequenceGenerator(name="bkdex_seq_generator",sequenceName="PAPER_ID_SEQ",allocationSize = 1)
		@GeneratedValue(generator="bkdex_seq_generator",strategy=GenerationType.SEQUENCE)
	    @Basic(optional = false)
	    @Column(name = "ID")
	private Integer id;//编号
	    @JoinColumn(name = "ADMIN_ID", referencedColumnName = "ID")
	    @ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Admin admin;//创建此试卷的管理员
	    @Basic(optional = false)
	    @Column(name = "PAPER_NAME")
	private String paper_name;//试卷名称
	    @Column(name = "START_TIME")
	    @Temporal(TemporalType.TIMESTAMP)
	private Date start_time;//开始考试时间
	    @Column(name = "END_TIME")
	    @Temporal(TemporalType.TIMESTAMP)
	private Date end_time;//结束考试时间
	    @Column(name = "PAPER_MINUTE")
	private Integer paper_minute;//考试总时间
	    @Column(name = "TOTAL_SCORE")
	private Integer total_score;//考试总分数
	    @Column(name = "POST_DATE")
	    @Temporal(TemporalType.TIMESTAMP)
	private Date post_date;//交卷时间
	    @Column(name = "SHOW_SCORE")
	    @Temporal(TemporalType.TIMESTAMP)
	private Date show_score;//公布成绩时间
	    @Column(name = "\"QORDER\"")
	private Integer qorder;//题目顺序,0表示自然顺序，1表示打乱随机
	    @Column(name = "STATUS")
	private String status;//状态，1开放，-1不开放
	    @Column(name = "REMARK")
	private String remark;//备注
	    @OneToMany(cascade = CascadeType.ALL, mappedBy = "paper", fetch = FetchType.EAGER)
	private List<PaperSection> paperSections=new ArrayList<PaperSection>();//试卷章节
	    @OneToMany(cascade = CascadeType.ALL, mappedBy = "paper", fetch = FetchType.EAGER)
	private List<PaperUserGroup> paperUserGroupList;
	    @OneToMany(cascade = CascadeType.ALL, mappedBy = "paper", fetch = FetchType.EAGER)
	private List<PaperDetail> paperDetailList;
	    @OneToMany(cascade = CascadeType.ALL, mappedBy = "paper", fetch = FetchType.EAGER)
	private List<ExamMain> examMainList;
	    @OneToMany(cascade = CascadeType.ALL, mappedBy = "paper", fetch = FetchType.EAGER)
	private List<Onlines> onlinesList;
	    
	public Paper() {
		super();
	}
	
	public Paper(Integer id, Admin admin, String paper_name, Date start_time, Date end_time, Integer paper_minute,
			Integer total_score, Date post_date, Date show_score, Integer qorder, String status, String remark) {
		super();
		this.id = id;
		this.admin = admin;
		this.paper_name = paper_name;
		this.start_time = start_time;
		this.end_time = end_time;
		this.paper_minute = paper_minute;
		this.total_score = total_score;
		this.post_date = post_date;
		this.show_score = show_score;
		this.qorder = qorder;
		this.status = status;
		this.remark = remark;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Admin getAdmin() {
		return admin;
	}
	public void setAdmin(Admin admin) {
		this.admin = admin;
	}
	public String getPaper_name() {
		return paper_name;
	}
	public void setPaper_name(String paper_name) {
		this.paper_name = paper_name;
	}
	public Date getStart_time() {
		return start_time;
	}
	public void setStart_time(Date start_time) {
		this.start_time = start_time;
	}
	public Date getEnd_time() {
		return end_time;
	}
	public void setEnd_time(Date end_time) {
		this.end_time = end_time;
	}
	public Integer getPaper_minute() {
		return paper_minute;
	}
	public void setPaper_minute(Integer paper_minute) {
		this.paper_minute = paper_minute;
	}
	public Integer getTotal_score() {
		return total_score;
	}
	public void setTotal_score(Integer total_score) {
		this.total_score = total_score;
	}
	public Date getPost_date() {
		return post_date;
	}
	public void setPost_date(Date post_date) {
		this.post_date = post_date;
	}
	public Date getShow_score() {
		return show_score;
	}
	public void setShow_score(Date show_score) {
		this.show_score = show_score;
	}
	public Integer getQorder() {
		return qorder;
	}
	public void setQorder(Integer qorder) {
		this.qorder = qorder;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public List<PaperSection> getPaperSections() {
		return paperSections;
	}

	public void setPaperSections(List<PaperSection> paperSections) {
		this.paperSections = paperSections;
	}

	@Override
	public String toString() {
		return "Paper [id=" + id + ", admin=" + admin + ", paper_name=" + paper_name + ", start_time=" + start_time
				+ ", end_time=" + end_time + ", paper_minute=" + paper_minute + ", total_score=" + total_score
				+ ", post_date=" + post_date + ", show_score=" + show_score + ", qorder=" + qorder + ", status="
				+ status + ", remark=" + remark + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((paper_name == null) ? 0 : paper_name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Paper other = (Paper) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (paper_name == null) {
			if (other.paper_name != null)
				return false;
		} else if (!paper_name.equals(other.paper_name))
			return false;
		return true;
	}

}
