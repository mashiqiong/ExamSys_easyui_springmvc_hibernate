package com.examsys.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 试卷明细实体类
 * @author edu-1
 *
 */
@Entity
@Table(name = "PAPER_DETAIL")
@NamedQueries({
@NamedQuery(name = "PaperDetail.findAll", query = "SELECT p FROM PaperDetail p")})
public class PaperDetail {
	
	private static final long serialVersionUID = 1L;
	    @Id
	    @SequenceGenerator(name="bkdex_seq_generator",sequenceName="PAPER_DETAIL_ID_SEQ",allocationSize = 1)
		@GeneratedValue(generator="bkdex_seq_generator",strategy=GenerationType.SEQUENCE)
	    @Basic(optional = false)
	    @Column(name = "ID")
	private Integer id;//编号
	    @JoinColumn(name = "PAPER_ID", referencedColumnName = "ID")
	    @ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Paper paper;//本章或本部分所属的试卷
	    @JoinColumn(name = "PAPER_SECTION_ID", referencedColumnName = "ID")
	    @ManyToOne(optional = false, fetch = FetchType.EAGER)
	private PaperSection paperSection;//所属的章节
	    @JoinColumn(name = "QUESTION_ID", referencedColumnName = "ID")
	    @ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Question question;//所属试题
	// @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
		@Column(name = "SCORE")
	private Double score;//本题分数
    	@Column(name = "PORDER")
	private Integer porder;//排序号
	
	public PaperDetail() {
		super();
	}

	public PaperDetail(Integer id, Paper paper, PaperSection paperSection, Question question, Double score,
			Integer porder) {
		super();
		this.id = id;
		this.paper = paper;
		this.paperSection = paperSection;
		this.question = question;
		this.score = score;
		this.porder = porder;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Paper getPaper() {
		return paper;
	}

	public void setPaper(Paper paper) {
		this.paper = paper;
	}

	public PaperSection getPaperSection() {
		return paperSection;
	}

	public void setPaperSection(PaperSection paperSection) {
		this.paperSection = paperSection;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Integer getPorder() {
		return porder;
	}

	public void setPorder(Integer porder) {
		this.porder = porder;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaperDetail other = (PaperDetail) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PaperDetail [id=" + id + ", paper=" + paper + ", paperSection=" + paperSection + ", question="
				+ question + ", score=" + score + ", porder=" + porder + "]";
	}
	
}
