package com.examsys.po;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.examsys.po.Paper;
import com.examsys.po.PaperDetail;

/**
 * 试卷中的章节实体类
 * @author edu-1
 *
 */
@Entity
@Table(name = "PAPER_SECTION")
@NamedQueries({
@NamedQuery(name = "PaperSection.findAll", query = "SELECT p FROM PaperSection p")})
public class PaperSection {
	
	private static final long serialVersionUID = 1L;
	    @Id
	    @SequenceGenerator(name="bkdex_seq_generator",sequenceName="PAPER_SECTION_ID_SEQ",allocationSize = 1)
		@GeneratedValue(generator="bkdex_seq_generator",strategy=GenerationType.SEQUENCE)
	    @Basic(optional = false)
	    @Column(name = "ID")
	private Integer id;//编号
	    @JoinColumn(name = "PAPER_ID", referencedColumnName = "ID")
	    @ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Paper paper;//本章或本部分所属的试卷
	    @Basic(optional = false)
	    @Column(name = "SECTION_NAME")
	private String section_name;//章节名称
		@Column(name = "PER_SCORE")
	private Integer per_score;//本章节的分数
		@Column(name = "REMARK")
	private String remark;//备注
		@OneToMany(cascade = CascadeType.ALL, mappedBy = "paperSection", fetch = FetchType.EAGER)
	private List<PaperDetail> paperDetailList;
		    
	
	public PaperSection() {
		super();
	}

	public PaperSection(Integer id, Paper paper, String section_name, Integer per_score, String remark) {
		super();
		this.id = id;
		this.paper = paper;
		this.section_name = section_name;
		this.per_score = per_score;
		this.remark = remark;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Paper getPaper() {
		return paper;
	}
	public void setPaper(Paper paper) {
		this.paper = paper;
	}
	public String getSection_name() {
		return section_name;
	}
	public void setSection_name(String section_name) {
		this.section_name = section_name;
	}
	public Integer getPer_score() {
		return per_score;
	}
	public void setPer_score(Integer per_score) {
		this.per_score = per_score;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "PaperSection [id=" + id + ", paper=" + paper + ", section_name=" + section_name + ", per_score="
				+ per_score + ", remark=" + remark + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaperSection other = (PaperSection) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}
