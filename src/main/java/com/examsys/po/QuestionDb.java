package com.examsys.po;

import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.examsys.po.Question;

/**
 * 题库实体类
 * @author edu-1
 *
 */
@Entity
@Table(name = "QUESTION_DB")
@NamedQueries({
@NamedQuery(name = "QuestionDb.findAll", query = "SELECT q FROM QuestionDb q")})
public class QuestionDb {
	
	private static final long serialVersionUID = 1L;
	    @Id
	    @SequenceGenerator(name="bkdex_seq_generator",sequenceName="QUESTION_DB_ID_SEQ",allocationSize = 1)
		@GeneratedValue(generator="bkdex_seq_generator",strategy=GenerationType.SEQUENCE)
	    @Basic(optional = false)
	    @Column(name = "ID")
	private Integer id;//编号
	    @JoinColumn(name = "ADMIN_ID", referencedColumnName = "ID")
	    @ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Admin admin;//创建此题库的管理员
		@Basic(optional = false)
	    @Column(name = "NAME")
	private String name;//题库名称
		@Basic(optional = false)
	    @Column(name = "CREATE_DATE")
	    @Temporal(TemporalType.TIMESTAMP)
	private Date create_date;//创建时间
		@Column(name = "STATUS")
	private String status;//状态，题库状态，1表示正常，-1表示锁定
		@Column(name = "REMARK")
	private String remark;//备注
		@OneToMany(cascade = CascadeType.ALL, mappedBy = "questionDb", fetch = FetchType.EAGER)
	private List<Question> questionList;
		
	
	public QuestionDb() {
		super();
	}
	
	public QuestionDb(Integer id, Admin admin, String name, Date create_date, String status, String remark) {
		super();
		this.id = id;
		this.admin = admin;
		this.name = name;
		this.create_date = create_date;
		this.status = status;
		this.remark = remark;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Admin getAdmin() {
		return admin;
	}
	public void setAdmin(Admin admin) {
		this.admin = admin;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

	
	@Override
	public String toString() {
		return "QuestionDb [id=" + id + ", admin=" + admin + ", name=" + name + ", create_date=" + create_date
				+ ", status=" + status + ", remark=" + remark + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QuestionDb other = (QuestionDb) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}
