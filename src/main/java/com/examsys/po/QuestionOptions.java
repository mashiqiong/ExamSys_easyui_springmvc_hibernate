package com.examsys.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 试题选项实体类
 * @author edu-1
 *
 */
@Entity
@Table(name = "QUESTION_OPTIONS")
@NamedQueries({
@NamedQuery(name = "QuestionOptions.findAll", query = "SELECT q FROM QuestionOptions q")})
public class QuestionOptions {
	
	private static final long serialVersionUID = 1L;
	    @Id
	    @SequenceGenerator(name="bkdex_seq_generator",sequenceName="QUESTION_OPTIONS_ID_SEQ",allocationSize = 1)
		@GeneratedValue(generator="bkdex_seq_generator",strategy=GenerationType.SEQUENCE)
	    @Basic(optional = false)
	    @Column(name = "ID")
	private Integer id;//编号
	    @JoinColumn(name = "QUESTION_ID", referencedColumnName = "ID")
	    @ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Question question;//所属试题
		@Column(name = "SALISA")
	private String salisa;//选项编号
		@Column(name = "SOPTION")
	private String soption;//选项描述
		@Column(name = "SEXTEND")
	private String sextend;//面试题的扩展项
	
	public QuestionOptions() {
		super();
	}
	
	public QuestionOptions(Integer id, Question question, String salisa, String soption, String sextend) {
		super();
		this.id = id;
		this.question = question;
		this.salisa = salisa;
		this.soption = soption;
		this.sextend = sextend;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	public String getSalisa() {
		return salisa;
	}
	public void setSalisa(String salisa) {
		this.salisa = salisa;
	}
	public String getSoption() {
		return soption;
	}
	public void setSoption(String soption) {
		this.soption = soption;
	}
	public String getSextend() {
		return sextend;
	}
	public void setSextend(String sextend) {
		this.sextend = sextend;
	}

	
	@Override
	public String toString() {
		return "QuestionOptions [id=" + id + ", question=" + question + ", salisa=" + salisa + ", soption=" + soption
				+ ", sextend=" + sextend + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QuestionOptions other = (QuestionOptions) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
