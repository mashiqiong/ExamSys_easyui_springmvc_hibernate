package com.examsys.po;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.examsys.po.PaperUserGroup;
import com.examsys.po.Users;

/**
 * 会员组或考试组实体类
 * @author edu-1
 *
 */
@Entity
@Table(name = "USER_GROUPS")
@NamedQueries({
@NamedQuery(name = "UserGroups.findAll", query = "SELECT u FROM UserGroups u")})
public class UserGroups {
	
	private static final long serialVersionUID = 1L;
	    @Id
	    @SequenceGenerator(name="bkdex_seq_generator",sequenceName="USER_GROUPS_ID_SEQ",allocationSize = 1)
		@GeneratedValue(generator="bkdex_seq_generator",strategy=GenerationType.SEQUENCE)
	    @Basic(optional = false)
	    @Column(name = "ID")
	private Integer id;//编号
	    @Column(name = "GROUP_NAME")
	private String group_name;//组名
	    @Column(name = "REMARK")
	private String remark;//备注
	    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userGroups", fetch = FetchType.EAGER)
	private List<PaperUserGroup> paperUserGroupList;
	    @OneToMany(mappedBy = "userGroups", fetch = FetchType.EAGER)
	private List<Users> usersList;
	
	public UserGroups() {
		super();
	}
	
	public UserGroups(Integer id, String group_name, String remark) {
		super();
		this.id = id;
		this.group_name = group_name;
		this.remark = remark;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getGroup_name() {
		return group_name;
	}
	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "UserGroups [id=" + id + ", group_name=" + group_name + ", remark=" + remark + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((group_name == null) ? 0 : group_name.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserGroups other = (UserGroups) obj;
		if (group_name == null) {
			if (other.group_name != null)
				return false;
		} else if (!group_name.equals(other.group_name))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
