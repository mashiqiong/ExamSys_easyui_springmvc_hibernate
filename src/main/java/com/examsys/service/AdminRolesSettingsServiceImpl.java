package com.examsys.service;

import java.util.List;

import com.examsys.dao.AdminRolesSettingsDao;
import com.examsys.po.AdminRolesSettings;
import com.examsys.util.Page;

/**
 * 系统功能业务逻辑层实现类
 * @author edu-1
 *
 */
public class AdminRolesSettingsServiceImpl extends AbstractBaseService<AdminRolesSettings, Integer> implements AdminRolesSettingsService {

	private AdminRolesSettingsDao dao;//系统功能数据访问层对象
	
	public AdminRolesSettingsDao getDao() {
		return dao;
	}

	public void setDao(AdminRolesSettingsDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加系统功能
	 *  
	 */
	@Override
	public boolean add(AdminRolesSettings obj) throws Exception{
		boolean flag=false;
		this.getDao().add(obj);
		flag=true;
		return flag;
	}

	/**
	 * 修改系统功能
	 */
	@Override
	public boolean update(AdminRolesSettings obj) throws Exception{
		boolean flag=false;
		this.getDao().update(obj);
		flag=true;
		return flag;
	}

	/**
	 * 删除系统功能
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public boolean delete(Integer id) throws Exception {
		boolean flag=false;
		this.getDao().delete(id);
		flag=true;
		return flag;
	}

	/**
	 * 获取系统功能
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public AdminRolesSettings get(Integer id) throws Exception {
		AdminRolesSettings adminRolesSettings=null;
		adminRolesSettings = this.getDao().get(id);
		return adminRolesSettings;
	}

	/**
	 * 获得系统功能表所有记录
	 * @throws Exception 
	 */
	@Override
	public List<AdminRolesSettings> getList() throws Exception {
		List<AdminRolesSettings> list=null;
		list = this.getDao().getList();
		
		return list;
	}

	/**
	 * 带条件获取系统功能记录
	 * @throws Exception 
	 */
	@Override
	public List<AdminRolesSettings> getList(AdminRolesSettings obj) throws Exception {
		List<AdminRolesSettings> list=null;
		list = this.getDao().getList(obj);
		
		return list;
	}

	@Override
	public Page<AdminRolesSettings> getList(AdminRolesSettings obj, Integer page, Integer limit) throws Exception {
		return this.getDao().getList(obj, page, limit);
	}

}
