package com.examsys.service;

import com.examsys.po.Config;

/**
 * 系统参数业务逻辑层接口
 * @author edu-1
 *
 */
public interface ConfigService extends IBaseService<Config, Integer> {

}
