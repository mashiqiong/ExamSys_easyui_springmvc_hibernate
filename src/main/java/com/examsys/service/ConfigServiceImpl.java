package com.examsys.service;

import java.util.List;

import com.examsys.dao.ConfigDao;
import com.examsys.po.Config;
import com.examsys.util.Page;

/**
 * 系统参数业务逻辑层实现类
 * @author edu-1
 *
 */
public class ConfigServiceImpl extends AbstractBaseService<Config, Integer> implements ConfigService {

	private ConfigDao dao;//系统参数数据访问层对象
	
	public ConfigDao getDao() {
		return dao;
	}

	public void setDao(ConfigDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加系统参数
	 * @throws Exception 
	 *  
	 */
	@Override
	public boolean add(Config obj) throws Exception {
		boolean flag=false;
		this.getDao().add(obj);
		flag=true;
		return flag;
	}

	/**
	 * 修改系统参数
	 * @throws Exception 
	 */
	@Override
	public boolean update(Config obj) throws Exception {
		boolean flag=false;
		this.getDao().update(obj);
		flag=true;
		return flag;
	}

	/**
	 * 删除系统参数
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public boolean delete(Integer id) throws Exception {
		boolean flag=false;
		this.getDao().delete(id);
		flag=true;
		return flag;
	}

	/**
	 * 获取系统参数
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public Config get(Integer id) throws Exception {
		Config config=null;
		config = this.getDao().get(id);
		return config;
	}

	/**
	 * 获得系统参数表所有记录
	 * @throws Exception 
	 */
	@Override
	public List<Config> getList() throws Exception {
		List<Config> list=null;
		list = this.getDao().getList();
		
		return list;
	}

	/**
	 * 带条件获取系统参数记录
	 * @throws Exception 
	 */
	@Override
	public List<Config> getList(Config obj) throws Exception {
		List<Config> list=null;
		list = this.getDao().getList(obj);
		
		return list;
	}

	@Override
	public Page<Config> getList(Config obj, Integer page, Integer limit) throws Exception {
		return this.getDao().getList(obj, page, limit);
	}

}
