package com.examsys.service;

import com.examsys.po.ExamDetail;

/**
 * 考生答题明细表实体类或者答题卡明细业务逻辑层接口
 * @author edu-1
 *
 */
public interface ExamDetailService extends IBaseService<ExamDetail, Integer> {

}
