package com.examsys.service;

import java.util.List;

import com.examsys.dao.ExamDetailDao;
import com.examsys.po.ExamDetail;
import com.examsys.util.Page;

/**
 * 考生答题明细表实体类或者答题卡明细业务逻辑层实现类
 * @author edu-1
 *
 */
public class ExamDetailServiceImpl extends AbstractBaseService<ExamDetail, Integer> implements ExamDetailService {

	private ExamDetailDao dao;//考生答题明细表实体类或者答题卡明细数据访问层对象
	
	public ExamDetailDao getDao() {
		return dao;
	}

	public void setDao(ExamDetailDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加考生答题明细表实体类或者答题卡明细
	 * @throws Exception 
	 *  
	 */
	@Override
	public boolean add(ExamDetail obj) throws Exception {
		boolean flag=false;
		this.getDao().add(obj);
		flag=true;
		return flag;
	}

	/**
	 * 修改考生答题明细表实体类或者答题卡明细
	 * @throws Exception 
	 */
	@Override
	public boolean update(ExamDetail obj) throws Exception {
		boolean flag=false;
		this.getDao().update(obj);
		flag=true;
		return flag;
	}

	/**
	 * 删除考生答题明细表实体类或者答题卡明细
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public boolean delete(Integer id) throws Exception {
		boolean flag=false;
		this.getDao().delete(id);
		flag=true;
		return flag;
	}

	/**
	 * 获取考生答题明细表实体类或者答题卡明细
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public ExamDetail get(Integer id) throws Exception {
		ExamDetail examDetail=null;
		examDetail = this.getDao().get(id);
		return examDetail;
	}

	/**
	 * 获得考生答题明细表实体类或者答题卡明细表所有记录
	 * @throws Exception 
	 */
	@Override
	public List<ExamDetail> getList() throws Exception {
		List<ExamDetail> list=null;
		list = this.getDao().getList();
		
		return list;
	}

	/**
	 * 带条件获取考生答题明细表实体类或者答题卡明细记录
	 * @throws Exception 
	 */
	@Override
	public List<ExamDetail> getList(ExamDetail obj) throws Exception {
		List<ExamDetail> list=null;
		list = this.getDao().getList(obj);
		
		return list;
	}

	@Override
	public Page<ExamDetail> getList(ExamDetail obj, Integer page, Integer limit) throws Exception {
		return this.getDao().getList(obj, page, limit);
	}

}
