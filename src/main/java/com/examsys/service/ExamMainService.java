package com.examsys.service;

import com.examsys.po.ExamMain;

/**
 * 考生答题卡业务逻辑层接口
 * @author edu-1
 *
 */
public interface ExamMainService extends IBaseService<ExamMain, Integer> {

}
