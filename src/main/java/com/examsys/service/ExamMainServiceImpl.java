package com.examsys.service;

import java.util.List;

import com.examsys.dao.ExamMainDao;
import com.examsys.po.ExamMain;
import com.examsys.util.Page;

/**
 * 考生答题卡业务逻辑层实现类
 * @author edu-1
 *
 */
public class ExamMainServiceImpl extends AbstractBaseService<ExamMain, Integer> implements ExamMainService {

	private ExamMainDao dao;//考生答题卡数据访问层对象
	
	public ExamMainDao getDao() {
		return dao;
	}

	public void setDao(ExamMainDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加考生答题卡
	 * @throws Exception 
	 *  
	 */
	@Override
	public boolean add(ExamMain obj) throws Exception {
		boolean flag=false;
		this.getDao().add(obj);
		flag=true;
		return flag;
	}

	/**
	 * 修改考生答题卡
	 * @throws Exception 
	 */
	@Override
	public boolean update(ExamMain obj) throws Exception {
		boolean flag=false;
		this.getDao().update(obj);
		flag=true;
		return flag;
	}

	/**
	 * 删除考生答题卡
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public boolean delete(Integer id) throws Exception {
		boolean flag=false;
		this.getDao().delete(id);
		flag=true;
		return flag;
	}

	/**
	 * 获取考生答题卡
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public ExamMain get(Integer id) throws Exception {
		ExamMain examMain=null;
		examMain = this.getDao().get(id);
		return examMain;
	}

	/**
	 * 获得考生答题卡表所有记录
	 * @throws Exception 
	 */
	@Override
	public List<ExamMain> getList() throws Exception {
		List<ExamMain> list=null;
		list = this.getDao().getList();
		
		return list;
	}

	/**
	 * 带条件获取考生答题卡记录
	 * @throws Exception 
	 */
	@Override
	public List<ExamMain> getList(ExamMain obj) throws Exception {
		List<ExamMain> list=null;
		list = this.getDao().getList(obj);
		
		return list;
	}

	@Override
	public Page<ExamMain> getList(ExamMain obj, Integer page, Integer limit) throws Exception {
		return this.getDao().getList(obj, page, limit);
	}

}
