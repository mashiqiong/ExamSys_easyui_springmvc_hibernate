package com.examsys.service;

import java.util.List;

import com.examsys.dao.OnlinesDao;
import com.examsys.po.Onlines;
import com.examsys.util.Page;

/**
 * 考试者在线考试的时间记录业务逻辑层实现类
 * @author edu-1
 *
 */
public class OnlinesServiceImpl extends AbstractBaseService<Onlines, Integer> implements OnlinesService {

	private OnlinesDao dao;//考试者在线考试的时间记录数据访问层对象
	
	public OnlinesDao getDao() {
		return dao;
	}

	public void setDao(OnlinesDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加考试者在线考试的时间记录
	 * @throws Exception 
	 *  
	 */
	@Override
	public boolean add(Onlines obj) throws Exception {
		boolean flag=false;
		this.getDao().add(obj);
		flag=true;
		return flag;
	}

	/**
	 * 修改考试者在线考试的时间记录
	 * @throws Exception 
	 */
	@Override
	public boolean update(Onlines obj) throws Exception {
		boolean flag=false;
		this.getDao().update(obj);
		flag=true;
		return flag;
	}

	/**
	 * 删除考试者在线考试的时间记录
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public boolean delete(Integer id) throws Exception {
		boolean flag=false;
		this.getDao().delete(id);
		flag=true;
		return flag;
	}

	/**
	 * 获取考试者在线考试的时间记录
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public Onlines get(Integer id) throws Exception {
		Onlines onlines=null;
		onlines = this.getDao().get(id);
		return onlines;
	}

	/**
	 * 获得考试者在线考试的时间记录表所有记录
	 * @throws Exception 
	 */
	@Override
	public List<Onlines> getList() throws Exception {
		List<Onlines> list=null;
		list = this.getDao().getList();
		
		return list;
	}

	/**
	 * 带条件获取考试者在线考试的时间记录记录
	 * @throws Exception 
	 */
	@Override
	public List<Onlines> getList(Onlines obj) throws Exception {
		List<Onlines> list=null;
		list = this.getDao().getList(obj);
		
		return list;
	}

	@Override
	public Page<Onlines> getList(Onlines obj, Integer page, Integer limit) throws Exception {
		return this.getDao().getList(obj, page, limit);
	}

}
