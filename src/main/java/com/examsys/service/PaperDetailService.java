package com.examsys.service;

import java.util.List;

import com.examsys.po.PaperDetail;
/**
 * 试卷明细业务逻辑层类
 * @author edu-1
 *
 */
public interface PaperDetailService extends IBaseService<PaperDetail, Integer> {
	/**
	 * 添加多条信息
	 * @param paperDetails
	 * @throws Exception
	 */
	public boolean add(List<PaperDetail> paperDetails) throws Exception;
}
