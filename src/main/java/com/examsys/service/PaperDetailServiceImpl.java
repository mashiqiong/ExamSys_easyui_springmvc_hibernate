package com.examsys.service;

import java.util.List;

import com.examsys.dao.PaperDetailDao;
import com.examsys.po.PaperDetail;
import com.examsys.util.Page;

/**
 * 试卷明细业务逻辑层实现类
 * @author edu-1
 *
 */
public class PaperDetailServiceImpl extends AbstractBaseService<PaperDetail, Integer> implements PaperDetailService {

	private PaperDetailDao dao;//数据访问层对象
	
	public PaperDetailDao getDao() {
		return dao;
	}

	public void setDao(PaperDetailDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加试卷明细
	 * @throws Exception 
	 */
	@Override
	public boolean add(PaperDetail obj) throws Exception {
		boolean flag=false;
		this.getDao().add(obj);
		flag=true;
		return flag;
	}

	/**
	 * 添加多条信息
	 * @param paperDetails
	 * @throws Exception
	 */
	public boolean add(List<PaperDetail> paperDetails) throws Exception{
		boolean flag=false;
		this.getDao().add(paperDetails);
		flag=true;
		return flag;
	}
	/**
	 * 修改试卷明细
	 * @throws Exception 
	 */
	@Override
	public boolean update(PaperDetail obj) throws Exception {
		boolean flag=false;
		this.getDao().update(obj);
		flag=true;
		return flag;
	}

	/**
	 * 通过编号删除试卷明细
	 * @throws Exception 
	 */
	@Override
	public boolean delete(Integer obj) throws Exception {
		boolean flag=false;
		this.getDao().delete(obj);
		flag=true;
		return flag;
	}

	/**
	 * 通过编号获取试卷明细
	 * @throws Exception 
	 */
	@Override
	public PaperDetail get(Integer obj) throws Exception {
		return this.getDao().get(obj);
	}

	/**
	 * 获取所有试卷明细记录
	 * @throws Exception 
	 */
	@Override
	public List<PaperDetail> getList() throws Exception {
		List<PaperDetail> list=null;
		list= this.getDao().getList();
		return list;
	}

	/**
	 * 通过条件获取试卷明细
	 * @throws Exception 
	 */
	@Override
	public List<PaperDetail> getList(PaperDetail obj) throws Exception {
		List<PaperDetail> list=null;
		list = this.getDao().getList(obj);
		return list;
	}

	@Override
	public Page<PaperDetail> getList(PaperDetail obj, Integer page, Integer limit) throws Exception {
		return this.getDao().getList(obj, page, limit);
	}

}
