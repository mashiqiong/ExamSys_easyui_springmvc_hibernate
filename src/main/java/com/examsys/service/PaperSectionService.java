package com.examsys.service;

import com.examsys.po.PaperSection;

/**
 * 试卷中的章节业务逻辑层接口
 * @author edu-1
 *
 */
public interface PaperSectionService extends IBaseService<PaperSection, Integer> {

}
