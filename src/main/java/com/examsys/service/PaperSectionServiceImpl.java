package com.examsys.service;

import java.util.List;

import com.examsys.dao.PaperSectionDao;
import com.examsys.po.PaperSection;
import com.examsys.util.Page;

/**
 * 试卷中的章节业务逻辑层实现类
 * @author edu-1
 *
 */
public class PaperSectionServiceImpl extends AbstractBaseService<PaperSection, Integer> implements PaperSectionService {

	private PaperSectionDao dao;//试卷中的章节数据访问层对象
	
	public PaperSectionDao getDao() {
		return dao;
	}

	public void setDao(PaperSectionDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加试卷中的章节
	 * @throws Exception 
	 *  
	 */
	@Override
	public boolean add(PaperSection obj) throws Exception {
		boolean flag=false;
		this.getDao().add(obj);
		flag=true;
		return flag;
	}

	/**
	 * 修改试卷中的章节
	 * @throws Exception 
	 */
	@Override
	public boolean update(PaperSection obj) throws Exception {
		boolean flag=false;
		this.getDao().update(obj);
		flag=true;
		return flag;
	}

	/**
	 * 删除试卷中的章节
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public boolean delete(Integer id) throws Exception {
		boolean flag=false;
		this.getDao().delete(id);
		flag=true;
		return flag;
	}

	/**
	 * 获取试卷中的章节
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public PaperSection get(Integer id) throws Exception {
		PaperSection paperSection=null;
		paperSection = this.getDao().get(id);
		return paperSection;
	}

	/**
	 * 获得试卷中的章节表所有记录
	 * @throws Exception 
	 */
	@Override
	public List<PaperSection> getList() throws Exception {
		List<PaperSection> list=null;
		list = this.getDao().getList();
		
		return list;
	}

	/**
	 * 带条件获取试卷中的章节记录
	 * @throws Exception 
	 */
	@Override
	public List<PaperSection> getList(PaperSection obj) throws Exception {
		List<PaperSection> list=null;
		list = this.getDao().getList(obj);
		
		return list;
	}

	@Override
	public Page<PaperSection> getList(PaperSection obj, Integer page, Integer limit) throws Exception {
		return this.getDao().getList(obj, page, limit);
	}

}
