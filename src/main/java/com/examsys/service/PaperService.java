package com.examsys.service;

import com.examsys.po.Paper;

/**
 * 试卷业务逻辑层接口
 * @author edu-1
 *
 */
public interface PaperService extends IBaseService<Paper, Integer> {

}
