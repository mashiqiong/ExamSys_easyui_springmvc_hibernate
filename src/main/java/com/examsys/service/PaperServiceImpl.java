package com.examsys.service;

import java.util.List;

import com.examsys.dao.PaperDao;
import com.examsys.dao.PaperDetailDao;
import com.examsys.dao.PaperSectionDao;
import com.examsys.po.Paper;
import com.examsys.po.PaperSection;
import com.examsys.util.Page;

/**
 * 试卷业务逻辑层实现类
 * @author edu-1
 *
 */
public class PaperServiceImpl extends AbstractBaseService<Paper, Integer> implements PaperService {

	private PaperDao dao;//试卷数据访问层对象
	private PaperSectionDao paperSectionDao;//试卷章节数据访问层对象
	private PaperDetailDao paperDetailDao;//试卷明细数据访问层对象
	
	public PaperDao getDao() {
		return dao;
	}

	public void setDao(PaperDao dao) {
		this.dao = dao;
	}

	public PaperSectionDao getPaperSectionDao() {
		return paperSectionDao;
	}

	public void setPaperSectionDao(PaperSectionDao paperSectionDao) {
		this.paperSectionDao = paperSectionDao;
	}

	public PaperDetailDao getPaperDetailDao() {
		return paperDetailDao;
	}

	public void setPaperDetailDao(PaperDetailDao paperDetailDao) {
		this.paperDetailDao = paperDetailDao;
	}

	/**
	 * 添加试卷
	 * @throws Exception 
	 *  
	 */
	@Override
	public boolean add(Paper obj) throws Exception {
		boolean flag=false;
			
		Integer id = this.getDao().getSeq();//获得编号
		obj.setId(id);//设置编号
		
		//再保存章节的对象
		List<PaperSection> paperSections = obj.getPaperSections();
		for(PaperSection ps:paperSections){
			ps.setPaper(obj);//关联试卷对象
		}
		
		this.getDao().add(obj);//先保存试卷对象
		flag=true;
		return flag;
	}

	/**
	 * 修改试卷
	 * @throws Exception 
	 */
	@Override
	public boolean update(Paper obj) throws Exception {
		boolean flag=false;
		this.getPaperDetailDao().deleteByPaperId(obj.getId());//先删除数据库中的试题选项
		List<PaperSection> paperSections = obj.getPaperSections();
		
		for(PaperSection paperSection:paperSections){
			paperSection.setPaper(obj);
		}
		this.getDao().update(obj);
		
		flag=true;
		return flag;
	}

	/**
	 * 通过试卷编号删除试卷
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public boolean delete(Integer id) throws Exception {
		boolean flag=false;
		this.getPaperDetailDao().deleteByPaperId(id);//先删除试卷中的试题
		this.getPaperSectionDao().deleteByPaperId(id);//再删除试卷章节
		this.getDao().delete(id);//再删除试卷本身
		flag=true;
		return flag;
	}

	/**
	 * 获取试卷
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public Paper get(Integer id) throws Exception {
		Paper paper=null;
		paper = this.getDao().get(id);
		return paper;
	}

	/**
	 * 获得试卷表所有记录
	 * @throws Exception 
	 */
	@Override
	public List<Paper> getList() throws Exception {
		List<Paper> list=null;
		list = this.getDao().getList();
		
		return list;
	}

	/**
	 * 带条件获取试卷记录
	 * @throws Exception 
	 */
	@Override
	public List<Paper> getList(Paper obj) throws Exception {
		List<Paper> list=null;
		list = this.getDao().getList(obj);
		
		return list;
	}

	@Override
	public Page<Paper> getList(Paper obj, Integer page, Integer limit) throws Exception {
		return this.getDao().getList(obj, page, limit);
	}

}
