package com.examsys.service;

import java.util.List;

import com.examsys.dao.PaperUserGroupDao;
import com.examsys.po.PaperUserGroup;
import com.examsys.util.Page;

/**
 * 记录可以参与考试的用户组业务逻辑层实现类
 * @author edu-1
 *
 */
public class PaperUserGroupServiceImpl extends AbstractBaseService<PaperUserGroup, Integer> implements PaperUserGroupService {

	private PaperUserGroupDao dao;//记录可以参与考试的用户组数据访问层对象
	
	public PaperUserGroupDao getDao() {
		return dao;
	}

	public void setDao(PaperUserGroupDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加记录可以参与考试的用户组
	 * @throws Exception 
	 *  
	 */
	@Override
	public boolean add(PaperUserGroup obj) throws Exception {
		boolean flag=false;
		this.getDao().add(obj);
		flag=true;
		return flag;
	}
	
	/**
	 * 添加多条信息
	 * @param paperUserGroups
	 * @throws Exception
	 */
	@Override
	public boolean add(List<PaperUserGroup> paperUserGroups) throws Exception{
		boolean flag=false;
		this.getDao().add(paperUserGroups);
		flag=true;
		return flag;
	}

	/**
	 * 修改记录可以参与考试的用户组
	 * @throws Exception 
	 */
	@Override
	public boolean update(PaperUserGroup obj) throws Exception {
		boolean flag=false;
		this.getDao().update(obj);
		flag=true;
		return flag;
	}

	/**
	 * 删除记录可以参与考试的用户组
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public boolean delete(Integer id) throws Exception {
		boolean flag=false;
		this.getDao().delete(id);
		flag=true;
		return flag;
	}

	/**
	 * 获取记录可以参与考试的用户组
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public PaperUserGroup get(Integer id) throws Exception {
		PaperUserGroup paperUserGroup=null;
		paperUserGroup = this.getDao().get(id);
		return paperUserGroup;
	}

	/**
	 * 获得记录可以参与考试的用户组表所有记录
	 * @throws Exception 
	 */
	@Override
	public List<PaperUserGroup> getList() throws Exception {
		List<PaperUserGroup> list=null;
		list = this.getDao().getList();
		
		return list;
	}

	/**
	 * 带条件获取记录可以参与考试的用户组记录
	 * @throws Exception 
	 */
	@Override
	public List<PaperUserGroup> getList(PaperUserGroup obj) throws Exception {
		List<PaperUserGroup> list=null;
		list = this.getDao().getList(obj);
		
		return list;
	}

	@Override
	public Page<PaperUserGroup> getList(PaperUserGroup obj, Integer page, Integer limit) throws Exception {
		return this.getDao().getList(obj, page, limit);
	}

}
