package com.examsys.service;

import com.examsys.po.QuestionDb;

/**
 * 题库业务逻辑层接口
 * @author edu-1
 *
 */
public interface QuestionDbService extends IBaseService<QuestionDb, Integer> {

}
