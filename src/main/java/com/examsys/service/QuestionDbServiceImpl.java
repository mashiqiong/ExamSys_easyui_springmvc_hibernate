package com.examsys.service;

import java.util.List;

import com.examsys.dao.QuestionDbDao;
import com.examsys.po.QuestionDb;
import com.examsys.util.Page;

/**
 * 题库业务逻辑层实现类
 * @author edu-1
 *
 */
public class QuestionDbServiceImpl extends AbstractBaseService<QuestionDb, Integer> implements QuestionDbService {

	private QuestionDbDao dao;//题库数据访问层对象
	
	public QuestionDbDao getDao() {
		return dao;
	}

	public void setDao(QuestionDbDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加题库
	 * @throws Exception 
	 *  
	 */
	@Override
	public boolean add(QuestionDb obj) throws Exception {
		boolean flag=false;
		this.getDao().add(obj);
		flag=true;
		return flag;
	}

	/**
	 * 修改题库
	 * @throws Exception 
	 */
	@Override
	public boolean update(QuestionDb obj) throws Exception {
		boolean flag=false;
		this.getDao().update(obj);
		flag=true;
		return flag;
	}

	/**
	 * 删除题库
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public boolean delete(Integer id) throws Exception {
		boolean flag=false;
		this.getDao().delete(id);
		flag=true;
		return flag;
	}

	/**
	 * 获取题库
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public QuestionDb get(Integer id) throws Exception {
		QuestionDb questionDb=null;
		questionDb = this.getDao().get(id);
		return questionDb;
	}

	/**
	 * 获得题库表所有记录
	 * @throws Exception 
	 */
	@Override
	public List<QuestionDb> getList() throws Exception {
		List<QuestionDb> list=null;
		list = this.getDao().getList();
		
		return list;
	}

	/**
	 * 带条件获取题库记录
	 * @throws Exception 
	 */
	@Override
	public List<QuestionDb> getList(QuestionDb obj) throws Exception {
		List<QuestionDb> list=null;
		list = this.getDao().getList(obj);
		
		return list;
	}

	@Override
	public Page<QuestionDb> getList(QuestionDb obj, Integer page, Integer limit) throws Exception {
		return this.getList(obj, page, limit);
	}

}
