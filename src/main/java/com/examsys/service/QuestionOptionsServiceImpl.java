package com.examsys.service;

import java.util.List;

import com.examsys.dao.QuestionOptionsDao;
import com.examsys.po.QuestionOptions;
import com.examsys.util.Page;

/**
 * 试题选项业务逻辑层实现类
 * @author edu-1
 *
 */
public class QuestionOptionsServiceImpl extends AbstractBaseService<QuestionOptions, Integer> implements QuestionOptionsService {

	private QuestionOptionsDao dao;//试题选项数据访问层对象
	
	public QuestionOptionsDao getDao() {
		return dao;
	}

	public void setDao(QuestionOptionsDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加试题选项
	 * @throws Exception 
	 *  
	 */
	@Override
	public boolean add(QuestionOptions obj) throws Exception {
		boolean flag=false;
		this.getDao().add(obj);
		flag=true;
		return flag;
	}

	/**
	 * 修改试题选项
	 * @throws Exception 
	 */
	@Override
	public boolean update(QuestionOptions obj) throws Exception {
		boolean flag=false;
		this.getDao().update(obj);
		flag=true;
		return flag;
	}

	/**
	 * 删除试题选项
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public boolean delete(Integer id) throws Exception {
		boolean flag=false;
		this.getDao().delete(id);
		flag=true;
		return flag;
	}

	/**
	 * 获取试题选项
	 * @param id 编号
	 */
	@Override
	public QuestionOptions get(Integer id)  throws Exception {
		QuestionOptions questionOptions=null;
		questionOptions = this.getDao().get(id);
		return questionOptions;
	}

	/**
	 * 获得试题选项表所有记录
	 */
	@Override
	public List<QuestionOptions> getList() throws Exception {
		List<QuestionOptions> list=null;
		list = this.getDao().getList();
		
		return list;
	}

	/**
	 * 带条件获取试题选项记录
	 */
	@Override
	public List<QuestionOptions> getList(QuestionOptions obj) throws Exception {
		List<QuestionOptions> list=null;
		list = this.getDao().getList(obj);
		
		return list;
	}

	@Override
	public Page<QuestionOptions> getList(QuestionOptions obj, Integer page, Integer limit) throws Exception {
		return this.getDao().getList(obj, page, limit);
	}

}
