package com.examsys.service;

import java.util.List;

import com.examsys.po.Question;

/**
 * 试题业务逻辑层接口
 * @author edu-1
 *
 */
public interface QuestionService extends IBaseService<Question, Integer> {
	// 通过条件获得信息列表，不包括给定编号的试题
	public List<Question> getList(String notInIds,Question question) throws Exception;
}
