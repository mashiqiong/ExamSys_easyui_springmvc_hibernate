package com.examsys.service;

import java.util.List;

import com.examsys.dao.SysTipsDao;
import com.examsys.po.SysTips;
import com.examsys.util.Page;

/**
 * 系统提示信息业务逻辑层实现类
 * @author edu-1
 *
 */
public class SysTipsServiceImpl extends AbstractBaseService<SysTips, Integer> implements SysTipsService {

	private SysTipsDao dao;//系统提示信息数据访问层对象
	
	public SysTipsDao getDao() {
		return dao;
	}

	public void setDao(SysTipsDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加系统提示信息
	 * @throws Exception 
	 *  
	 */
	@Override
	public boolean add(SysTips obj) throws Exception {
		boolean flag=false;
		this.getDao().add(obj);
		flag=true;
		return flag;
	}

	/**
	 * 修改系统提示信息
	 * @throws Exception 
	 */
	@Override
	public boolean update(SysTips obj) throws Exception {
		boolean flag=false;
		this.getDao().update(obj);
		flag=true;
		return flag;
	}

	/**
	 * 删除系统提示信息
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public boolean delete(Integer id) throws Exception {
		boolean flag=false;
		this.getDao().delete(id);
		flag=true;
		return flag;
	}

	/**
	 * 获取系统提示信息
	 * @param id 编号
	 * @throws Exception 
	 */
	@Override
	public SysTips get(Integer id) throws Exception {
		SysTips sysTips=null;
		sysTips = this.getDao().get(id);
		return sysTips;
	}

	/**
	 * 获得系统提示信息表所有记录
	 * @throws Exception 
	 */
	@Override
	public List<SysTips> getList() throws Exception {
		List<SysTips> list=null;
		list = this.getDao().getList();
		
		return list;
	}

	/**
	 * 带条件获取系统提示信息记录
	 * @throws Exception 
	 */
	@Override
	public List<SysTips> getList(SysTips obj) throws Exception {
		List<SysTips> list=null;
		list = this.getDao().getList(obj);
		
		return list;
	}

	@Override
	public Page<SysTips> getList(SysTips obj, Integer page, Integer limit) throws Exception {
		return this.getDao().getList(obj, page, limit);
	}

}
