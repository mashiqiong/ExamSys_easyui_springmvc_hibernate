package com.examsys.service;

import java.util.List;

import com.examsys.dao.UserGroupsDao;
import com.examsys.po.UserGroups;
import com.examsys.util.Page;

/**
 * 会员组或考试组业务逻辑层实现类
 * @author edu-1
 *
 */
public class UserGroupsServiceImpl extends AbstractBaseService<UserGroups, Integer> implements UserGroupsService {

	private UserGroupsDao dao;//会员组或考试组数据访问层对象
	
	public UserGroupsDao getDao() {
		return dao;
	}

	public void setDao(UserGroupsDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加会员组或考试组
	 *  
	 */
	@Override
	public boolean add(UserGroups obj) throws Exception {
		boolean flag=false;
		this.getDao().add(obj);
		flag=true;
		return flag;
	}

	/**
	 * 修改会员组或考试组
	 */
	@Override
	public boolean update(UserGroups obj) throws Exception {
		boolean flag=false;
		this.getDao().update(obj);
		flag=true;
		return flag;
	}

	/**
	 * 删除会员组或考试组
	 * @param id 编号
	 */
	@Override
	public boolean delete(Integer id) throws Exception {
		boolean flag=false;
		this.getDao().delete(id);
		flag=true;
		return flag;
	}

	/**
	 * 获取会员组或考试组
	 * @param id 编号
	 */
	@Override
	public UserGroups get(Integer id) throws Exception {
		UserGroups userGroups=null;
		userGroups = this.getDao().get(id);
		return userGroups;
	}

	/**
	 * 获得会员组或考试组表所有记录
	 */
	@Override
	public List<UserGroups> getList() throws Exception {
		List<UserGroups> list=null;
		list = this.getDao().getList();
		
		return list;
	}

	/**
	 * 带条件获取会员组或考试组记录
	 */
	@Override
	public List<UserGroups> getList(UserGroups obj) throws Exception {
		List<UserGroups> list=null;
		list = this.getDao().getList(obj);
		
		return list;
	}

	/**
	 * 通过条件获得信息列表，不包括给定编号的用户组
	 * @param notInIds
	 * @return
	 */
	public List<UserGroups> getList(String notInIds) throws Exception {
		List<UserGroups> list=null;
		list = this.getDao().getList(notInIds);
		
		return list;
	}

	@Override
	public Page<UserGroups> getList(UserGroups obj, Integer page, Integer limit) throws Exception {
		return this.getDao().getList(obj, page, limit);
	}
}
