package com.examsys.service;

import com.examsys.po.Users;

/**
 * 会员或或者考试者业务逻辑层接口
 * @author edu-1
 *
 */
public interface UsersService extends IBaseService<Users, Integer> {
	
	/**
	 * 通过用户名去获取考试者
	 * @param userName
	 * @return
	 * @throws Exception 
	 */
	public Users getUsersByUserName(String userName) throws Exception;
}
