package com.examsys.service;

import java.util.List;

import com.examsys.dao.UsersDao;
import com.examsys.po.Users;
import com.examsys.util.Page;

/**
 * 会员或或者考试者业务逻辑层实现类
 * @author edu-1
 *
 */
public class UsersServiceImpl extends AbstractBaseService<Users, Integer> implements UsersService {

	private UsersDao dao;//会员或或者考试者数据访问层对象
	
	public UsersDao getDao() {
		return dao;
	}

	public void setDao(UsersDao dao) {
		this.dao = dao;
	}

	/**
	 * 添加会员或或者考试者
	 *  
	 */
	@Override
	public boolean add(Users obj) throws Exception {
		boolean flag=false;
		this.getDao().add(obj);
		flag=true;
		return flag;
	}

	/**
	 * 修改会员或或者考试者
	 */
	@Override
	public boolean update(Users obj) throws Exception {
		boolean flag=false;
		this.getDao().update(obj);
		flag=true;
		return flag;
	}

	/**
	 * 删除会员或或者考试者
	 * @param id 编号
	 */
	@Override
	public boolean delete(Integer id) throws Exception {
		boolean flag=false;
		this.getDao().delete(id);
		flag=true;
		return flag;
	}

	/**
	 * 获取会员或或者考试者
	 * @param id 编号
	 */
	@Override
	public Users get(Integer id) throws Exception {
		Users users=null;
		users = this.getDao().get(id);
		return users;
	}

	/**
	 * 获得会员或或者考试者表所有记录
	 */
	@Override
	public List<Users> getList() throws Exception {
		List<Users> list=null;
		list = this.getDao().getList();
		
		return list;
	}

	/**
	 * 带条件获取会员或或者考试者记录
	 */
	@Override
	public List<Users> getList(Users obj) throws Exception {
		List<Users> list=null;
		list = this.getDao().getList(obj);
		
		return list;
	}
	
	/**
	 * 通过用户名去获取考试者
	 * @param userName
	 * @return
	 */
	public Users getUsersByUserName(String userName) throws Exception {
		Users users=null;
		users = this.getDao().getUsersByUserName(userName);
		return users;
	}

	@Override
	public Page<Users> getList(Users obj, Integer page, Integer limit) throws Exception {
		return this.getDao().getList(obj, page, limit);
	}

}
