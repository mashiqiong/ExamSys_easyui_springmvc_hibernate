<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<title>在线考试系统</title>
	<c:import url="/header.jsp"></c:import>
</head>
<body class="easyui-layout layout panel-noscroll">
	<div class="easyui-layout" style="width:100%;height:768px;">
		<div data-options="region:'north'" style="background-color: rgb(224, 236, 255);height:100px">欢迎使用考试系统</div>
		<div data-options="region:'south',split:true" style="height:150px;"></div>
		<!-- <div data-options="region:'east',split:true" title="East" style="width:100px;"></div> -->
		<div data-options="region:'west',split:true" title="功能导航" style="width:200px;">
			<ul id="tree" class="easyui-tree" data-options="url:'ajaxTree.do',method:'get',animate:true"></ul>
		</div>
		<div data-options="region:'center'">
			<div id="tabs" class="easyui-tabs" style="width:100%;height:100%">
				
			</div>
		</div>
	</div>
	<script type="text/javascript">
	$(function(){

		//实例化树菜单
		$("#tree").tree({
			lines:true,
			onClick:function(node){
				if(node.attributes){
					openTab(node.text,node.attributes.url);
				}
			}
		});
		
		// 新增Tab
		function openTab(text,url){
			if($("#tabs").tabs('exists',text)){
				$("#tabs").tabs('select',text);
			}else{
				var content="<iframe frameborder='0' scrolling='auto' style='width:100%;height:100%' src="+url+"></iframe>";
				$("#tabs").tabs('add',{
					title:text,
					closable:true,
					content:content
				});
			}
		}

		openTab("系统功能管理","adminRolesSettings/select");
		$("#cl-dashboard").html('');
	});
		
		
	</script>
</body>
</html>
