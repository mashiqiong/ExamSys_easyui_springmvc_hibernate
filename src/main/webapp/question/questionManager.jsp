<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<title>试题管理</title>
	<c:import url="/header.jsp"></c:import>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/question.js"></script>
	<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/static/ueditor-1.4.3.3/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/static/ueditor-1.4.3.3/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/static/ueditor-1.4.3.3/lang/zh-cn/zh-cn.js"></script>
</head>
  
 <body class="easyui-layout">
 	<div class="easyui-panel" style="height:754px;padding:10px;">
 		<div class="easyui-panel" style="padding:10px;">
	        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add'" onclick="$('#add').dialog('open')">添加</a>
	        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="$('#search').dialog('open')">查询</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-edit'" onclick="javascript:loadRemote()">修改</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove'" onclick="javascript:removeConfirm()">删除</a>
		</div>
		<table id="dg" style="width:100%;height:400px" class="easyui-datagrid" data-options="url:'${pageContext.request.contextPath}/question/selectDatas.do',method:'post',rownumbers:true,pagination:true,border:true,singleSelect:true">
		</table>
	</div>
	
	<div id="add" class="easyui-dialog" title="添加试题" 
		data-options="modal:true,closed:true,iconCls:'icon-add',buttons:'#add-dlg-buttons'" 
		style="width:900px;height:450px;padding:10px;">
		<div style="padding:10px 0 10px 60px">
			<form id="addForm" method="post">
	            <table>
	                <tr>
	                    <td>题目来源:</td>
	                    <td><input class="easyui-validatebox" type="text" name="qfrom" data-options="required:true"></input></td>
	                </tr>
	                <tr>
	                    <td>所属题库:</td>
		                <td><input class="easyui-combobox"
				            name="db_id"
				            data-options="
		                    url:'${pageContext.request.contextPath}/question/questionDbDatas',
		                    valueField:'id',
		                    textField:'name',
		                    panelHeight:'auto'
		            		"></td>
	                </tr>
	                <tr>
	                    <td>题目类型:</td>
	                    <td>
	                    	<select class="easyui-combobox" id="qtype" name="qtype" style="width:170px;">
					        	<option value="1" selected="selected">单选</option>
					        	<option value="2">多选</option>
					        	<option value="3">填空</option>
					        	<option value="4">判断</option>
					        	<option value="5">问答</option>
					    	</select>
						</td>
	                </tr>
	                <tr>
	                    <td>难度级别:</td>
	                    <td>
	                    	<select class="easyui-combobox" name="qlevel" style="width:170px;">
					        	<option value="1" selected="selected">易</option>
					        	<option value="2">正常</option>
					        	<option value="3">难</option>
					    	</select>
						</td>
	                </tr>
	                <tr>
	                    <td>备注:</td>
	                    <td><input class="easyui-validatebox" type="text" name="remark" data-options="required:false"></input></td>
	                </tr>
	                <tr>
	                    <td>题干内容:</td>
	                    <td><textarea class="easyui-validatebox" style="width: 710px;" id="add_content" name="content" data-options="required:true"></textarea ></td>
	                </tr>
	                
	                <tr>
	                	<td>试题选项</td>
		                <td colspan="1">
		                	<p><a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add'" id="btn_xuan_addrow" onclick="javascript:xuan_addrow($('#qtype').val());">添加选项</a></p>
		                	<div id="key_setting" class="easyui-panel" title="" style="width:710px;height:300px;padding:10px;border-radius:5px">
					            <table class="stable" style="width:700px;text-align:left" id="key_setting_table"><tbody><tr><td>选项A</td><td><input type="radio" class="validate[required]" name="skey" value="A"></td><td><textarea rows="2" cols="40" name="soption" class="validate[required]" id="toption_A"></textarea></td><td>额外答案：</td><td><textarea rows="2" cols="5" name="sextend" id="textend_A"></textarea></td><td> <a href="javascript:;" onclick="$(this).parent().parent().remove()" class="easyui-linkbutton" data-options="iconCls:'icon-remove'">移除</a></td></tr><tr><td>选项B</td><td><input type="radio" class="validate[required]" name="skey" value="B"></td><td><textarea rows="2" cols="40" name="soption" class="validate[required]" id="toption_B"></textarea></td><td>额外答案：</td><td><textarea rows="2" cols="5" name="sextend" id="textend_B"></textarea></td><td> <a href="javascript:;" onclick="$(this).parent().parent().remove()" class="easyui-linkbutton" data-options="iconCls:'icon-remove'">移除</a></td></tr><tr><td>选项C</td><td><input type="radio" class="validate[required]" name="skey" value="C"></td><td><textarea rows="2" cols="40" name="soption" class="validate[required]" id="toption_C"></textarea></td><td>额外答案：</td><td><textarea rows="2" cols="5" name="sextend" id="textend_C"></textarea></td><td> <a href="javascript:;" onclick="$(this).parent().parent().remove()" class="easyui-linkbutton" data-options="iconCls:'icon-remove'">移除</a></td></tr><tr><td>选项D</td><td><input type="radio" class="validate[required]" name="skey" value="D"></td><td><textarea rows="2" cols="40" name="soption" class="validate[required]" id="toption_D"></textarea></td><td>额外答案：</td><td><textarea rows="2" cols="5" name="sextend" id="textend_D"></textarea></td><td> <a href="javascript:;" onclick="$(this).parent().parent().remove()" class="easyui-linkbutton" data-options="iconCls:'icon-remove'">移除</a></td></tr></tbody></table>
						    </div>
		                </td>
	                </tr>
	                <tr>
	                    <td>试题解析:</td>
	                    <td><textarea class="easyui-validatebox" style="width: 710px;" id="add_key_desc" name="key_desc" data-options="required:true"></textarea></td>
	                </tr>
	            </table>
	            
	         </form>
         </div>
         
         <div id="add-dlg-buttons">
	        <a id="add-save-button" href="javascript:void(0)" class="easyui-linkbutton">保存</a>
	        <a id="add-close-button" href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#add').dialog('close')">关闭</a>
	    </div>
    </div>
	
	<div id="search" class="easyui-dialog" title="查询条件" 
		data-options="modal:true,closed:true,iconCls:'icon-search',buttons:'#search-dlg-buttons'" 
		style="width:500px;height:200px;padding:10px;">
		
		<div style="padding:10px 0 10px 60px">
			<form id="searchForm" method="post">
	            <table>
	                <tr>
	                    <td>所属题库:</td>
		                <td><input class="easyui-combobox"
				            name="db_id"
				            data-options="
		                    url:'${pageContext.request.contextPath}/question/questionDbDatas',
		                    valueField:'id',
		                    textField:'name',
		                    panelHeight:'auto'
		            		"></td>
	                </tr>
	                <tr>
	                    <td>题目类型:</td>
	                    <td>
	                    	<select class="easyui-combobox" id="qtype" name="qtype" style="width:170px;">
					        	<option value="1" selected="selected">单选</option>
					        	<option value="2">多选</option>
					        	<option value="3">填空</option>
					        	<option value="4">判断</option>
					        	<option value="5">问答</option>
					    	</select>
						</td>
	                </tr>
	                <tr>
	                    <td>难度级别:</td>
	                    <td>
	                    	<select class="easyui-combobox" name="qlevel" style="width:170px;">
					        	<option value="1" selected="selected">易</option>
					        	<option value="2">正常</option>
					        	<option value="3">难</option>
					    	</select>
						</td>
	                </tr>
	            </table>
	            
	         </form>
         </div>
		<div id="search-dlg-buttons">
	        <a id="search-save-button" href="javascript:void(0)" class="easyui-linkbutton">查询</a>
	        <a id="search-close-button" href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#search').dialog('close')">关闭</a>
	    </div>
    </div>
    
    <div id="edit" class="easyui-dialog" title="修改试题" 
    	data-options="modal:true,closed:true,iconCls:'icon-edit',buttons:'#edit-dlg-buttons'" 
		style="width:900px;height:450px;padding:10px;">
		<div style="padding:10px 0 10px 60px">
			<form id="editForm" method="post">
	            <table>
	                <tr>
	                    <td>题目来源:</td>
	                    <td><input class="easyui-validatebox" type="text" name="qfrom" data-options="required:true"></input></td>
	                </tr>
	                <tr>
	                    <td>所属题库:</td>
		                <td><input class="easyui-combobox"
				            name="db_id"
				            data-options="
		                    url:'${pageContext.request.contextPath}/question/questionDbDatas',
		                    valueField:'id',
		                    textField:'name',
		                    panelHeight:'auto'
		            		"></td>
	                </tr>
	                <tr>
	                    <td>题目类型:</td>
	                    <td>
	                    	<select class="easyui-combobox" id="qtype" name="qtype" style="width:170px;">
					        	<option value="1" selected="selected">单选</option>
					        	<option value="2">多选</option>
					        	<option value="3">填空</option>
					        	<option value="4">判断</option>
					        	<option value="5">问答</option>
					    	</select>
						</td>
	                </tr>
	                <tr>
	                    <td>难度级别:</td>
	                    <td>
	                    	<select class="easyui-combobox" name="qlevel" style="width:170px;">
					        	<option value="1" selected="selected">易</option>
					        	<option value="2">正常</option>
					        	<option value="3">难</option>
					    	</select>
						</td>
	                </tr>
	                <tr>
	                    <td>备注:</td>
	                    <td><input class="easyui-validatebox" type="text" name="remark" data-options="required:false"></input></td>
	                </tr>
	                <tr>
	                    <td>题干内容:</td>
	                    <td><textarea class="easyui-validatebox" style="width: 710px;" name="edit_content" data-options="required:true"></textarea ></td>
	                </tr>
	                
	                <tr>
	                	<td></td>
		                <td colspan="1">
		                	<div id="key_setting" class="easyui-panel" title="试题选项" style="width:710px;height:300px;padding:10px;"
						            data-options="tools:'#tt'">
						            <table class="stable" width="700" align="left" id="key_setting_table"><tbody><tr><td>选项A</td><td><input type="radio" class="validate[required]" name="skey" value="A"></td><td><textarea rows="2" cols="40" name="soption" class="validate[required]" id="toption_A"></textarea></td><td>额外答案：</td><td><textarea rows="2" cols="5" name="sextend" id="textend_A"></textarea></td><td> <a href="javascript:;" onclick="$(this).parent().parent().remove()" class="easyui-linkbutton" data-options="iconCls:'icon-remove'">移除</a></td></tr><tr><td>选项B</td><td><input type="radio" class="validate[required]" name="skey" value="B"></td><td><textarea rows="2" cols="40" name="soption" class="validate[required]" id="toption_B"></textarea></td><td>额外答案：</td><td><textarea rows="2" cols="5" name="sextend" id="textend_B"></textarea></td><td> <a href="javascript:;" onclick="$(this).parent().parent().remove()" class="easyui-linkbutton" data-options="iconCls:'icon-remove'">移除</a></td></tr><tr><td>选项C</td><td><input type="radio" class="validate[required]" name="skey" value="C"></td><td><textarea rows="2" cols="40" name="soption" class="validate[required]" id="toption_C"></textarea></td><td>额外答案：</td><td><textarea rows="2" cols="5" name="sextend" id="textend_C"></textarea></td><td> <a href="javascript:;" onclick="$(this).parent().parent().remove()" class="easyui-linkbutton" data-options="iconCls:'icon-remove'">移除</a></td></tr><tr><td>选项D</td><td><input type="radio" class="validate[required]" name="skey" value="D"></td><td><textarea rows="2" cols="40" name="soption" class="validate[required]" id="toption_D"></textarea></td><td>额外答案：</td><td><textarea rows="2" cols="5" name="sextend" id="textend_D"></textarea></td><td> <a href="javascript:;" onclick="$(this).parent().parent().remove()" class="easyui-linkbutton" data-options="iconCls:'icon-remove'">移除</a></td></tr></tbody></table>
						    </div>
		                	<div id="tt">
						        <a href="javascript:void(0)" class="icon-add" id="btn_xuan_addrow" onclick="javascript:xuan_addrow($('#qtype').val());"></a>
						    </div>
		                	
		                </td>
	                </tr>
	                <tr>
	                    <td>试题解析:</td>
	                    <td><textarea class="easyui-validatebox" style="width: 710px;" name="edit_key_desc" data-options="required:true"></textarea></td>
	                </tr>
	            </table>
	            
	         </form>
         </div>
		<div id="edit-dlg-buttons">
	        <a id="edit-save-button" href="javascript:void(0)" class="easyui-linkbutton">保存</a>
	        <a id="edit-close-button" href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#edit').dialog('close')">关闭</a>
	    </div>
    </div>
    
</body>
</html>
