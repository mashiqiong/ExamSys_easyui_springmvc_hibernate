--------------------------------------------------------
--  File created - 星期四-三月-30-2017   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Sequence ADMIN_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ADMIN_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 86 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence ADMIN_ROLES_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ADMIN_ROLES_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 79 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence ADMIN_ROLES_SETTINGS_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ADMIN_ROLES_SETTINGS_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 217 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence CONFIG_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "CONFIG_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 20 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence EXAM_DETAIL_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "EXAM_DETAIL_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence EXAM_MAIN_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "EXAM_MAIN_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence GROUPS_NAME_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GROUPS_NAME_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 8 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence HIBERNATE_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "HIBERNATE_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 5 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence ONLINES_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ONLINES_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence PAPER_DETAIL_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "PAPER_DETAIL_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 42 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence PAPER_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "PAPER_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 30 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence PAPER_SECTION_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "PAPER_SECTION_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 28 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence PAPER_USER_GROUP_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "PAPER_USER_GROUP_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 34 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence QUESTION_DB_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "QUESTION_DB_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 27 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence QUESTION_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "QUESTION_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 64 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence QUESTION_OPTIONS_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "QUESTION_OPTIONS_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 116 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SYS_TIPS_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SYS_TIPS_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 29 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence USERS_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "USERS_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 35 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence USER_GROUPS_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "USER_GROUPS_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 41 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table ADMIN
--------------------------------------------------------

  CREATE TABLE "ADMIN" 
   (	"ID" NUMBER(11,0), 
	"ROLE_ID" NUMBER(11,0), 
	"USER_NAME" VARCHAR2(50), 
	"USER_PASS" VARCHAR2(100), 
	"PHONE" VARCHAR2(20), 
	"LOGIN_TIMES" NUMBER(11,0), 
	"CREATE_DATE" DATE, 
	"LOGIN_DATE" DATE, 
	"STATUS" VARCHAR2(1) DEFAULT 1, 
	"REMARK" VARCHAR2(50)
   ) ;
 

   COMMENT ON COLUMN "ADMIN"."ID" IS '编号';
 
   COMMENT ON COLUMN "ADMIN"."ROLE_ID" IS '角色编号';
 
   COMMENT ON COLUMN "ADMIN"."USER_NAME" IS '用户名';
 
   COMMENT ON COLUMN "ADMIN"."USER_PASS" IS '密码';
 
   COMMENT ON COLUMN "ADMIN"."PHONE" IS '手机号码';
 
   COMMENT ON COLUMN "ADMIN"."LOGIN_TIMES" IS '登录次数';
 
   COMMENT ON COLUMN "ADMIN"."CREATE_DATE" IS '创建时间';
 
   COMMENT ON COLUMN "ADMIN"."LOGIN_DATE" IS '最后登录日期';
 
   COMMENT ON COLUMN "ADMIN"."STATUS" IS '状态';
 
   COMMENT ON COLUMN "ADMIN"."REMARK" IS '备注';
 
   COMMENT ON TABLE "ADMIN"  IS '管理员角色表';
--------------------------------------------------------
--  DDL for Table ADMIN_ROLES
--------------------------------------------------------

  CREATE TABLE "ADMIN_ROLES" 
   (	"ID" NUMBER(11,0), 
	"ROLE_NAME" VARCHAR2(50), 
	"ROLE_PRIVELEGE" VARCHAR2(1000), 
	"CREATE_DATE" DATE, 
	"REMARK" VARCHAR2(50)
   ) ;
 

   COMMENT ON COLUMN "ADMIN_ROLES"."ID" IS '编号';
 
   COMMENT ON COLUMN "ADMIN_ROLES"."ROLE_NAME" IS '角色名称';
 
   COMMENT ON COLUMN "ADMIN_ROLES"."ROLE_PRIVELEGE" IS '此角色所能操作的功能项';
 
   COMMENT ON COLUMN "ADMIN_ROLES"."CREATE_DATE" IS '创建时间';
 
   COMMENT ON COLUMN "ADMIN_ROLES"."REMARK" IS '备注';
 
   COMMENT ON TABLE "ADMIN_ROLES"  IS '管理员表';
--------------------------------------------------------
--  DDL for Table ADMIN_ROLES_SETTINGS
--------------------------------------------------------

  CREATE TABLE "ADMIN_ROLES_SETTINGS" 
   (	"ID" NUMBER(11,0), 
	"NAME" VARCHAR2(20), 
	"CODE" VARCHAR2(200), 
	"PARENT_ID" NUMBER(11,0), 
	"PORDER" NUMBER(11,0) DEFAULT 0, 
	"URL" VARCHAR2(200)
   ) ;
 

   COMMENT ON COLUMN "ADMIN_ROLES_SETTINGS"."ID" IS '编号';
 
   COMMENT ON COLUMN "ADMIN_ROLES_SETTINGS"."NAME" IS '菜单或功能名称';
 
   COMMENT ON COLUMN "ADMIN_ROLES_SETTINGS"."CODE" IS '菜单或者功能代码';
 
   COMMENT ON COLUMN "ADMIN_ROLES_SETTINGS"."PARENT_ID" IS '父级菜单编号';
 
   COMMENT ON COLUMN "ADMIN_ROLES_SETTINGS"."PORDER" IS '排序号';
 
   COMMENT ON COLUMN "ADMIN_ROLES_SETTINGS"."URL" IS '超链接';
 
   COMMENT ON TABLE "ADMIN_ROLES_SETTINGS"  IS '系统功能表';
--------------------------------------------------------
--  DDL for Table CONFIG
--------------------------------------------------------

  CREATE TABLE "CONFIG" 
   (	"ID" NUMBER(11,0), 
	"NAME" VARCHAR2(20), 
	"CONFIG_KEY" VARCHAR2(100), 
	"CONFIG_VALUE" CLOB, 
	"REMARK" VARCHAR2(100)
   ) ;
 

   COMMENT ON COLUMN "CONFIG"."ID" IS '编号';
 
   COMMENT ON COLUMN "CONFIG"."NAME" IS '参数名';
 
   COMMENT ON COLUMN "CONFIG"."CONFIG_KEY" IS '参数代码';
 
   COMMENT ON COLUMN "CONFIG"."CONFIG_VALUE" IS '参数值';
 
   COMMENT ON COLUMN "CONFIG"."REMARK" IS '备注';
 
   COMMENT ON TABLE "CONFIG"  IS '系统参数表';
--------------------------------------------------------
--  DDL for Table EXAM_DETAIL
--------------------------------------------------------

  CREATE TABLE "EXAM_DETAIL" 
   (	"ID" NUMBER(11,0), 
	"MAIN_ID" NUMBER(11,0), 
	"QUESTION_ID" NUMBER(11,0), 
	"ANSWER" VARCHAR2(500), 
	"SCORE" NUMBER(11,1), 
	"STATUS" VARCHAR2(1) DEFAULT 0, 
	"REMARK" VARCHAR2(100)
   ) ;
 

   COMMENT ON COLUMN "EXAM_DETAIL"."ID" IS '编号';
 
   COMMENT ON COLUMN "EXAM_DETAIL"."MAIN_ID" IS '答题主表编号';
 
   COMMENT ON COLUMN "EXAM_DETAIL"."QUESTION_ID" IS '题目编号';
 
   COMMENT ON COLUMN "EXAM_DETAIL"."ANSWER" IS '考生答题内容';
 
   COMMENT ON COLUMN "EXAM_DETAIL"."SCORE" IS '本题得分';
 
   COMMENT ON COLUMN "EXAM_DETAIL"."STATUS" IS '考试状态，用于记录此题是否作答了,0未作答，1已作答';
 
   COMMENT ON COLUMN "EXAM_DETAIL"."REMARK" IS '备注';
 
   COMMENT ON TABLE "EXAM_DETAIL"  IS '考生答题明细表';
--------------------------------------------------------
--  DDL for Table EXAM_MAIN
--------------------------------------------------------

  CREATE TABLE "EXAM_MAIN" 
   (	"ID" NUMBER(11,0), 
	"USER_ID" NUMBER(11,0), 
	"PAPER_ID" NUMBER(11,0), 
	"START_TIME" DATE, 
	"END_TIME" DATE, 
	"SCORE" NUMBER(11,1), 
	"IP" VARCHAR2(20), 
	"STATUS" VARCHAR2(1) DEFAULT 0, 
	"REMARK" VARCHAR2(100)
   ) ;
 

   COMMENT ON COLUMN "EXAM_MAIN"."ID" IS '编号';
 
   COMMENT ON COLUMN "EXAM_MAIN"."USER_ID" IS '考试者编号';
 
   COMMENT ON COLUMN "EXAM_MAIN"."PAPER_ID" IS '试卷编号';
 
   COMMENT ON COLUMN "EXAM_MAIN"."START_TIME" IS '开始考试时间';
 
   COMMENT ON COLUMN "EXAM_MAIN"."END_TIME" IS '结束考试时间';
 
   COMMENT ON COLUMN "EXAM_MAIN"."IP" IS '登录进来的IP';
 
   COMMENT ON COLUMN "EXAM_MAIN"."STATUS" IS '考试状态，1考试中，2已经交卷，3已经评分';
 
   COMMENT ON COLUMN "EXAM_MAIN"."REMARK" IS '备注';
 
   COMMENT ON TABLE "EXAM_MAIN"  IS '考生答题主表';
--------------------------------------------------------
--  DDL for Table ONLINES
--------------------------------------------------------

  CREATE TABLE "ONLINES" 
   (	"ID" VARCHAR2(20), 
	"USER_ID" NUMBER(11,0), 
	"PAPER_ID" NUMBER(11,0), 
	"LAST_TIME" DATE, 
	"IP" VARCHAR2(20)
   ) ;
 

   COMMENT ON COLUMN "ONLINES"."USER_ID" IS '考试者编号';
 
   COMMENT ON COLUMN "ONLINES"."PAPER_ID" IS '试卷编号';
 
   COMMENT ON COLUMN "ONLINES"."LAST_TIME" IS '最后一次登录时间';
 
   COMMENT ON COLUMN "ONLINES"."IP" IS '登录进来的IP';
 
   COMMENT ON TABLE "ONLINES"  IS '考试者在线考试的时间记录表';
--------------------------------------------------------
--  DDL for Table PAPER
--------------------------------------------------------

  CREATE TABLE "PAPER" 
   (	"ID" NUMBER(11,0), 
	"ADMIN_ID" NUMBER(11,0), 
	"PAPER_NAME" VARCHAR2(100), 
	"START_TIME" TIMESTAMP (9), 
	"END_TIME" TIMESTAMP (9), 
	"PAPER_MINUTE" NUMBER(11,0), 
	"TOTAL_SCORE" NUMBER(11,0), 
	"POST_DATE" TIMESTAMP (9), 
	"SHOW_SCORE" TIMESTAMP (9), 
	"QORDER" NUMBER(11,0) DEFAULT 0, 
	"STATUS" NVARCHAR2(2) DEFAULT 1, 
	"REMARK" VARCHAR2(100)
   ) ;
 

   COMMENT ON COLUMN "PAPER"."ID" IS '编号';
 
   COMMENT ON COLUMN "PAPER"."ADMIN_ID" IS '管理员编号';
 
   COMMENT ON COLUMN "PAPER"."PAPER_NAME" IS '试卷名称';
 
   COMMENT ON COLUMN "PAPER"."END_TIME" IS '结束考试时间';
 
   COMMENT ON COLUMN "PAPER"."PAPER_MINUTE" IS '考试总时间';
 
   COMMENT ON COLUMN "PAPER"."TOTAL_SCORE" IS '考试总分数';
 
   COMMENT ON COLUMN "PAPER"."POST_DATE" IS '提交时间';
 
   COMMENT ON COLUMN "PAPER"."SHOW_SCORE" IS '公布成绩时间';
 
   COMMENT ON COLUMN "PAPER"."QORDER" IS '题目顺序，0表示自然顺序，1表示打乱随机，默认0';
 
   COMMENT ON COLUMN "PAPER"."STATUS" IS '试卷状态,1开放，-1不开放';
 
   COMMENT ON COLUMN "PAPER"."REMARK" IS '备注';
 
   COMMENT ON TABLE "PAPER"  IS '试卷表';
--------------------------------------------------------
--  DDL for Table PAPER_DETAIL
--------------------------------------------------------

  CREATE TABLE "PAPER_DETAIL" 
   (	"ID" NUMBER(11,0), 
	"PAPER_ID" NUMBER(11,0), 
	"PAPER_SECTION_ID" NUMBER(11,0), 
	"QUESTION_ID" NUMBER(11,0), 
	"SCORE" NUMBER(11,2), 
	"PORDER" NUMBER(11,0)
   ) ;
 

   COMMENT ON COLUMN "PAPER_DETAIL"."ID" IS '编号';
 
   COMMENT ON COLUMN "PAPER_DETAIL"."PAPER_ID" IS '试卷编号';
 
   COMMENT ON COLUMN "PAPER_DETAIL"."PAPER_SECTION_ID" IS '章节编号';
 
   COMMENT ON COLUMN "PAPER_DETAIL"."QUESTION_ID" IS '题目编号';
 
   COMMENT ON COLUMN "PAPER_DETAIL"."SCORE" IS '本题分数';
 
   COMMENT ON COLUMN "PAPER_DETAIL"."PORDER" IS '排序号';
 
   COMMENT ON TABLE "PAPER_DETAIL"  IS '试卷明细表';
--------------------------------------------------------
--  DDL for Table PAPER_SECTION
--------------------------------------------------------

  CREATE TABLE "PAPER_SECTION" 
   (	"ID" NUMBER(11,0), 
	"PAPER_ID" NUMBER(11,0), 
	"SECTION_NAME" VARCHAR2(50), 
	"PER_SCORE" NUMBER(11,0), 
	"REMARK" VARCHAR2(100)
   ) ;
 

   COMMENT ON COLUMN "PAPER_SECTION"."ID" IS '编号';
 
   COMMENT ON COLUMN "PAPER_SECTION"."PAPER_ID" IS '试卷编号';
 
   COMMENT ON COLUMN "PAPER_SECTION"."SECTION_NAME" IS '章节名称';
 
   COMMENT ON COLUMN "PAPER_SECTION"."PER_SCORE" IS '本章节的分数';
 
   COMMENT ON COLUMN "PAPER_SECTION"."REMARK" IS '备注';
 
   COMMENT ON TABLE "PAPER_SECTION"  IS '试卷中的章节表';
--------------------------------------------------------
--  DDL for Table PAPER_USER_GROUP
--------------------------------------------------------

  CREATE TABLE "PAPER_USER_GROUP" 
   (	"ID" NUMBER(11,0), 
	"PAPER_ID" NUMBER(11,0), 
	"USER_GROUP_ID" NUMBER(11,0)
   ) ;
 

   COMMENT ON COLUMN "PAPER_USER_GROUP"."ID" IS '编号';
 
   COMMENT ON COLUMN "PAPER_USER_GROUP"."PAPER_ID" IS '试卷编号';
 
   COMMENT ON COLUMN "PAPER_USER_GROUP"."USER_GROUP_ID" IS '用户组编号';
 
   COMMENT ON TABLE "PAPER_USER_GROUP"  IS '记录可以参与考试的用户组表';
--------------------------------------------------------
--  DDL for Table QUESTION
--------------------------------------------------------

  CREATE TABLE "QUESTION" 
   (	"ID" NUMBER(11,0), 
	"DB_ID" NUMBER(11,0), 
	"ADMIN_ID" NUMBER(11,0), 
	"QTYPE" NUMBER(11,0) DEFAULT 1, 
	"QLEVEL" NUMBER(11,0) DEFAULT 1, 
	"QFROM" VARCHAR2(50), 
	"CONTENT" CLOB, 
	"SKEY" CLOB, 
	"KEY_DESC" CLOB, 
	"CREATE_DATE" DATE, 
	"STATUS" VARCHAR2(20) DEFAULT 1, 
	"REMARK" VARCHAR2(100)
   ) ;
 

   COMMENT ON COLUMN "QUESTION"."ID" IS '编号';
 
   COMMENT ON COLUMN "QUESTION"."DB_ID" IS '题库编号';
 
   COMMENT ON COLUMN "QUESTION"."ADMIN_ID" IS '管理员编号';
 
   COMMENT ON COLUMN "QUESTION"."QTYPE" IS '1单选，2多选，3填空，4判断，5问答，默认单选题';
 
   COMMENT ON COLUMN "QUESTION"."QLEVEL" IS '难度级别，1易，2正常，3难';
 
   COMMENT ON COLUMN "QUESTION"."QFROM" IS '来源';
 
   COMMENT ON COLUMN "QUESTION"."CONTENT" IS '题干内容';
 
   COMMENT ON COLUMN "QUESTION"."SKEY" IS '标准答案';
 
   COMMENT ON COLUMN "QUESTION"."KEY_DESC" IS '试题解析';
 
   COMMENT ON COLUMN "QUESTION"."CREATE_DATE" IS '创建时间';
 
   COMMENT ON COLUMN "QUESTION"."STATUS" IS '题目状态，0不完全开放，1完全开放';
 
   COMMENT ON COLUMN "QUESTION"."REMARK" IS '备注';
 
   COMMENT ON TABLE "QUESTION"  IS '试题表';
--------------------------------------------------------
--  DDL for Table QUESTION_DB
--------------------------------------------------------

  CREATE TABLE "QUESTION_DB" 
   (	"ID" NUMBER(11,0), 
	"ADMIN_ID" NUMBER(11,0), 
	"NAME" VARCHAR2(50), 
	"CREATE_DATE" DATE, 
	"STATUS" VARCHAR2(2) DEFAULT 1, 
	"REMARK" VARCHAR2(100)
   ) ;
 

   COMMENT ON COLUMN "QUESTION_DB"."ID" IS '编号';
 
   COMMENT ON COLUMN "QUESTION_DB"."ADMIN_ID" IS '管理员编号';
 
   COMMENT ON COLUMN "QUESTION_DB"."NAME" IS '题库名称';
 
   COMMENT ON COLUMN "QUESTION_DB"."CREATE_DATE" IS '创建时间';
 
   COMMENT ON COLUMN "QUESTION_DB"."STATUS" IS '题库状态，1表示正常，-1表示锁定';
 
   COMMENT ON COLUMN "QUESTION_DB"."REMARK" IS '备注';
 
   COMMENT ON TABLE "QUESTION_DB"  IS '题库表';
--------------------------------------------------------
--  DDL for Table QUESTION_OPTIONS
--------------------------------------------------------

  CREATE TABLE "QUESTION_OPTIONS" 
   (	"ID" NUMBER(11,0), 
	"QUESTION_ID" NUMBER(11,0), 
	"SALISA" VARCHAR2(10), 
	"SOPTION" VARCHAR2(500), 
	"SEXTEND" VARCHAR2(10)
   ) ;
 

   COMMENT ON COLUMN "QUESTION_OPTIONS"."ID" IS '编号';
 
   COMMENT ON COLUMN "QUESTION_OPTIONS"."QUESTION_ID" IS '题目编号';
 
   COMMENT ON COLUMN "QUESTION_OPTIONS"."SALISA" IS '选项编号';
 
   COMMENT ON COLUMN "QUESTION_OPTIONS"."SOPTION" IS '选项描述';
 
   COMMENT ON COLUMN "QUESTION_OPTIONS"."SEXTEND" IS '面试题的扩展项';
 
   COMMENT ON TABLE "QUESTION_OPTIONS"  IS '试题选项表';
--------------------------------------------------------
--  DDL for Table SYS_TIPS
--------------------------------------------------------

  CREATE TABLE "SYS_TIPS" 
   (	"ID" NUMBER(11,0), 
	"SCODE" VARCHAR2(50), 
	"SDESC" VARCHAR2(200)
   ) ;
 

   COMMENT ON COLUMN "SYS_TIPS"."SCODE" IS '代码';
 
   COMMENT ON COLUMN "SYS_TIPS"."SDESC" IS '信息内容';
 
   COMMENT ON TABLE "SYS_TIPS"  IS '系统提示信息表';
--------------------------------------------------------
--  DDL for Table USERS
--------------------------------------------------------

  CREATE TABLE "USERS" 
   (	"ID" NUMBER(11,0), 
	"GROUP_ID" NUMBER(11,0), 
	"USER_NAME" VARCHAR2(20), 
	"USER_PASS" VARCHAR2(100), 
	"USER_NO" VARCHAR2(50), 
	"REAL_NAME" VARCHAR2(20), 
	"EMAIL" VARCHAR2(20), 
	"PHONE" VARCHAR2(20), 
	"CREATE_DATE" DATE, 
	"LOGIN_DATE" DATE, 
	"STATUS" VARCHAR2(1) DEFAULT 1, 
	"REMARK" VARCHAR2(100), 
	"LOGIN_TIMES" NUMBER(11,0) DEFAULT 0
   ) ;
 

   COMMENT ON COLUMN "USERS"."ID" IS '编号';
 
   COMMENT ON COLUMN "USERS"."GROUP_ID" IS '会员组编号';
 
   COMMENT ON COLUMN "USERS"."USER_NAME" IS '会员名';
 
   COMMENT ON COLUMN "USERS"."USER_PASS" IS '密码';
 
   COMMENT ON COLUMN "USERS"."USER_NO" IS '学号';
 
   COMMENT ON COLUMN "USERS"."REAL_NAME" IS '真实姓名';
 
   COMMENT ON COLUMN "USERS"."EMAIL" IS '邮箱';
 
   COMMENT ON COLUMN "USERS"."PHONE" IS '电话号码';
 
   COMMENT ON COLUMN "USERS"."CREATE_DATE" IS '注册时间';
 
   COMMENT ON COLUMN "USERS"."LOGIN_DATE" IS '登录日期';
 
   COMMENT ON COLUMN "USERS"."STATUS" IS '状态';
 
   COMMENT ON COLUMN "USERS"."REMARK" IS '备注';
 
   COMMENT ON COLUMN "USERS"."LOGIN_TIMES" IS '登录次数';
 
   COMMENT ON TABLE "USERS"  IS '会员表';
--------------------------------------------------------
--  DDL for Table USER_GROUPS
--------------------------------------------------------

  CREATE TABLE "USER_GROUPS" 
   (	"ID" NUMBER(11,0), 
	"GROUP_NAME" VARCHAR2(20), 
	"REMARK" VARCHAR2(100)
   ) ;
 

   COMMENT ON COLUMN "USER_GROUPS"."ID" IS '编号';
 
   COMMENT ON COLUMN "USER_GROUPS"."GROUP_NAME" IS '组名';
 
   COMMENT ON COLUMN "USER_GROUPS"."REMARK" IS '备注';
 
   COMMENT ON TABLE "USER_GROUPS"  IS '会员组表';
--------------------------------------------------------
--  DDL for Table tm_test
--------------------------------------------------------

  CREATE TABLE "tm_test" 
   (	"A" CHAR(1)
   ) ;

---------------------------------------------------
--   DATA FOR TABLE ADMIN_ROLES_SETTINGS
--   FILTER = none used
---------------------------------------------------
REM INSERTING into ADMIN_ROLES_SETTINGS
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (81,'考试成绩管理','exam_manager',null,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (4,'管理员删除','admin_delete',46,1,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (37,'试题添加','question_add',36,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (3,'管理员修改','admin_update',46,1,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (5,'管理员查询','admin_select',46,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (9,'管理员角色管理','roles_manager',null,1,'adminRoles/select');
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (36,'试题管理','question_manager',null,0,'question/select');
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (38,'试题修改','question_update',36,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (39,'试题删除','question_delete',36,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (40,'试题查询','question_selec',36,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (62,'考试时间记录管理','onlineManager',null,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (87,'考试成绩管理7','exam_manager',null,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (115,'开始考试成绩管理33','start1ExamManager',null,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (33,'题库删除','questionDb_delete',31,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (31,'题库管理','questionDb_manager',null,0,'questionDb/select');
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (32,'题库添加','questionDb_add',31,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (34,'题库修改','questionDb_update',31,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (35,'题库查询','questionDb_select',31,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (41,'试卷管理','paper_manager',null,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (42,'试卷添加','paper_add',41,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (43,'试卷修改','paper_update',41,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (44,'试卷删除','paper_delete',41,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (45,'试卷查询','paper_select',41,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (46,'管理员管理','admin_manager',null,0,'admin/select');
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (86,'考试成绩管理93','exam_manager',null,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (88,'开始成绩管理','exam_manager',null,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (90,'考试成绩管理8','exam_manager',null,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (174,'88成绩管理888','exam_update',null,null,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (173,'22考试成绩修改222','exam_manager',174,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (128,'始考试成绩管理8888','start_Manager888',null,1,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (123,'考试成绩管理000','exam_manager',null,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (198,'44','44',null,44,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (168,'1考试成绩管理many461','exam_update',null,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (126,'开始考试成绩管理8888','start_Manager',null,1,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (6,'系统功能管理','settings_manager',null,0,'adminRolesSettings/select.do');
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (7,'系统功能添加','settings_add',6,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (10,'系统参数管理','config_manager',null,0,'config/select');
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (11,'系统参数添加','config_add',10,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (12,'系统参数修改','config_update',10,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (13,'系统参数删除','config_delete',10,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (14,'系统参数查询','config_select',10,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (16,'会员组管理','userGroups_manager',null,0,'userGroups/select');
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (17,'会员组添加1','userGroups_add',16,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (18,'会员组修改','userGroups_update',16,0,'pp');
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (19,'会员组查询','userGroups_select',16,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (20,'会员组删除1','userGroups_delete',16,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (21,'会员管理','users_manager',null,0,'users/select');
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (22,'会员添加','users_add',21,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (23,'会员修改','users_update',21,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (24,'会员删除','users_delete',21,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (25,'会员查询','users_select',21,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (112,'考试成绩管理10','exam_manager',null,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (116,'考试成绩修改10','exam_manager',null,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (26,'系统提示信息管理','sysTips_manager',null,0,'sysTips/select');
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (27,'系统提示信息添加','sysTips_add',26,0,'pp');
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (28,'系统提示信息查询','sysTips_select',26,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (29,'系统提示信息修改','sysTips_update',26,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (30,'系统提示信息删除','sysTips_delete',26,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (61,'管理员添加','admin_add',46,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (1,'考试成绩管理3','exam_manager',null,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (91,'考试成绩管理9','exam_manager',null,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (170,'22成绩管理m1any4','exam_update',null,null,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (169,'2考试成绩管理many461','exam_update',170,0,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (181,'22成理m1any4','exam_update',null,null,null);
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (180,'2考试成y461','exam_update',181,0,'pp');
Insert into ADMIN_ROLES_SETTINGS (ID,NAME,CODE,PARENT_ID,PORDER,URL) values (183,'333666','33',null,6661,'pp');

---------------------------------------------------
--   END DATA FOR TABLE ADMIN_ROLES_SETTINGS
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE ONLINES
--   FILTER = none used
---------------------------------------------------
REM INSERTING into ONLINES

---------------------------------------------------
--   END DATA FOR TABLE ONLINES
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE PAPER_USER_GROUP
--   FILTER = none used
---------------------------------------------------
REM INSERTING into PAPER_USER_GROUP
Insert into PAPER_USER_GROUP (ID,PAPER_ID,USER_GROUP_ID) values (33,22,22);

---------------------------------------------------
--   END DATA FOR TABLE PAPER_USER_GROUP
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE CONFIG
--   FILTER = none used
---------------------------------------------------
REM INSERTING into CONFIG
Insert into CONFIG (ID,NAME,CONFIG_KEY,CONFIG_VALUE,REMARK) values (2,'公司名称','company',(CLOB),'用于显示在欢迎页');
Insert into CONFIG (ID,NAME,CONFIG_KEY,CONFIG_VALUE,REMARK) values (9,'士大夫 ','范德萨发',(CLOB),null);
Insert into CONFIG (ID,NAME,CONFIG_KEY,CONFIG_VALUE,REMARK) values (10,'的说法 ','很反感',(CLOB),'和规范化');
Insert into CONFIG (ID,NAME,CONFIG_KEY,CONFIG_VALUE,REMARK) values (11,'are发','多少',(CLOB),'否大 ');
Insert into CONFIG (ID,NAME,CONFIG_KEY,CONFIG_VALUE,REMARK) values (12,'发到付','士大夫',(CLOB),'范德萨否');
Insert into CONFIG (ID,NAME,CONFIG_KEY,CONFIG_VALUE,REMARK) values (13,'打死','天恢复供货',(CLOB),'黑寡妇大海');
Insert into CONFIG (ID,NAME,CONFIG_KEY,CONFIG_VALUE,REMARK) values (14,'的说法 ',null,(CLOB),null);
Insert into CONFIG (ID,NAME,CONFIG_KEY,CONFIG_VALUE,REMARK) values (15,'一条路u','一条路',(CLOB),'图');
Insert into CONFIG (ID,NAME,CONFIG_KEY,CONFIG_VALUE,REMARK) values (16,'股份的公司','发送',(CLOB),' 讽德诵功 ');
Insert into CONFIG (ID,NAME,CONFIG_KEY,CONFIG_VALUE,REMARK) values (17,'分公司答复',' 功夫大使馆',(CLOB),'功夫大使馆');
Insert into CONFIG (ID,NAME,CONFIG_KEY,CONFIG_VALUE,REMARK) values (18,'高峰山道观 ','发送发的',(CLOB),'范德萨个');
Insert into CONFIG (ID,NAME,CONFIG_KEY,CONFIG_VALUE,REMARK) values (7,'222','123',(CLOB),'2233');

---------------------------------------------------
--   END DATA FOR TABLE CONFIG
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE USERS
--   FILTER = none used
---------------------------------------------------
REM INSERTING into USERS
Insert into USERS (ID,GROUP_ID,USER_NAME,USER_PASS,USER_NO,REAL_NAME,EMAIL,PHONE,CREATE_DATE,LOGIN_DATE,STATUS,REMARK,LOGIN_TIMES) values (19,7,'啊啊','aa','啊啊','啊啊','啊啊','啊啊',to_timestamp('23-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('23-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','啊啊',0);
Insert into USERS (ID,GROUP_ID,USER_NAME,USER_PASS,USER_NO,REAL_NAME,EMAIL,PHONE,CREATE_DATE,LOGIN_DATE,STATUS,REMARK,LOGIN_TIMES) values (22,7,'功夫大使馆',' gfd sgsdf',' 功夫大使馆的','割发代首功夫大使馆','个反倒是功夫大使馆',' 功夫大使馆是否 ',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1',' 讽德诵功手动否公司法定',0);
Insert into USERS (ID,GROUP_ID,USER_NAME,USER_PASS,USER_NO,REAL_NAME,EMAIL,PHONE,CREATE_DATE,LOGIN_DATE,STATUS,REMARK,LOGIN_TIMES) values (20,7,'个双方都','fd gf ','地方够大 ','地方','多高地方','割发代首',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','发的',0);
Insert into USERS (ID,GROUP_ID,USER_NAME,USER_PASS,USER_NO,REAL_NAME,EMAIL,PHONE,CREATE_DATE,LOGIN_DATE,STATUS,REMARK,LOGIN_TIMES) values (21,7,'给对方',' gdf gd','豆腐干豆腐干','个豆腐干豆腐干','干豆腐干豆腐 ','豆腐干豆腐干',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1',' 更多个地方官',0);
Insert into USERS (ID,GROUP_ID,USER_NAME,USER_PASS,USER_NO,REAL_NAME,EMAIL,PHONE,CREATE_DATE,LOGIN_DATE,STATUS,REMARK,LOGIN_TIMES) values (23,7,'爱迪生发多少 ','fdsafdsa','发过火过分的话回顾','发送','还是功夫护手霜',' 范德萨发人生巅峰',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','发的撒发生的',0);
Insert into USERS (ID,GROUP_ID,USER_NAME,USER_PASS,USER_NO,REAL_NAME,EMAIL,PHONE,CREATE_DATE,LOGIN_DATE,STATUS,REMARK,LOGIN_TIMES) values (24,7,'的撒范德萨','fdsaf ds ',' 范德萨发手动','范德萨发手动','的所发生的','第三方阿萨德',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','的沙发斯蒂芬 ',0);
Insert into USERS (ID,GROUP_ID,USER_NAME,USER_PASS,USER_NO,REAL_NAME,EMAIL,PHONE,CREATE_DATE,LOGIN_DATE,STATUS,REMARK,LOGIN_TIMES) values (25,7,'水电费','sdfasd ','范德萨发',' 地方萨芬 ','的萨芬撒点 ','的说法 ',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','的撒发多少',0);
Insert into USERS (ID,GROUP_ID,USER_NAME,USER_PASS,USER_NO,REAL_NAME,EMAIL,PHONE,CREATE_DATE,LOGIN_DATE,STATUS,REMARK,LOGIN_TIMES) values (26,7,'的说法','dsfasdf ','范德萨发生的','第三方手动','的撒发多少分士大夫',' 范德萨范德萨',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','范德萨发双方都',0);
Insert into USERS (ID,GROUP_ID,USER_NAME,USER_PASS,USER_NO,REAL_NAME,EMAIL,PHONE,CREATE_DATE,LOGIN_DATE,STATUS,REMARK,LOGIN_TIMES) values (27,7,'胜多负少大法师 ','dsfasdf ','的沙发斯蒂芬安抚','范德萨发生的','范德萨发生的安抚',' 范德萨发',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','的说法水电费',0);
Insert into USERS (ID,GROUP_ID,USER_NAME,USER_PASS,USER_NO,REAL_NAME,EMAIL,PHONE,CREATE_DATE,LOGIN_DATE,STATUS,REMARK,LOGIN_TIMES) values (28,7,'的所发生的','dfas f','黑寡妇大海','的说法','好的 ','的说法',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','大水电费',0);
Insert into USERS (ID,GROUP_ID,USER_NAME,USER_PASS,USER_NO,REAL_NAME,EMAIL,PHONE,CREATE_DATE,LOGIN_DATE,STATUS,REMARK,LOGIN_TIMES) values (29,7,'的说法','dsf a ',' 光辉','多少发','发个个','的说法',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','否',0);
Insert into USERS (ID,GROUP_ID,USER_NAME,USER_PASS,USER_NO,REAL_NAME,EMAIL,PHONE,CREATE_DATE,LOGIN_DATE,STATUS,REMARK,LOGIN_TIMES) values (30,7,'多发点','dsf asdff','的说法啊','地方萨芬',' 范德萨发','地方萨芬',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','的说法啊',0);
Insert into USERS (ID,GROUP_ID,USER_NAME,USER_PASS,USER_NO,REAL_NAME,EMAIL,PHONE,CREATE_DATE,LOGIN_DATE,STATUS,REMARK,LOGIN_TIMES) values (14,7,'226','226','226','226','226','226',to_timestamp('10-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('10-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','226',0);
Insert into USERS (ID,GROUP_ID,USER_NAME,USER_PASS,USER_NO,REAL_NAME,EMAIL,PHONE,CREATE_DATE,LOGIN_DATE,STATUS,REMARK,LOGIN_TIMES) values (31,28,'99','99','99','99','99','99',to_timestamp('03-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('03-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','99',0);

---------------------------------------------------
--   END DATA FOR TABLE USERS
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE ADMIN_ROLES
--   FILTER = none used
---------------------------------------------------
REM INSERTING into ADMIN_ROLES
Insert into ADMIN_ROLES (ID,ROLE_NAME,ROLE_PRIVELEGE,CREATE_DATE,REMARK) values (1,'超级管理员','26,27,28,29,30,21,22,23,24,25,16,17,18,19,20,10,11,12,13,14,6,7,126,46,4,3,5,61,41,42,43,44,45,31,33,32,34,35,36,37,38,39,40,9',to_timestamp('05-1月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'请勿删除！');
Insert into ADMIN_ROLES (ID,ROLE_NAME,ROLE_PRIVELEGE,CREATE_DATE,REMARK) values (59,'物流组1','16,17,18,19,20',to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'物流组1');
Insert into ADMIN_ROLES (ID,ROLE_NAME,ROLE_PRIVELEGE,CREATE_DATE,REMARK) values (75,'财务组2','41,42,43,44,45',to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'财务组2');
Insert into ADMIN_ROLES (ID,ROLE_NAME,ROLE_PRIVELEGE,CREATE_DATE,REMARK) values (28,'scxds','26,30,29,28,27',to_timestamp('24-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'sadc');
Insert into ADMIN_ROLES (ID,ROLE_NAME,ROLE_PRIVELEGE,CREATE_DATE,REMARK) values (29,'范德萨发 ','26,30,29,28,27,21,25,24,23,22,16,20,19,18,17,10,14,13,12,11,6,7,46,61,5,3,4,41,45,44,43,42,31,35,34,32,33,36,40,39,38,37,9',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'的说法 ');
Insert into ADMIN_ROLES (ID,ROLE_NAME,ROLE_PRIVELEGE,CREATE_DATE,REMARK) values (33,'没VB吗','26,30,29,28,27,21,25,24,23,22,16,20,19,18,17,10,14,13,12,11,6,7,46,61,5,3,4,41,45,44,43,42,31,35,34,32,33,36,40,39,38,37,9',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'美女MV');
Insert into ADMIN_ROLES (ID,ROLE_NAME,ROLE_PRIVELEGE,CREATE_DATE,REMARK) values (34,'很过分的好','26,30,29,28,27,21,25,24,23,22,16,20,19,18,17,10,14,13,12,11,6,7,46,61,5,3,4,41,45,44,43,42,31,35,34,32,33,36,40,39,38,37,9',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'花费更多回复的');
Insert into ADMIN_ROLES (ID,ROLE_NAME,ROLE_PRIVELEGE,CREATE_DATE,REMARK) values (35,'好观复嘟嘟','26,30,29,28,27,21,25,24,23,22,16,20,19,18,17,10,14,13,12,11,6,7,46,61,5,3,4,41,45,44,43,42,31,35,34,32,33,36,40,39,38,37,9',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'好的发个好');
Insert into ADMIN_ROLES (ID,ROLE_NAME,ROLE_PRIVELEGE,CREATE_DATE,REMARK) values (55,'物流组','16,17,18,19,20',to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'物流组');
Insert into ADMIN_ROLES (ID,ROLE_NAME,ROLE_PRIVELEGE,CREATE_DATE,REMARK) values (21,'测试1','6,7,9,5,2',to_timestamp('18-1月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'999');
Insert into ADMIN_ROLES (ID,ROLE_NAME,ROLE_PRIVELEGE,CREATE_DATE,REMARK) values (4,'普通管理员','21,25,24,23,22,16,20,19,18,17,2,3,41,42,43,44,45,31,35,34,32,33,36,40,39,38,37',to_timestamp('13-1月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'测试');
Insert into ADMIN_ROLES (ID,ROLE_NAME,ROLE_PRIVELEGE,CREATE_DATE,REMARK) values (22,'测试2','6,7',to_timestamp('18-1月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'三');
Insert into ADMIN_ROLES (ID,ROLE_NAME,ROLE_PRIVELEGE,CREATE_DATE,REMARK) values (77,'财务组3','41,42,43,44,45',to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'财务组3');
Insert into ADMIN_ROLES (ID,ROLE_NAME,ROLE_PRIVELEGE,CREATE_DATE,REMARK) values (49,'财务组','41,42,43,44,45',to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'财务组');
Insert into ADMIN_ROLES (ID,ROLE_NAME,ROLE_PRIVELEGE,CREATE_DATE,REMARK) values (53,'财务组1','41,42,43,44,45',to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'财务组1');

---------------------------------------------------
--   END DATA FOR TABLE ADMIN_ROLES
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE QUESTION
--   FILTER = none used
---------------------------------------------------
REM INSERTING into QUESTION
Insert into QUESTION (ID,DB_ID,ADMIN_ID,QTYPE,QLEVEL,QFROM,CONTENT,SKEY,KEY_DESC,CREATE_DATE,STATUS,REMARK) values (51,9,1,3,1,'考试网',(CLOB),(CLOB),(CLOB),to_timestamp('22-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','填空题');
Insert into QUESTION (ID,DB_ID,ADMIN_ID,QTYPE,QLEVEL,QFROM,CONTENT,SKEY,KEY_DESC,CREATE_DATE,STATUS,REMARK) values (42,15,1,1,2,'12',(CLOB),(CLOB),(CLOB),to_timestamp('16-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'0','12');
Insert into QUESTION (ID,DB_ID,ADMIN_ID,QTYPE,QLEVEL,QFROM,CONTENT,SKEY,KEY_DESC,CREATE_DATE,STATUS,REMARK) values (50,9,1,3,2,'考试网',(CLOB),(CLOB),(CLOB),to_timestamp('22-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','填空题');
Insert into QUESTION (ID,DB_ID,ADMIN_ID,QTYPE,QLEVEL,QFROM,CONTENT,SKEY,KEY_DESC,CREATE_DATE,STATUS,REMARK) values (52,5,1,3,1,'sadf',(CLOB),(CLOB),(CLOB),to_timestamp('22-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','fdsf');
Insert into QUESTION (ID,DB_ID,ADMIN_ID,QTYPE,QLEVEL,QFROM,CONTENT,SKEY,KEY_DESC,CREATE_DATE,STATUS,REMARK) values (59,15,1,1,1,'dfdf',(CLOB),(CLOB),(CLOB),to_timestamp('18-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','dsfasf');
Insert into QUESTION (ID,DB_ID,ADMIN_ID,QTYPE,QLEVEL,QFROM,CONTENT,SKEY,KEY_DESC,CREATE_DATE,STATUS,REMARK) values (60,15,1,1,1,'dfdf',(CLOB),(CLOB),(CLOB),to_timestamp('18-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','dsfasf');
Insert into QUESTION (ID,DB_ID,ADMIN_ID,QTYPE,QLEVEL,QFROM,CONTENT,SKEY,KEY_DESC,CREATE_DATE,STATUS,REMARK) values (54,15,1,1,1,'功夫大使馆 ',(CLOB),(CLOB),(CLOB),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1',' 顺丰到付');
Insert into QUESTION (ID,DB_ID,ADMIN_ID,QTYPE,QLEVEL,QFROM,CONTENT,SKEY,KEY_DESC,CREATE_DATE,STATUS,REMARK) values (55,15,1,1,1,'的说法',(CLOB),(CLOB),(CLOB),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','范德萨');
Insert into QUESTION (ID,DB_ID,ADMIN_ID,QTYPE,QLEVEL,QFROM,CONTENT,SKEY,KEY_DESC,CREATE_DATE,STATUS,REMARK) values (49,15,1,4,3,'2222',(CLOB),(CLOB),(CLOB),to_timestamp('20-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','2222');
Insert into QUESTION (ID,DB_ID,ADMIN_ID,QTYPE,QLEVEL,QFROM,CONTENT,SKEY,KEY_DESC,CREATE_DATE,STATUS,REMARK) values (56,15,1,1,1,'第三方 ',(CLOB),(CLOB),(CLOB),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','范德萨发啊 ');
Insert into QUESTION (ID,DB_ID,ADMIN_ID,QTYPE,QLEVEL,QFROM,CONTENT,SKEY,KEY_DESC,CREATE_DATE,STATUS,REMARK) values (57,15,1,1,1,'发生 ',(CLOB),(CLOB),(CLOB),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','发生大啊');
Insert into QUESTION (ID,DB_ID,ADMIN_ID,QTYPE,QLEVEL,QFROM,CONTENT,SKEY,KEY_DESC,CREATE_DATE,STATUS,REMARK) values (58,15,1,1,1,'范德萨安抚',(CLOB),(CLOB),(CLOB),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','的说法as ');
Insert into QUESTION (ID,DB_ID,ADMIN_ID,QTYPE,QLEVEL,QFROM,CONTENT,SKEY,KEY_DESC,CREATE_DATE,STATUS,REMARK) values (44,15,1,1,1,'dfdf',(CLOB),(CLOB),(CLOB),to_timestamp('18-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','dsfasf');
Insert into QUESTION (ID,DB_ID,ADMIN_ID,QTYPE,QLEVEL,QFROM,CONTENT,SKEY,KEY_DESC,CREATE_DATE,STATUS,REMARK) values (39,7,1,5,3,'DDD',(CLOB),(CLOB),(CLOB),to_timestamp('18-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','SSSS');
Insert into QUESTION (ID,DB_ID,ADMIN_ID,QTYPE,QLEVEL,QFROM,CONTENT,SKEY,KEY_DESC,CREATE_DATE,STATUS,REMARK) values (63,9,1,1,1,'te',(CLOB),(CLOB),(CLOB),to_timestamp('13-3月 -17 08.55.09.000000000 下午','DD-MON-RR HH.MI.SS.FF AM'),'1','tt');
Insert into QUESTION (ID,DB_ID,ADMIN_ID,QTYPE,QLEVEL,QFROM,CONTENT,SKEY,KEY_DESC,CREATE_DATE,STATUS,REMARK) values (53,5,1,3,2,'111',(CLOB),(CLOB),(CLOB),to_timestamp('22-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','11111');

---------------------------------------------------
--   END DATA FOR TABLE QUESTION
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE QUESTION_OPTIONS
--   FILTER = none used
---------------------------------------------------
REM INSERTING into QUESTION_OPTIONS
Insert into QUESTION_OPTIONS (ID,QUESTION_ID,SALISA,SOPTION,SEXTEND) values (95,51,'2',null,null);
Insert into QUESTION_OPTIONS (ID,QUESTION_ID,SALISA,SOPTION,SEXTEND) values (96,52,'fasdf',null,null);
Insert into QUESTION_OPTIONS (ID,QUESTION_ID,SALISA,SOPTION,SEXTEND) values (61,42,'1','123',null);
Insert into QUESTION_OPTIONS (ID,QUESTION_ID,SALISA,SOPTION,SEXTEND) values (62,42,'2','12',null);
Insert into QUESTION_OPTIONS (ID,QUESTION_ID,SALISA,SOPTION,SEXTEND) values (94,50,'2',null,null);
Insert into QUESTION_OPTIONS (ID,QUESTION_ID,SALISA,SOPTION,SEXTEND) values (93,49,'错误',null,null);
Insert into QUESTION_OPTIONS (ID,QUESTION_ID,SALISA,SOPTION,SEXTEND) values (98,54,'第三方 ','范德萨安抚的说法冯绍峰',null);
Insert into QUESTION_OPTIONS (ID,QUESTION_ID,SALISA,SOPTION,SEXTEND) values (99,55,'范德萨发生','的说法十大否发',null);
Insert into QUESTION_OPTIONS (ID,QUESTION_ID,SALISA,SOPTION,SEXTEND) values (100,56,'大发生大',' 的萨芬撒点分',null);
Insert into QUESTION_OPTIONS (ID,QUESTION_ID,SALISA,SOPTION,SEXTEND) values (101,57,'范德萨','多少发多少 ',null);
Insert into QUESTION_OPTIONS (ID,QUESTION_ID,SALISA,SOPTION,SEXTEND) values (92,49,'正确',null,null);
Insert into QUESTION_OPTIONS (ID,QUESTION_ID,SALISA,SOPTION,SEXTEND) values (102,58,'范德萨否','多少发生大 ',null);
Insert into QUESTION_OPTIONS (ID,QUESTION_ID,SALISA,SOPTION,SEXTEND) values (112,63,'a','aa',null);
Insert into QUESTION_OPTIONS (ID,QUESTION_ID,SALISA,SOPTION,SEXTEND) values (113,63,'d','aa',null);
Insert into QUESTION_OPTIONS (ID,QUESTION_ID,SALISA,SOPTION,SEXTEND) values (114,63,'b','dd',null);
Insert into QUESTION_OPTIONS (ID,QUESTION_ID,SALISA,SOPTION,SEXTEND) values (115,63,'f','aaddf',null);
Insert into QUESTION_OPTIONS (ID,QUESTION_ID,SALISA,SOPTION,SEXTEND) values (97,53,'2',null,null);

---------------------------------------------------
--   END DATA FOR TABLE QUESTION_OPTIONS
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE SYS_TIPS
--   FILTER = none used
---------------------------------------------------
REM INSERTING into SYS_TIPS
Insert into SYS_TIPS (ID,SCODE,SDESC) values (14,'二特',' 退热贴惹我他');
Insert into SYS_TIPS (ID,SCODE,SDESC) values (15,'业务我','假音多发几个');
Insert into SYS_TIPS (ID,SCODE,SDESC) values (17,'特认为台湾儿童','额让他温热委托人');
Insert into SYS_TIPS (ID,SCODE,SDESC) values (18,'他温热','水电费公司的风格');
Insert into SYS_TIPS (ID,SCODE,SDESC) values (19,'防守打法','反倒是更好双方都');
Insert into SYS_TIPS (ID,SCODE,SDESC) values (20,'热其他热问题吧','京东方国际的');
Insert into SYS_TIPS (ID,SCODE,SDESC) values (21,'盎然的沟通  ','二哥水电费广东省公司的');
Insert into SYS_TIPS (ID,SCODE,SDESC) values (22,'地方根深蒂固富商大贾','的师傅告诉对方感受到 ');
Insert into SYS_TIPS (ID,SCODE,SDESC) values (23,'分公司答复是',' 股份收到过水电费 ');
Insert into SYS_TIPS (ID,SCODE,SDESC) values (24,'发个胜多负4少告诉对方 ','更多三个地方三国杀');
Insert into SYS_TIPS (ID,SCODE,SDESC) values (25,'功夫大使馆水电费','公司股份公司');

---------------------------------------------------
--   END DATA FOR TABLE SYS_TIPS
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE EXAM_DETAIL
--   FILTER = none used
---------------------------------------------------
REM INSERTING into EXAM_DETAIL

---------------------------------------------------
--   END DATA FOR TABLE EXAM_DETAIL
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE ADMIN
--   FILTER = none used
---------------------------------------------------
REM INSERTING into ADMIN
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (1,1,'admin','admin','18278969155',0,to_timestamp('08-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('08-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','我是超级管理员，请不要删除');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (7,4,'test','test','111',0,to_timestamp('08-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('08-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','222');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (45,59,'王五1','1234','13778786659',1,to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1',null);
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (46,59,'陆六子1','1234','13778786656',1,to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1',null);
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (77,75,'张三2','1234','13778969122',0,to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','三张');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (78,75,'李四2','1234','13778969123',0,to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','李四2');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (12,1,'范德萨否','sd fsdfa ','范德萨发',0,to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','的说法');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (13,1,'的说法 ','dsf asd','的法萨芬 ',0,to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','第三方十大');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (14,1,'范德萨发','fdsf a ','的说法',0,to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1',' 范德萨发');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (15,1,'的说法否夺三','ds fads',' 范德萨发',0,to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','范德萨否');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (16,1,'的说法啊','ds fasd ','范德萨',0,to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','否大师傅');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (19,1,'11','fdsaf s','范德萨否',0,to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','范德萨发十大');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (20,1,'35','33','33',0,to_timestamp('03-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('03-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','33');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (21,1,'336','66','767',0,to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','767');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (22,1,'4强5他','dfgs','广东分公司',0,to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','多少个');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (23,1,'股份的公司','gfh ','挂机狗',0,to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','巨热UR');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (24,1,'热武器若','wetq  ','未确认 ',0,to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','五二 ');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (43,55,'王五','1234','13778786659',1,to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1',null);
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (44,55,'陆六子','1234','13778786656',1,to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1',null);
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (82,77,'李四3','1234','13778969123',0,to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','李四3');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (81,77,'张三3','1234','13778969122',0,to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','三张');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (85,4,'三生痴狂','12306','10038收到',0,to_timestamp('17-3月 -17 09.53.11.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('17-3月 -17 09.53.11.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'0',null);
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (31,49,'张三','1234','13778969122',0,to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','三张');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (40,53,'李四1','1234','13778969123',0,to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','李四');
Insert into ADMIN (ID,ROLE_ID,USER_NAME,USER_PASS,PHONE,LOGIN_TIMES,CREATE_DATE,LOGIN_DATE,STATUS,REMARK) values (39,53,'张三1','1234','13778969122',0,to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('07-3月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','三张');

---------------------------------------------------
--   END DATA FOR TABLE ADMIN
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE USER_GROUPS
--   FILTER = none used
---------------------------------------------------
REM INSERTING into USER_GROUPS
Insert into USER_GROUPS (ID,GROUP_NAME,REMARK) values (23,' 范德萨发','的萨芬撒点否');
Insert into USER_GROUPS (ID,GROUP_NAME,REMARK) values (21,'发顺丰 ','多少发');
Insert into USER_GROUPS (ID,GROUP_NAME,REMARK) values (22,'范德萨 ','第三方sad');
Insert into USER_GROUPS (ID,GROUP_NAME,REMARK) values (24,'范德萨发 ','的说法大师傅');
Insert into USER_GROUPS (ID,GROUP_NAME,REMARK) values (25,'大师傅阿萨德 ','多少法师的发');
Insert into USER_GROUPS (ID,GROUP_NAME,REMARK) values (26,'的所发生的','大师傅啊');
Insert into USER_GROUPS (ID,GROUP_NAME,REMARK) values (27,'打发打发',' 多少否');
Insert into USER_GROUPS (ID,GROUP_NAME,REMARK) values (28,'发送否',' 第三方');
Insert into USER_GROUPS (ID,GROUP_NAME,REMARK) values (29,'发顺丰的',' 反倒是更好好');
Insert into USER_GROUPS (ID,GROUP_NAME,REMARK) values (1,'99','99');
Insert into USER_GROUPS (ID,GROUP_NAME,REMARK) values (6,'111111','111111');
Insert into USER_GROUPS (ID,GROUP_NAME,REMARK) values (7,'java6','java6');
Insert into USER_GROUPS (ID,GROUP_NAME,REMARK) values (2,'11','11');
Insert into USER_GROUPS (ID,GROUP_NAME,REMARK) values (8,'组2','2');
Insert into USER_GROUPS (ID,GROUP_NAME,REMARK) values (3,'111','111');
Insert into USER_GROUPS (ID,GROUP_NAME,REMARK) values (5,'111111','111111');
Insert into USER_GROUPS (ID,GROUP_NAME,REMARK) values (4,'组1','1');

---------------------------------------------------
--   END DATA FOR TABLE USER_GROUPS
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE PAPER
--   FILTER = none used
---------------------------------------------------
REM INSERTING into PAPER
Insert into PAPER (ID,ADMIN_ID,PAPER_NAME,START_TIME,END_TIME,PAPER_MINUTE,TOTAL_SCORE,POST_DATE,SHOW_SCORE,QORDER,STATUS,REMARK) values (18,1,'888',to_timestamp('21-2月 -17 08.08.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('21-2月 -17 08.08.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),888,888,to_timestamp('21-2月 -17 08.08.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('21-2月 -17 08.08.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),1,'1','888');
Insert into PAPER (ID,ADMIN_ID,PAPER_NAME,START_TIME,END_TIME,PAPER_MINUTE,TOTAL_SCORE,POST_DATE,SHOW_SCORE,QORDER,STATUS,REMARK) values (21,1,'范德萨否',to_timestamp('28-2月 -17 01.01.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 01.01.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),26,100,to_timestamp('28-2月 -17 08.49.27.050000000 下午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 08.49.27.052000000 下午','DD-MON-RR HH.MI.SS.FF AM'),0,'1','的说法啊');
Insert into PAPER (ID,ADMIN_ID,PAPER_NAME,START_TIME,END_TIME,PAPER_MINUTE,TOTAL_SCORE,POST_DATE,SHOW_SCORE,QORDER,STATUS,REMARK) values (22,1,'手动阀 ',to_timestamp('28-2月 -17 01.01.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 11.01.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),77,33,to_timestamp('28-2月 -17 08.49.57.585000000 下午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 08.49.57.588000000 下午','DD-MON-RR HH.MI.SS.FF AM'),0,'1','否萨芬');
Insert into PAPER (ID,ADMIN_ID,PAPER_NAME,START_TIME,END_TIME,PAPER_MINUTE,TOTAL_SCORE,POST_DATE,SHOW_SCORE,QORDER,STATUS,REMARK) values (23,1,'而提前 ',to_timestamp('28-2月 -17 03.03.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 04.02.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),30,100,to_timestamp('28-2月 -17 08.51.08.210000000 下午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 08.51.08.213000000 下午','DD-MON-RR HH.MI.SS.FF AM'),0,'1','阿尔我我');
Insert into PAPER (ID,ADMIN_ID,PAPER_NAME,START_TIME,END_TIME,PAPER_MINUTE,TOTAL_SCORE,POST_DATE,SHOW_SCORE,QORDER,STATUS,REMARK) values (24,1,'发生大否',to_timestamp('28-2月 -17 01.01.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 01.01.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),60,100,to_timestamp('28-2月 -17 08.52.19.729000000 下午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 08.52.19.732000000 下午','DD-MON-RR HH.MI.SS.FF AM'),0,'1','发送否');
Insert into PAPER (ID,ADMIN_ID,PAPER_NAME,START_TIME,END_TIME,PAPER_MINUTE,TOTAL_SCORE,POST_DATE,SHOW_SCORE,QORDER,STATUS,REMARK) values (25,1,'大 第三方 ',to_timestamp('28-2月 -17 01.01.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 01.01.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),60,100,to_timestamp('28-2月 -17 08.52.55.330000000 下午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 08.52.55.332000000 下午','DD-MON-RR HH.MI.SS.FF AM'),0,'1','手动阀');
Insert into PAPER (ID,ADMIN_ID,PAPER_NAME,START_TIME,END_TIME,PAPER_MINUTE,TOTAL_SCORE,POST_DATE,SHOW_SCORE,QORDER,STATUS,REMARK) values (26,1,'法师的发',to_timestamp('28-2月 -17 01.01.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 01.01.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),60,100,to_timestamp('28-2月 -17 08.53.26.426000000 下午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 08.53.26.428000000 下午','DD-MON-RR HH.MI.SS.FF AM'),0,'1','防守打法');
Insert into PAPER (ID,ADMIN_ID,PAPER_NAME,START_TIME,END_TIME,PAPER_MINUTE,TOTAL_SCORE,POST_DATE,SHOW_SCORE,QORDER,STATUS,REMARK) values (27,1,'范德萨发',to_timestamp('28-2月 -17 01.01.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 01.01.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),60,100,to_timestamp('28-2月 -17 08.53.54.945000000 下午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 08.53.54.947000000 下午','DD-MON-RR HH.MI.SS.FF AM'),0,'1','发顺丰');
Insert into PAPER (ID,ADMIN_ID,PAPER_NAME,START_TIME,END_TIME,PAPER_MINUTE,TOTAL_SCORE,POST_DATE,SHOW_SCORE,QORDER,STATUS,REMARK) values (28,1,'发生大否 ',to_timestamp('28-2月 -17 01.01.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 01.01.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),60,100,to_timestamp('28-2月 -17 08.54.32.889000000 下午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 08.54.32.892000000 下午','DD-MON-RR HH.MI.SS.FF AM'),0,'1','发送');
Insert into PAPER (ID,ADMIN_ID,PAPER_NAME,START_TIME,END_TIME,PAPER_MINUTE,TOTAL_SCORE,POST_DATE,SHOW_SCORE,QORDER,STATUS,REMARK) values (29,1,'发送范德萨',to_timestamp('28-2月 -17 01.01.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 01.01.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),60,100,to_timestamp('28-2月 -17 08.55.00.977000000 下午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('28-2月 -17 08.55.00.980000000 下午','DD-MON-RR HH.MI.SS.FF AM'),0,'1','发到付啊');
Insert into PAPER (ID,ADMIN_ID,PAPER_NAME,START_TIME,END_TIME,PAPER_MINUTE,TOTAL_SCORE,POST_DATE,SHOW_SCORE,QORDER,STATUS,REMARK) values (8,1,'dfdsaf112',to_timestamp('21-2月 -17 12.12.00.000000000 下午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('21-2月 -17 03.13.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),1111,111,to_timestamp('21-2月 -17 03.13.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('21-2月 -17 03.13.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),1,'1','11');
Insert into PAPER (ID,ADMIN_ID,PAPER_NAME,START_TIME,END_TIME,PAPER_MINUTE,TOTAL_SCORE,POST_DATE,SHOW_SCORE,QORDER,STATUS,REMARK) values (16,1,'12',to_timestamp('21-2月 -17 12.01.00.000000000 下午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('21-2月 -17 03.02.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),12,12,to_timestamp('21-2月 -17 01.03.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('21-2月 -17 03.03.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),1,'1','12');
Insert into PAPER (ID,ADMIN_ID,PAPER_NAME,START_TIME,END_TIME,PAPER_MINUTE,TOTAL_SCORE,POST_DATE,SHOW_SCORE,QORDER,STATUS,REMARK) values (9,1,'3213',to_timestamp('21-2月 -17 09.09.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('21-2月 -17 11.31.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),123,32,to_timestamp('21-2月 -17 11.31.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),to_timestamp('21-2月 -17 11.31.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),1,'1','21');

---------------------------------------------------
--   END DATA FOR TABLE PAPER
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE PAPER_DETAIL
--   FILTER = none used
---------------------------------------------------
REM INSERTING into PAPER_DETAIL
Insert into PAPER_DETAIL (ID,PAPER_ID,PAPER_SECTION_ID,QUESTION_ID,SCORE,PORDER) values (18,8,15,44,0,0);
Insert into PAPER_DETAIL (ID,PAPER_ID,PAPER_SECTION_ID,QUESTION_ID,SCORE,PORDER) values (41,28,26,56,2,0);
Insert into PAPER_DETAIL (ID,PAPER_ID,PAPER_SECTION_ID,QUESTION_ID,SCORE,PORDER) values (16,8,15,44,0,0);
Insert into PAPER_DETAIL (ID,PAPER_ID,PAPER_SECTION_ID,QUESTION_ID,SCORE,PORDER) values (17,8,15,39,0,0);
Insert into PAPER_DETAIL (ID,PAPER_ID,PAPER_SECTION_ID,QUESTION_ID,SCORE,PORDER) values (19,9,8,44,2,0);
Insert into PAPER_DETAIL (ID,PAPER_ID,PAPER_SECTION_ID,QUESTION_ID,SCORE,PORDER) values (21,18,19,44,6,0);

---------------------------------------------------
--   END DATA FOR TABLE PAPER_DETAIL
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE tm_test
--   FILTER = none used
---------------------------------------------------
REM INSERTING into "tm_test"

---------------------------------------------------
--   END DATA FOR TABLE tm_test
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE QUESTION_DB
--   FILTER = none used
---------------------------------------------------
REM INSERTING into QUESTION_DB
Insert into QUESTION_DB (ID,ADMIN_ID,NAME,CREATE_DATE,STATUS,REMARK) values (9,1,'数学题',to_timestamp('11-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','难2');
Insert into QUESTION_DB (ID,ADMIN_ID,NAME,CREATE_DATE,STATUS,REMARK) values (10,1,'英语题',to_timestamp('11-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'-1','难1');
Insert into QUESTION_DB (ID,ADMIN_ID,NAME,CREATE_DATE,STATUS,REMARK) values (19,1,'发是 ',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','第三方打死');
Insert into QUESTION_DB (ID,ADMIN_ID,NAME,CREATE_DATE,STATUS,REMARK) values (20,1,'发生大否',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','手动否');
Insert into QUESTION_DB (ID,ADMIN_ID,NAME,CREATE_DATE,STATUS,REMARK) values (21,1,'法萨芬',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','范德萨 ');
Insert into QUESTION_DB (ID,ADMIN_ID,NAME,CREATE_DATE,STATUS,REMARK) values (22,1,'水电费啊',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','水电费啊啊士大夫');
Insert into QUESTION_DB (ID,ADMIN_ID,NAME,CREATE_DATE,STATUS,REMARK) values (23,1,'发的 ',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','发到付');
Insert into QUESTION_DB (ID,ADMIN_ID,NAME,CREATE_DATE,STATUS,REMARK) values (25,1,'十大发',to_timestamp('28-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','发生大 ');
Insert into QUESTION_DB (ID,ADMIN_ID,NAME,CREATE_DATE,STATUS,REMARK) values (5,1,'政治题',to_timestamp('11-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'-1','难');
Insert into QUESTION_DB (ID,ADMIN_ID,NAME,CREATE_DATE,STATUS,REMARK) values (7,7,'语文题',to_timestamp('11-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','难');
Insert into QUESTION_DB (ID,ADMIN_ID,NAME,CREATE_DATE,STATUS,REMARK) values (15,1,'测试大',to_timestamp('11-2月 -17 12.00.00.000000000 上午','DD-MON-RR HH.MI.SS.FF AM'),'1','22');

---------------------------------------------------
--   END DATA FOR TABLE QUESTION_DB
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE EXAM_MAIN
--   FILTER = none used
---------------------------------------------------
REM INSERTING into EXAM_MAIN

---------------------------------------------------
--   END DATA FOR TABLE EXAM_MAIN
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE PAPER_SECTION
--   FILTER = none used
---------------------------------------------------
REM INSERTING into PAPER_SECTION
Insert into PAPER_SECTION (ID,PAPER_ID,SECTION_NAME,PER_SCORE,REMARK) values (15,8,'12',12,'12');
Insert into PAPER_SECTION (ID,PAPER_ID,SECTION_NAME,PER_SCORE,REMARK) values (17,18,'8',8,'8');
Insert into PAPER_SECTION (ID,PAPER_ID,SECTION_NAME,PER_SCORE,REMARK) values (18,18,'7',7,'7');
Insert into PAPER_SECTION (ID,PAPER_ID,SECTION_NAME,PER_SCORE,REMARK) values (19,18,'5',5,'5');
Insert into PAPER_SECTION (ID,PAPER_ID,SECTION_NAME,PER_SCORE,REMARK) values (21,23,'地方啊',100,'发生大');
Insert into PAPER_SECTION (ID,PAPER_ID,SECTION_NAME,PER_SCORE,REMARK) values (22,24,'的发发的',50,'否十大');
Insert into PAPER_SECTION (ID,PAPER_ID,SECTION_NAME,PER_SCORE,REMARK) values (23,25,'发的啊 ',40,'发生大否');
Insert into PAPER_SECTION (ID,PAPER_ID,SECTION_NAME,PER_SCORE,REMARK) values (24,26,'发生大',70,'的说法');
Insert into PAPER_SECTION (ID,PAPER_ID,SECTION_NAME,PER_SCORE,REMARK) values (25,27,'发的否',60,'范德萨安抚');
Insert into PAPER_SECTION (ID,PAPER_ID,SECTION_NAME,PER_SCORE,REMARK) values (26,28,'范德萨',80,'发的否');
Insert into PAPER_SECTION (ID,PAPER_ID,SECTION_NAME,PER_SCORE,REMARK) values (27,29,'范德萨否',60,'发送否啊');
Insert into PAPER_SECTION (ID,PAPER_ID,SECTION_NAME,PER_SCORE,REMARK) values (8,9,'22',123,'12');
Insert into PAPER_SECTION (ID,PAPER_ID,SECTION_NAME,PER_SCORE,REMARK) values (12,16,'12',12,'12');

---------------------------------------------------
--   END DATA FOR TABLE PAPER_SECTION
---------------------------------------------------
--------------------------------------------------------
--  Constraints for Table ADMIN
--------------------------------------------------------

  ALTER TABLE "ADMIN" ADD CONSTRAINT "ADMIN_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "ADMIN" ADD CONSTRAINT "ADMIN_UK1" UNIQUE ("USER_NAME") ENABLE;
 
  ALTER TABLE "ADMIN" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "ADMIN" MODIFY ("ROLE_ID" NOT NULL ENABLE);
 
  ALTER TABLE "ADMIN" MODIFY ("USER_NAME" NOT NULL ENABLE);
 
  ALTER TABLE "ADMIN" MODIFY ("USER_PASS" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ADMIN_ROLES
--------------------------------------------------------

  ALTER TABLE "ADMIN_ROLES" ADD CONSTRAINT "ADMIN_ROLES_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "ADMIN_ROLES" ADD CONSTRAINT "ADMIN_ROLES_UK1" UNIQUE ("ROLE_NAME") ENABLE;
 
  ALTER TABLE "ADMIN_ROLES" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "ADMIN_ROLES" MODIFY ("ROLE_NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ADMIN_ROLES_SETTINGS
--------------------------------------------------------

  ALTER TABLE "ADMIN_ROLES_SETTINGS" ADD CONSTRAINT "ADMIN_ROLES_SETTINGS_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "ADMIN_ROLES_SETTINGS" ADD CONSTRAINT "ADMIN_ROLES_SETTINGS_UK1" UNIQUE ("NAME") ENABLE;
 
  ALTER TABLE "ADMIN_ROLES_SETTINGS" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "ADMIN_ROLES_SETTINGS" MODIFY ("NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CONFIG
--------------------------------------------------------

  ALTER TABLE "CONFIG" ADD CONSTRAINT "CONFIG_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "CONFIG" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table EXAM_DETAIL
--------------------------------------------------------

  ALTER TABLE "EXAM_DETAIL" ADD CONSTRAINT "EXAM_DETAIL_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "EXAM_DETAIL" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "EXAM_DETAIL" MODIFY ("STATUS" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table EXAM_MAIN
--------------------------------------------------------

  ALTER TABLE "EXAM_MAIN" ADD CONSTRAINT "EXAM_MAIN_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "EXAM_MAIN" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "EXAM_MAIN" MODIFY ("USER_ID" NOT NULL ENABLE);
 
  ALTER TABLE "EXAM_MAIN" MODIFY ("PAPER_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ONLINES
--------------------------------------------------------

  ALTER TABLE "ONLINES" ADD CONSTRAINT "ONLINES_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "ONLINES" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "ONLINES" MODIFY ("USER_ID" NOT NULL ENABLE);
 
  ALTER TABLE "ONLINES" MODIFY ("PAPER_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PAPER
--------------------------------------------------------

  ALTER TABLE "PAPER" ADD CONSTRAINT "PAPER_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "PAPER" ADD CONSTRAINT "PAPER_UK1" UNIQUE ("PAPER_NAME") ENABLE;
 
  ALTER TABLE "PAPER" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "PAPER" MODIFY ("ADMIN_ID" NOT NULL ENABLE);
 
  ALTER TABLE "PAPER" MODIFY ("PAPER_NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PAPER_DETAIL
--------------------------------------------------------

  ALTER TABLE "PAPER_DETAIL" ADD CONSTRAINT "PAPER_DETAIL_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "PAPER_DETAIL" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "PAPER_DETAIL" MODIFY ("PAPER_ID" NOT NULL ENABLE);
 
  ALTER TABLE "PAPER_DETAIL" MODIFY ("PAPER_SECTION_ID" NOT NULL ENABLE);
 
  ALTER TABLE "PAPER_DETAIL" MODIFY ("QUESTION_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PAPER_SECTION
--------------------------------------------------------

  ALTER TABLE "PAPER_SECTION" ADD CONSTRAINT "PAPER_SECTION_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "PAPER_SECTION" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "PAPER_SECTION" MODIFY ("PAPER_ID" NOT NULL ENABLE);
 
  ALTER TABLE "PAPER_SECTION" MODIFY ("SECTION_NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PAPER_USER_GROUP
--------------------------------------------------------

  ALTER TABLE "PAPER_USER_GROUP" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "PAPER_USER_GROUP" MODIFY ("PAPER_ID" NOT NULL ENABLE);
 
  ALTER TABLE "PAPER_USER_GROUP" MODIFY ("USER_GROUP_ID" NOT NULL ENABLE);
 
  ALTER TABLE "PAPER_USER_GROUP" ADD CONSTRAINT "TABLE1_PK" PRIMARY KEY ("ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table QUESTION
--------------------------------------------------------

  ALTER TABLE "QUESTION" ADD CONSTRAINT "QUESTION_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "QUESTION" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "QUESTION" MODIFY ("DB_ID" NOT NULL ENABLE);
 
  ALTER TABLE "QUESTION" MODIFY ("ADMIN_ID" NOT NULL ENABLE);
 
  ALTER TABLE "QUESTION" MODIFY ("QTYPE" NOT NULL ENABLE);
 
  ALTER TABLE "QUESTION" MODIFY ("QLEVEL" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table QUESTION_DB
--------------------------------------------------------

  ALTER TABLE "QUESTION_DB" ADD CONSTRAINT "QUESTION_DB_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "QUESTION_DB" ADD CONSTRAINT "QUESTION_DB_UK1" UNIQUE ("NAME") ENABLE;
 
  ALTER TABLE "QUESTION_DB" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "QUESTION_DB" MODIFY ("ADMIN_ID" NOT NULL ENABLE);
 
  ALTER TABLE "QUESTION_DB" MODIFY ("NAME" NOT NULL ENABLE);
 
  ALTER TABLE "QUESTION_DB" MODIFY ("CREATE_DATE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table QUESTION_OPTIONS
--------------------------------------------------------

  ALTER TABLE "QUESTION_OPTIONS" ADD CONSTRAINT "QUESTION_OPTIONS_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "QUESTION_OPTIONS" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "QUESTION_OPTIONS" MODIFY ("QUESTION_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SYS_TIPS
--------------------------------------------------------

  ALTER TABLE "SYS_TIPS" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "SYS_TIPS" ADD CONSTRAINT "SYS_TIPS_PK" PRIMARY KEY ("ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table USERS
--------------------------------------------------------

  ALTER TABLE "USERS" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "USERS" ADD CONSTRAINT "USERS_PK" PRIMARY KEY ("ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table USER_GROUPS
--------------------------------------------------------

  ALTER TABLE "USER_GROUPS" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "USER_GROUPS" ADD CONSTRAINT "USER_GROUPS_PK" PRIMARY KEY ("ID") ENABLE;

--------------------------------------------------------
--  DDL for Index ADMIN_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ADMIN_PK" ON "ADMIN" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index ADMIN_ROLES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ADMIN_ROLES_PK" ON "ADMIN_ROLES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index ADMIN_ROLES_SETTINGS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ADMIN_ROLES_SETTINGS_PK" ON "ADMIN_ROLES_SETTINGS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index ADMIN_ROLES_SETTINGS_UK1
--------------------------------------------------------

  CREATE UNIQUE INDEX "ADMIN_ROLES_SETTINGS_UK1" ON "ADMIN_ROLES_SETTINGS" ("NAME") 
  ;
--------------------------------------------------------
--  DDL for Index ADMIN_ROLES_UK1
--------------------------------------------------------

  CREATE UNIQUE INDEX "ADMIN_ROLES_UK1" ON "ADMIN_ROLES" ("ROLE_NAME") 
  ;
--------------------------------------------------------
--  DDL for Index ADMIN_UK1
--------------------------------------------------------

  CREATE UNIQUE INDEX "ADMIN_UK1" ON "ADMIN" ("USER_NAME") 
  ;
--------------------------------------------------------
--  DDL for Index CONFIG_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "CONFIG_PK" ON "CONFIG" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index EXAM_DETAIL_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "EXAM_DETAIL_PK" ON "EXAM_DETAIL" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index EXAM_MAIN_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "EXAM_MAIN_PK" ON "EXAM_MAIN" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index ONLINES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ONLINES_PK" ON "ONLINES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index PAPER_DETAIL_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PAPER_DETAIL_PK" ON "PAPER_DETAIL" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index PAPER_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PAPER_PK" ON "PAPER" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index PAPER_SECTION_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PAPER_SECTION_PK" ON "PAPER_SECTION" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index PAPER_UK1
--------------------------------------------------------

  CREATE UNIQUE INDEX "PAPER_UK1" ON "PAPER" ("PAPER_NAME") 
  ;
--------------------------------------------------------
--  DDL for Index QUESTION_DB_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "QUESTION_DB_PK" ON "QUESTION_DB" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index QUESTION_DB_UK1
--------------------------------------------------------

  CREATE UNIQUE INDEX "QUESTION_DB_UK1" ON "QUESTION_DB" ("NAME") 
  ;
--------------------------------------------------------
--  DDL for Index QUESTION_OPTIONS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "QUESTION_OPTIONS_PK" ON "QUESTION_OPTIONS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index QUESTION_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "QUESTION_PK" ON "QUESTION" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_TIPS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_TIPS_PK" ON "SYS_TIPS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index TABLE1_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "TABLE1_PK" ON "PAPER_USER_GROUP" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index USERS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "USERS_PK" ON "USERS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index USER_GROUPS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "USER_GROUPS_PK" ON "USER_GROUPS" ("ID") 
  ;
--------------------------------------------------------
--  Ref Constraints for Table ADMIN
--------------------------------------------------------

  ALTER TABLE "ADMIN" ADD CONSTRAINT "ADMIN_ROLES_PK1" FOREIGN KEY ("ROLE_ID")
	  REFERENCES "ADMIN_ROLES" ("ID") ENABLE;



--------------------------------------------------------
--  Ref Constraints for Table EXAM_DETAIL
--------------------------------------------------------

  ALTER TABLE "EXAM_DETAIL" ADD CONSTRAINT "EXAM_DETAIL_EXAM_MAIN_FK1" FOREIGN KEY ("MAIN_ID")
	  REFERENCES "EXAM_MAIN" ("ID") ENABLE;
 
  ALTER TABLE "EXAM_DETAIL" ADD CONSTRAINT "EXAM_DETAIL_QUESTION_FK1" FOREIGN KEY ("QUESTION_ID")
	  REFERENCES "QUESTION" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table EXAM_MAIN
--------------------------------------------------------

  ALTER TABLE "EXAM_MAIN" ADD CONSTRAINT "EXAM_MAIN_PAPER_FK1" FOREIGN KEY ("PAPER_ID")
	  REFERENCES "PAPER" ("ID") ENABLE;
 
  ALTER TABLE "EXAM_MAIN" ADD CONSTRAINT "EXAM_MAIN_USERS_FK1" FOREIGN KEY ("USER_ID")
	  REFERENCES "USERS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ONLINES
--------------------------------------------------------

  ALTER TABLE "ONLINES" ADD CONSTRAINT "ONLINES_PAPER_FK1" FOREIGN KEY ("PAPER_ID")
	  REFERENCES "PAPER" ("ID") ENABLE;
 
  ALTER TABLE "ONLINES" ADD CONSTRAINT "ONLINES_USERS_FK1" FOREIGN KEY ("USER_ID")
	  REFERENCES "USERS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PAPER
--------------------------------------------------------

  ALTER TABLE "PAPER" ADD CONSTRAINT "PAPER_ADMIN_FK1" FOREIGN KEY ("ADMIN_ID")
	  REFERENCES "ADMIN" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PAPER_DETAIL
--------------------------------------------------------

  ALTER TABLE "PAPER_DETAIL" ADD CONSTRAINT "PAPER_DETAIL_PAPER_FK1" FOREIGN KEY ("PAPER_ID")
	  REFERENCES "PAPER" ("ID") ENABLE;
 
  ALTER TABLE "PAPER_DETAIL" ADD CONSTRAINT "PAPER_DETAIL_PAPER_SECTIO_FK1" FOREIGN KEY ("PAPER_SECTION_ID")
	  REFERENCES "PAPER_SECTION" ("ID") ENABLE;
 
  ALTER TABLE "PAPER_DETAIL" ADD CONSTRAINT "PAPER_DETAIL_QUESTION_FK1" FOREIGN KEY ("QUESTION_ID")
	  REFERENCES "QUESTION" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PAPER_SECTION
--------------------------------------------------------

  ALTER TABLE "PAPER_SECTION" ADD CONSTRAINT "PAPER_SECTION_PAPER_FK1" FOREIGN KEY ("PAPER_ID")
	  REFERENCES "PAPER" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PAPER_USER_GROUP
--------------------------------------------------------

  ALTER TABLE "PAPER_USER_GROUP" ADD CONSTRAINT "TABLE1_PAPER_FK1" FOREIGN KEY ("PAPER_ID")
	  REFERENCES "PAPER" ("ID") ENABLE;
 
  ALTER TABLE "PAPER_USER_GROUP" ADD CONSTRAINT "TABLE1_USER_GROUPS_FK1" FOREIGN KEY ("USER_GROUP_ID")
	  REFERENCES "USER_GROUPS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table QUESTION
--------------------------------------------------------

  ALTER TABLE "QUESTION" ADD CONSTRAINT "QUESTION_ADMIN_FK1" FOREIGN KEY ("ADMIN_ID")
	  REFERENCES "ADMIN" ("ID") ENABLE;
 
  ALTER TABLE "QUESTION" ADD CONSTRAINT "QUESTION_QUESTION_DB_FK1" FOREIGN KEY ("DB_ID")
	  REFERENCES "QUESTION_DB" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table QUESTION_DB
--------------------------------------------------------

  ALTER TABLE "QUESTION_DB" ADD CONSTRAINT "QUESTION_DB_ADMIN_FK1" FOREIGN KEY ("ADMIN_ID")
	  REFERENCES "ADMIN" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table QUESTION_OPTIONS
--------------------------------------------------------

  ALTER TABLE "QUESTION_OPTIONS" ADD CONSTRAINT "QUESTION_OPTIONS_QUESTION_FK1" FOREIGN KEY ("QUESTION_ID")
	  REFERENCES "QUESTION" ("ID") ENABLE;

--------------------------------------------------------
--  Ref Constraints for Table USERS
--------------------------------------------------------

  ALTER TABLE "USERS" ADD CONSTRAINT "USERS_USER_GROUPS_FK1" FOREIGN KEY ("GROUP_ID")
	  REFERENCES "USER_GROUPS" ("ID") ENABLE;


